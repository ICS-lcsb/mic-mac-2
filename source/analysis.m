%% Preprocessing

% PARAMETERS

%folder_path = '\\atlas\LCSB_Images\groups\skupin\_projects\MICMAC_2\PAPER_HumanAnalysis\All_Regions_no_strel\';
folder_path = './OutputImages/HumanAnalysis/HumanAnalysis2021-11-24/';

C1 = 'CTL';
C2 = 'AD';
C3 = 'DLB';

R1 = 'CA1';
R2 = 'CA3';
R3 = 'DG';

n_feats = 50;
selected_features = [1:4 6 7 9:12 28 41 42 43 45 48];
feat_names = ["Volume","Volume/nEdges","Average Curvature","Max Curvature","Compactness","Polarity","Link Density","Average Node degree","Mean Edge Length","Max Edge Length","S-Metric","Ending Nodes Density","1st Largest Bound","2nd Largest Bound","1 over 2","Node density"];


bound_percentage = 0.075;

% OPEN DIRECTORY 

addpath(genpath('./Library'));

dispstat('           -- MIC-MAC 2.0 -- ','keepthis');
dispstat('              -Analysis- ','keepthis');
dispstat('          ','keepthis');

fold = dir(folder_path);
[n_objs, ~] = size(fold);
samples_attribute_name = [""];
samples_metadata_name = [""];
samples_volume_name = [""];

n_samps_attr = 0;
n_samps_meta = 0;
n_samps_vol = 0;

for ii = 1 : n_objs
    if contains(fold(ii).name,'attributes.mat')
        n_samps_attr = n_samps_attr+1;
        samples_attribute_name(n_samps_attr) = string(fold(ii).name);
    elseif contains(fold(ii).name,'metadata.mat')
        n_samps_meta = n_samps_meta+1;
        samples_metadata_name(n_samps_meta) = string(fold(ii).name);
    elseif contains(fold(ii).name,'volumes.mat')
        n_samps_vol = n_samps_vol+1;
        samples_volume_name(n_samps_vol) = string(fold(ii).name);
    else
        continue;
    end
end

if n_samps_attr*2 ~= n_samps_meta+n_samps_vol
    error('For some sample/s at least one file (attributes.mat, metadata.mat, volumes.mat) is missing.');
else
    dispstat(['Found ' num2str(n_samps_attr) ' samples.'],'timestamp','keepthis');
    n_samps = n_samps_attr;
    clear n_samps_attr n_samps_vol n_samps_meta n_objs fold
end

% MERGING ATTRIBUTE DATASET

data = [];
structsPerSample = zeros(1,n_samps);
samples_Regions = zeros(1,n_samps);
samples_Conditions = zeros(1,n_samps);

for ii = 1: n_samps
        
    load(strcat(folder_path,samples_attribute_name(ii)));
    
    [structsPerSample(ii),~] = size(attributes); 
    
    fail = 0;
    
    if contains(samples_attribute_name(ii),C1)
        attributes(:,n_feats+1) = 1; 
        samples_Conditions(ii) = 1;
    elseif contains(samples_attribute_name(ii),C2)
        attributes(:,n_feats+1) = 2;
        samples_Conditions(ii) = 2;
    elseif contains(samples_attribute_name(ii),C3)
        attributes(:,n_feats+1) = 3;
        samples_Conditions(ii) = 3;
    else
        error(['No ID found for sample ' char(samples_attribute_name(ii))]);
        fail = 1;
    end
    
    if contains(samples_attribute_name(ii),R1)
        attributes(:,n_feats+2) = 1; 
        samples_Regions(ii) = 1;
    elseif contains(samples_attribute_name(ii),R2)
        attributes(:,n_feats+2) = 2;
        samples_Regions (ii) = 2;
    elseif contains(samples_attribute_name(ii),R3)
        attributes(:,n_feats+2) = 3;
        samples_Regions(ii) = 3;
    else
        error(['No ID found for sample ' char(samples_attribute_name(ii))]);
        fail = 1;
    end
    
    if ~fail
        data = vertcat(data,attributes);
    end
end

startSamp(1) = 1;
for ii = 2 : n_samps
    startSamp(ii) = startSamp(ii-1)+structsPerSample(ii-1);
end

n_cells = length(data);

clear attributes fail

dispstat('Merging Cell Attributes Complete','timestamp','keepthis');

% MERGING VOLUME DATASET  

volumes = {};
bounding_boxes = [];
filler = 0;

for ii = 1: n_samps
    load(strcat(folder_path,samples_volume_name(ii)));
    volumes(filler+1:filler+structsPerSample(ii)) = structs.Image;
    bounding_boxes(filler+1:filler+structsPerSample(ii),:) = structs.BoundingBox;
    filler = filler+structsPerSample(ii);
end

clear structs filler

dispstat('Merging Cell Volumes Complete','timestamp','keepthis');

% MERGING METADATA DATASET  

sz = [n_samps 4];
varTypes = {'double','double','double','double'};
varNames = {'X','Y','Z','Density'};
metadata = table('Size',sz,'VariableTypes',varTypes,'VariableNames',varNames);

for ii = 1: n_samps
    load(strcat(folder_path,samples_metadata_name(ii)),'xx','yy','zz','TOT_IBA1_density');
    metadata.X(ii) = xx;
    metadata.Y(ii) = yy;
    metadata.Z(ii) = zz;
    metadata.Density(ii) = TOT_IBA1_density;
end

clear xx yy zz TOT_IBA1_density varTypes varNames sz

dispstat('Merging Metadata Complete','timestamp','keepthis');

% REMOVING CELLS TOUCHING BORDERS

removedList = false(1,n_cells);

for ii = 1 : n_samps
    I_bounds = true(metadata.X(ii),metadata.Y(ii),metadata.Z(ii));
    I_bounds(10:end-10,10:end-10,2:end-1) = false;
    
    for jj = 1 : structsPerSample(ii)
        tot_idx = startSamp(ii) + jj - 1;
        vol = volumes{tot_idx};
        
        [idx,idy,idz] = limitBoundingBox(bounding_boxes(tot_idx,:),metadata.Z(ii));
        
        n_pix_struct = sum(sum(sum(vol)));
        n_pix_bound = sum(sum(sum(vol & I_bounds(idx,idy,idz))));
        bound_scores(jj) = n_pix_bound/n_pix_struct;

        if bound_scores(jj) > bound_percentage
            removedList(tot_idx) = true;
        end 
    end
end

for ii = 1 : n_samps
   removedPerSample(ii) = sum(removedList(startSamp(ii):startSamp(ii)+structsPerSample(ii)-1));
end

data(removedList,:) = [];
volumes(removedList) = [];
bounding_boxes(removedList,:) = [];

structsPerSample = structsPerSample - removedPerSample;
startSamp(1) = 1;
for ii = 2 : n_samps
    startSamp(ii) = startSamp(ii-1)+structsPerSample(ii-1);
end

startSamp(end+1) = length(data); 

clear ii jj n_pix_struct n_pix_bound bounds tot_idx vol idx idy idz

dispstat('Removing Cell Touching Border Complete','timestamp','keepthis');

% LOADING SAMPLE INFO

load('./SampleInfoComplete12.mat');
found = false;
table_idx = zeros(1,n_samps);

for jj = 1 : n_samps
    
    for ii = 1 : height(SampleInfo)
        if contains(samples_volume_name(jj),SampleInfo.Condition(ii)) && contains(samples_volume_name(jj),SampleInfo.ID(ii)) && contains(samples_volume_name(jj),SampleInfo.Region(ii))
            found = true;
            break;
        end
    end
    
    if found
        table_idx(jj) = ii;
        found = false;
    end
    
end

if prod(table_idx) == 0
   error('For some samples there is no corresponding Sample Information.') 
end

SampleInfo_temp = SampleInfo;
SampleInfo_temp(:,:) = [];

for ii = 1 : n_samps
    SampleInfo_temp(ii,:) =  SampleInfo(table_idx(ii),:);
end

SampleInfo = SampleInfo_temp;

SampleInfo.LB(SampleInfo.Region ~= "CA1") = nan;
SampleInfo.SeverityQ(SampleInfo.Region ~= "CA1") = nan;

clear ii jj SampleInfo_temp found table_idx bound_percentage bound_scores ...
    I_bounds n_cells removedList removedPerSample ...
    samples_Conditions samples_Regions samples_attribute_name ...
    samples_metadata_name samples_volume_name 

dispstat('Sample Information Retrieval Complete','timestamp','keepthis');

% REMOVING OUTLIERS

%Removing NaN and Inf
outlierList = false(length(data),1);
nan_outliers = sum(isnan(data(:,selected_features)),2)>0;
inf_outliers = sum(isinf(data(:,selected_features)),2)>0;
outlierList = outlierList | nan_outliers | inf_outliers;

data(outlierList,:) = []; 
volumes(outlierList) = [];
bounding_boxes(outlierList,:) = [];

removedPerSample = zeros(1,n_samps);
for ii = 1 : n_samps
   removedPerSample(ii) = sum(outlierList(startSamp(ii):startSamp(ii+1)-1)); 
end
structsPerSample = structsPerSample - removedPerSample;

startSamp = zeros(1,n_samps+1);
startSamp(1) = 1;
for ii = 2 : n_samps
    startSamp(ii) = startSamp(ii-1)+structsPerSample(ii-1);
end
startSamp(end) = length(data); 

%Removing Outliers with trained model
load('trainedModelOutliers.mat');
temp_cell_set = data(:, selected_features);
outlierList = trainedModel.predictFcn(temp_cell_set);
outlierList = logical(outlierList);

data(outlierList,:) = []; 
volumes(outlierList) = [];
bounding_boxes(outlierList,:) = [];

removedPerSample = zeros(1,n_samps);
for ii = 1 : n_samps
   removedPerSample(ii) = sum(outlierList(startSamp(ii):startSamp(ii+1)-1)); 
end
structsPerSample = structsPerSample - removedPerSample;

startSamp = zeros(1,n_samps+1);
startSamp(1) = 1;
for ii = 2 : n_samps
    startSamp(ii) = startSamp(ii-1)+structsPerSample(ii-1);
end
startSamp(end) = length(data); 

%Creating Cleaned Sets
cell_set = data(:, selected_features);
mean_set = zeros(n_samps, length(selected_features));
std_set = zeros(n_samps, length(selected_features));

for ii = 1 : n_samps
   mean_set(ii,:) = mean(cell_set(startSamp(ii):startSamp(ii+1),:)); 
end

cell_set_norm = normalize(cell_set,'range');

clear  ii temp_cell_set trainedModel outlierList inf_outliers nan_outliers C1 C2 C3 R1 R2 R3 M1 M2 folder_path 

dispstat('Removing Outliers and Set Creation Complete','timestamp','keepthis');

%% Number of Cells per Region/Condition

CTL_CA1 = length(find(data(:,51) == 1 & data(:,52) == 1 ));
CTL_CA3 = length(find(data(:,51) == 1 & data(:,52) == 2 ));
CTL_DG = length(find(data(:,51) == 1 & data(:,52) == 3 ));

AD_CA1 = length(find(data(:,51) == 2 & data(:,52) == 1 ));
AD_CA3 = length(find(data(:,51) == 2 & data(:,52) == 2 ));
AD_DG = length(find(data(:,51) == 2 & data(:,52) == 3 ));

DLB_CA1 = length(find(data(:,51) == 3 & data(:,52) == 1 ));
DLB_CA3 = length(find(data(:,51) == 3 & data(:,52) == 2 ));
DLB_DG = length(find(data(:,51) == 3 & data(:,52) == 3 ));

X = categorical({'CTL','AD','DLB'});
X = reordercats(X,{'CTL','AD','DLB'});


cell_bar = [CTL_CA1 CTL_CA3 CTL_DG ; AD_CA1 AD_CA3 AD_DG ; DLB_CA1 DLB_CA3 DLB_DG ];

figure; bar(X, cell_bar)
legend('CA1','CA3','DG')
title('Number of cells per region')

cell_mat = nan(90,9);

temp = find(SampleInfo.Condition == "CTL" & SampleInfo.Region == "CA1");
cell_mat(1:length(temp),1) = structsPerSample(temp);
temp = find(SampleInfo.Condition == "CTL" & SampleInfo.Region == "CA3");
cell_mat(1:length(temp),2) = structsPerSample(temp);
temp = find(SampleInfo.Condition == "CTL" & SampleInfo.Region == "DG");
cell_mat(1:length(temp),3) = structsPerSample(temp);

temp = find(SampleInfo.Condition == "AD" & SampleInfo.Region == "CA1");
cell_mat(1:length(temp),4) = structsPerSample(temp);
temp = find(SampleInfo.Condition == "AD" & SampleInfo.Region == "CA3");
cell_mat(1:length(temp),5) = structsPerSample(temp);
temp = find(SampleInfo.Condition == "AD" & SampleInfo.Region == "DG");
cell_mat(1:length(temp),6) = structsPerSample(temp);

temp = find(SampleInfo.Condition == "DLB" & SampleInfo.Region == "CA1");
cell_mat(1:length(temp),7) = structsPerSample(temp);
temp = find(SampleInfo.Condition == "DLB" & SampleInfo.Region == "CA3");
cell_mat(1:length(temp),8) = structsPerSample(temp);
temp = find(SampleInfo.Condition == "DLB" & SampleInfo.Region == "DG");
cell_mat(1:length(temp),9) = structsPerSample(temp);

figure;violinplot(cell_mat,{'CTL -CA1','CTL - CA3','CTL - DG','AD - CA1','AD - CA3','AD - DG','DLB - CA1','DLB - CA3','DLB - DG'})
title('Number of cells per region per condition')


%% Proteinopathy Analysis

%Condition
figure; violinplot(SampleInfo.Tau,SampleInfo.Condition,'GroupOrder',{'CTL' 'DLB' 'AD'});
title('TAU Density per Condition ')

figure; violinplot(SampleInfo.ALoad,SampleInfo.Condition,'GroupOrder',{'CTL' 'DLB' 'AD'});
title('Amyloid Density per Condition  ')

figure; violinplot(SampleInfo.Psyn,SampleInfo.Condition,'GroupOrder',{'CTL' 'DLB' 'AD'});
title('Psyn Density per Condition  ')

%CTL
figure; violinplot(SampleInfo.Tau(SampleInfo.Condition == 'CTL'),SampleInfo.Region(SampleInfo.Condition == 'CTL'),'GroupOrder',{'CA1' 'CA3' 'DG'});
title('TAU Density per CTL')

%AD
figure; violinplot(SampleInfo.ALoad(SampleInfo.Condition == 'AD'),SampleInfo.Region(SampleInfo.Condition == 'AD'),'GroupOrder',{'CA1' 'CA3' 'DG'});
title('Amyloid Density per AD')
%DLB
figure; violinplot(SampleInfo.Psyn(SampleInfo.Condition == 'DLB'),SampleInfo.Region(SampleInfo.Condition == 'DLB'),'GroupOrder',{'CA1' 'CA3' 'DG'});
title('Psyn Density per DLB')

%CA1
figure; violinplot(SampleInfo.Tau(SampleInfo.Region == 'CA1'),SampleInfo.Condition(SampleInfo.Region == 'CA1'),'GroupOrder',{'CTL' 'DLB' 'AD'});
title('TAU Density per Condition in CA1')

figure; violinplot(SampleInfo.ALoad(SampleInfo.Region == 'CA1'),SampleInfo.Condition(SampleInfo.Region == 'CA1'),'GroupOrder',{'CTL' 'DLB' 'AD'});
title('Amyloid Density per Condition  in CA1')

figure; violinplot(SampleInfo.Psyn(SampleInfo.Region == 'CA1'),SampleInfo.Condition(SampleInfo.Region == 'CA1'),'GroupOrder',{'CTL' 'DLB' 'AD'});
title('Psyn Density per Condition  in CA1')

%CA3
figure; violinplot(SampleInfo.Tau(SampleInfo.Region == 'CA3'),SampleInfo.Condition(SampleInfo.Region == 'CA3'),'GroupOrder',{'CTL' 'DLB' 'AD'});
title('TAU Density per Condition in CA3')

figure; violinplot(SampleInfo.ALoad(SampleInfo.Region == 'CA3'),SampleInfo.Condition(SampleInfo.Region == 'CA3'),'GroupOrder',{'CTL' 'DLB' 'AD'});
title('Amyloid Density per Condition  in CA3')

figure; violinplot(SampleInfo.Psyn(SampleInfo.Region == 'CA3'),SampleInfo.Condition(SampleInfo.Region == 'CA3'),'GroupOrder',{'CTL' 'DLB' 'AD'});
title('Psyn Density per Condition  in CA3')

%DG
figure; violinplot(SampleInfo.Tau(SampleInfo.Region == 'DG'),SampleInfo.Condition(SampleInfo.Region == 'DG'),'GroupOrder',{'CTL' 'DLB' 'AD'});
title('TAU Density per Condition in DG')

figure; violinplot(SampleInfo.ALoad(SampleInfo.Region == 'DG'),SampleInfo.Condition(SampleInfo.Region == 'DG'),'GroupOrder',{'CTL' 'DLB' 'AD'});
title('Amyloid Density per Condition  in DG')

figure; violinplot(SampleInfo.Psyn(SampleInfo.Region == 'DG'),SampleInfo.Condition(SampleInfo.Region == 'DG'),'GroupOrder',{'CTL' 'DLB' 'AD'});
title('Psyn Density per Condition  in DG')

%Gender
figure; violinplot(SampleInfo.Tau(SampleInfo.Condition == 'AD' & SampleInfo.Region == 'CA1'),SampleInfo.Gender(SampleInfo.Condition == 'AD' & SampleInfo.Region == 'CA1'),'GroupOrder',{'M' 'F'});
title('TAU Density in AD (M vs F)')

figure; violinplot(SampleInfo.ALoad(SampleInfo.Condition == 'AD' & SampleInfo.Region == 'CA1'),SampleInfo.Gender(SampleInfo.Condition == 'AD' & SampleInfo.Region == 'CA1'),'GroupOrder',{'M' 'F'});
title('Amyloid Density in AD (M vs F)')

figure; violinplot(SampleInfo.Psyn(SampleInfo.Condition == 'AD' & SampleInfo.Region == 'CA1'),SampleInfo.Gender(SampleInfo.Condition == 'AD' & SampleInfo.Region == 'CA1'),'GroupOrder',{'M' 'F'});
title('Psyn Density in AD (M vs F)')

figure; violinplot(SampleInfo.Tau(SampleInfo.Condition == 'CTL' & SampleInfo.Region == 'CA1'),SampleInfo.Gender(SampleInfo.Condition == 'CTL' & SampleInfo.Region == 'CA1'),'GroupOrder',{'M' 'F'});
title('TAU Density in CTL (M vs F)')

figure; violinplot(SampleInfo.ALoad(SampleInfo.Condition == 'CTL' & SampleInfo.Region == 'CA1'),SampleInfo.Gender(SampleInfo.Condition == 'CTL' & SampleInfo.Region == 'CA1'),'GroupOrder',{'M' 'F'});
title('Amyloid Density in CTL (M vs F)')

figure; violinplot(SampleInfo.Psyn(SampleInfo.Condition == 'CTL' & SampleInfo.Region == 'CA1'),SampleInfo.Gender(SampleInfo.Condition == 'CTL' & SampleInfo.Region == 'CA1'),'GroupOrder',{'M' 'F'});
title('Psyn Density in CTL (M vs F)')

figure; violinplot(SampleInfo.Tau(SampleInfo.Condition == 'DLB' & SampleInfo.Region == 'CA1'),SampleInfo.Gender(SampleInfo.Condition == 'DLB' & SampleInfo.Region == 'CA1'),'GroupOrder',{'M' 'F'});
title('TAU Density in DLB (M vs F)')

figure; violinplot(SampleInfo.ALoad(SampleInfo.Condition == 'DLB' & SampleInfo.Region == 'CA1'),SampleInfo.Gender(SampleInfo.Condition == 'DLB' & SampleInfo.Region == 'CA1'),'GroupOrder',{'M' 'F'});
title('Amyloid Density in DLB (M vs F)')

figure; violinplot(SampleInfo.Psyn(SampleInfo.Condition == 'DLB' & SampleInfo.Region == 'CA1'),SampleInfo.Gender(SampleInfo.Condition == 'DLB' & SampleInfo.Region == 'CA1'),'GroupOrder',{'M' 'F'});
title('Psyn Density in DLB (M vs F)')

%% CORRELATION PLOTS WITH MARKERS

var_names = [feat_names, "Tau", "ALoad", "Psyn"];
proteinopaths = horzcat(SampleInfo.Tau,SampleInfo.ALoad,SampleInfo.Psyn);

figure;
corrplot(horzcat(mean_set,proteinopaths),'type','Spearman','testR','on','varNames',var_names);
title('Correlation Plot for all conditions')


%ALL BY CONDITION

figure;
corrplot(horzcat(mean_set(SampleInfo.Condition == "CTL",:),proteinopaths(SampleInfo.Condition == "CTL",:)),'type','Spearman','testR','on','varNames',var_names);
title('Correlation Plot for all regions in CTL')

figure;
corrplot(horzcat(mean_set(SampleInfo.Condition == "AD",:),proteinopaths(SampleInfo.Condition == "AD",:)),'type','Spearman','testR','on','varNames',var_names);
title('Correlation Plot for all regions in AD')

figure;
corrplot(horzcat(mean_set(SampleInfo.Condition == "DLB",:),proteinopaths(SampleInfo.Condition == "DLB",:)),'type','Spearman','testR','on','varNames',var_names);
title('Correlation Plot for all regions in DLB')

%ALL BY REGION

figure;
corrplot(horzcat(mean_set(SampleInfo.Region == "CA1",:),proteinopaths(SampleInfo.Region == "CA1",:)),'type','Spearman','testR','on','varNames',var_names);
title('Correlation Plot for all conditions in CA1')

figure;
corrplot(horzcat(mean_set(SampleInfo.Region == "CA3",:),proteinopaths(SampleInfo.Region == "CA3",:)),'type','Spearman','testR','on','varNames',var_names);
title('Correlation Plot for all conditions in CA3')

figure;
corrplot(horzcat(mean_set(SampleInfo.Region == "DG",:),proteinopaths(SampleInfo.Region == "DG",:)),'type','Spearman','testR','on','varNames',var_names);
title('Correlation Plot for all conditions in DG')

%CTL

figure;
corrplot(horzcat(mean_set(SampleInfo.Region == "CA1" & SampleInfo.Condition == "CTL",:),proteinopaths(SampleInfo.Region == "CA1"  & SampleInfo.Condition == "CTL",:)),'type','Spearman','testR','on','varNames',var_names);
title('Correlation Plot CTL in CA1')

figure;
corrplot(horzcat(mean_set(SampleInfo.Region == "CA3" & SampleInfo.Condition == "CTL",:),proteinopaths(SampleInfo.Region == "CA3"  & SampleInfo.Condition == "CTL",:)),'type','Spearman','testR','on','varNames',var_names);
title('Correlation Plot CTL in CA3')

figure;
corrplot(horzcat(mean_set(SampleInfo.Region == "DG" & SampleInfo.Condition == "CTL",:),proteinopaths(SampleInfo.Region == "DG"  & SampleInfo.Condition == "CTL",:)),'type','Spearman','testR','on','varNames',var_names);
title('Correlation Plot CTL in DG')

%AD
figure;
corrplot(horzcat(mean_set(SampleInfo.Region == "CA1" & SampleInfo.Condition == "AD",:),proteinopaths(SampleInfo.Region == "CA1"  & SampleInfo.Condition == "AD",:)),'type','Spearman','testR','on','varNames',var_names);
title('Correlation Plot AD in CA1')

figure;
corrplot(horzcat(mean_set(SampleInfo.Region == "CA3" & SampleInfo.Condition == "AD",:),proteinopaths(SampleInfo.Region == "CA3"  & SampleInfo.Condition == "AD",:)),'type','Spearman','testR','on','varNames',var_names);
title('Correlation Plot AD in CA3')

figure;
corrplot(horzcat(mean_set(SampleInfo.Region == "DG" & SampleInfo.Condition == "AD",:),proteinopaths(SampleInfo.Region == "DG"  & SampleInfo.Condition == "AD",:)),'type','Spearman','testR','on','varNames',var_names);
title('Correlation Plot AD in DG')

%DLB
figure;
corrplot(horzcat(mean_set(SampleInfo.Region == "CA1" & SampleInfo.Condition == "DLB",:),proteinopaths(SampleInfo.Region == "CA1"  & SampleInfo.Condition == "DLB",:)),'type','Spearman','testR','on','varNames',var_names);
title('Correlation Plot DLB in CA1')

figure;
corrplot(horzcat(mean_set(SampleInfo.Region == "CA3" & SampleInfo.Condition == "DLB",:),proteinopaths(SampleInfo.Region == "CA3"  & SampleInfo.Condition == "DLB",:)),'type','Spearman','testR','on','varNames',var_names);
title('Correlation Plot DLB in CA3')

figure;
corrplot(horzcat(mean_set(SampleInfo.Region == "DG" & SampleInfo.Condition == "DLB",:),proteinopaths(SampleInfo.Region == "DG"  & SampleInfo.Condition == "DLB",:)),'type','Pearson','testR','on','varNames',var_names);
title('Correlation Plot DLB in DG')

% EXTRA INFO CORRELATION

var_names = [feat_names, "Tau", "ALoad", "Psyn","PMD","FixTime","Age","N Slices","Years Sympt"];
corrset = horzcat(mean_set,SampleInfo.Tau,SampleInfo.ALoad,SampleInfo.Psyn,SampleInfo.PMD,SampleInfo.FixationTime,SampleInfo.Age,metadata.Z,SampleInfo.YearsWithSympt);

figure; corrplot(corrset,'type','Spearman','testR','on','varNames',var_names)


%% PVALS CLUST FEATS

for zz = 1 : 16
    for ii = 1 : n_clusts
        for jj = 1 : n_clusts
           if ii < jj
            group1 = cell_set(idx == ii,zz);
            group2 = cell_set(idx == jj,zz);
            p = ranksum(group1,group2);
                if p < 0.05
                    disp(strcat("Feature ", feat_names(zz), " - Cluster ", num2str(ii), " vs Cluster ", num2str(jj), " : ", num2str(p)));
                end
           end
        end
    end
end

%% PVALS CAT

%Region wise
for zz = 1 : length(feat_names)
    for ii = ["CA1" "CA3" "DG"]
        for jj = ["CA1" "CA3" "DG"]
            for kk = ["CTL" "AD" "DLB"]
                if ii ~= jj
                    group1 = mean_set(SampleInfo.Region == ii & SampleInfo.Condition == kk,zz);
                    group2 = mean_set(SampleInfo.Region == jj & SampleInfo.Condition == kk,zz);
                    p = ranksum(group1,group2);
                    if p < 0.05
                        disp(strcat(feat_names(zz), " - Region ", num2str(ii), " vs Region ", num2str(jj), " in ", kk ," : ", num2str(p)));
                        
                    end
                end
            end
        end
    end
end

%Condition Wise
for zz = 1 : length(feat_names)
    for ii = ["CTL" "AD" "DLB"]
        for jj = ["CTL" "AD" "DLB"]
            for kk = ["CA1" "CA3" "DG"]
                if ii ~= jj
                    group1 = mean_set(SampleInfo.Condition == ii & SampleInfo.Region == kk,zz);
                    group2 = mean_set(SampleInfo.Condition == jj & SampleInfo.Region == kk,zz);
                    p = ranksum(group1,group2);
                    if p < 0.05
                        disp(strcat(feat_names(zz), " - Condition ", num2str(ii), " vs Condition ", num2str(jj), " in ", kk ," : ", num2str(p)));
                    end
                end
            end
        end
    end
end

%Tau
for ii = ["CTL" "AD" "DLB"]
    for jj = ["CTL" "AD" "DLB"]
        for kk = ["CA1" "CA3" "DG"]
            if ii ~= jj
                group1 = SampleInfo.Tau(SampleInfo.Condition == ii & SampleInfo.Region == kk);
                group2 = SampleInfo.Tau(SampleInfo.Condition == jj & SampleInfo.Region == kk);
                p = ranksum(group1,group2);
                if p < 0.05
                    disp(strcat("Tau - Condition ", num2str(ii), " vs Condition ", num2str(jj), " in ", kk ," : ", num2str(p)));
                end
            end
        end
    end
end

%Aload
for ii = ["CTL" "AD" "DLB"]
    for jj = ["CTL" "AD" "DLB"]
        for kk = ["CA1" "CA3" "DG"]
            if ii ~= jj
                group1 = SampleInfo.ALoad(SampleInfo.Condition == ii & SampleInfo.Region == kk);
                group2 = SampleInfo.ALoad(SampleInfo.Condition == jj & SampleInfo.Region == kk);
                p = ranksum(group1,group2);
                if p < 0.05
                    disp(strcat("Amyloid Load - Condition ", num2str(ii), " vs Condition ", num2str(jj), " in ", kk ," : ", num2str(p)));
                end
            end
        end
    end
end

%Psyn
for ii = ["CTL" "AD" "DLB"]
    for jj = ["CTL" "AD" "DLB"]
        for kk = ["CA1" "CA3" "DG"]
            if ii ~= jj
                group1 = SampleInfo.Psyn(SampleInfo.Condition == ii & SampleInfo.Region == kk);
                group2 = SampleInfo.Psyn(SampleInfo.Condition == jj & SampleInfo.Region == kk);
                p = ranksum(group1,group2);
                if p < 0.05
                    disp(strcat("Psyn Load - Condition ", num2str(ii), " vs Condition ", num2str(jj), " in ", kk ," : ", num2str(p)));
                end
            end
        end
    end
end

%PVAL REGION WISE ON SPECIFIC CONDITION

%Tau
for ii = ["CA1" "CA3" "DG"]
    for jj = ["CA1" "CA3" "DG"]
        for kk = ["CTL" "AD" "DLB"]
            if ii ~= jj
                group1 = SampleInfo.Tau(SampleInfo.Region == ii & SampleInfo.Condition == kk);
                group2 = SampleInfo.Tau(SampleInfo.Region == jj & SampleInfo.Condition == kk);
                p = ranksum(group1,group2);
                if p < 0.05
                    disp(strcat("Tau - Region ", num2str(ii), " vs Region ", num2str(jj), " in ", kk ," : ", num2str(p)));
                end
            end
        end
    end
end

%Aload
for ii = ["CA1" "CA3" "DG"]
    for jj = ["CA1" "CA3" "DG"]
        for kk = ["CTL" "AD" "DLB"]
            if ii ~= jj
                group1 = SampleInfo.ALoad(SampleInfo.Region == ii & SampleInfo.Condition == kk);
                group2 = SampleInfo.ALoad(SampleInfo.Region == jj & SampleInfo.Condition == kk);
                p = ranksum(group1,group2);
                if p < 0.05
                    disp(strcat("Amyloid load - Region ", num2str(ii), " vs Region ", num2str(jj), " in ", kk ," : ", num2str(p)));
                end
            end
        end
    end
end

%Psyn
for ii = ["CA1" "CA3" "DG"]
    for jj = ["CA1" "CA3" "DG"]
        for kk = ["CTL" "AD" "DLB"]
            if ii ~= jj
                group1 = SampleInfo.Psyn(SampleInfo.Region == ii & SampleInfo.Condition == kk);
                group2 = SampleInfo.Psyn(SampleInfo.Region == jj & SampleInfo.Condition == kk);
                p = ranksum(group1,group2);
                if p < 0.05
                    disp(strcat("Psyn - Region ", num2str(ii), " vs Region ", num2str(jj), " in ", kk ," : ", num2str(p)));
                end
            end
        end
    end
end

%Gender Wise
for ii = ["CTL" "AD" "DLB"]
    for kk = ["CA1" "CA3" "DG"]
        for zz = 1 : length(feat_names)
            
                group1 = mean_set(SampleInfo.Condition == ii & SampleInfo.Region == kk & SampleInfo.Gender == 'M',zz);
                group2 = mean_set(SampleInfo.Condition == ii & SampleInfo.Region == kk & SampleInfo.Gender == 'F',zz);
                p = ranksum(group1,group2);
                if p < 0.05
                    disp(strcat(feat_names(zz)," M vs F - in ", num2str(ii), " ", num2str(kk) ," : ", num2str(p)));
                end
            
        end
    end
end

%% Barplot coloring example

cluster_colors{1} = [121 125 98]./256;
cluster_colors{2} = [217 174 148]./256;
cluster_colors{3} = [155 155 122]./256;
cluster_colors{4} = [255 203 105]./256;
cluster_colors{5} = [208 140 96]./256;
cluster_colors{6} = [153 123 102]./256;

figure; b = bar(cell_per_samp,'stacked');
b(1).FaceColor = cluster_colors{1};
b(2).FaceColor = cluster_colors{2};
b(3).FaceColor = cluster_colors{3};
b(4).FaceColor = cluster_colors{4};
b(5).FaceColor = cluster_colors{5};
b(6).FaceColor = cluster_colors{6};

%% HIERARCHICAL CLUSTERING

rng(1)

tree = linkage(cell_set_norm,'ward');

max_clust = 7;
n_clusts = 7;
idx = cluster(tree,'maxclust',max_clust);

% figure; hist(idx,max_clust);
% title("number of cells per cluster")
% 
% figure; 
% [H,nodes,orig]  = dendrogram(tree,'ColorThreshold',16);
% set(H,'LineWidth',3)

%Create centroid and recalculate distance
centroid = {};

for ii = 1 : max_clust
   centroid{ii} = mean(cell_set_norm(idx==ii,:));
end

distClust = zeros(length(idx),1);

 for ii = 1 : length(idx)
     distClust(ii) = sqrt(sum((centroid{idx(ii)}-cell_set_norm(ii,:)).^2));
 end
 
%% RENDERING STRUCTURES PER CLUSTER

imgXclust = 36;

for kk = 1 : max_clust
    tempClust = distClust;
    tempClust(idx~=kk) = inf;
    [~, indexes] = mink(tempClust,imgXclust);
    gridVols(indexes,volumes);
    title(num2str(kk))
end

%% UMAP PLOTS

rng(1)

% CLUSTER WISE

umap_data = run_umap(cell_set_norm,'method','MEX');

figure;
hold on
for ii = 1 : max_clust
    scatter(umap_data(idx==ii,1),umap_data(idx==ii,2),7,'filled')
end 

hold off
legend('Cluster 1','Cluster 2','Cluster 3','Cluster 4','Cluster 5','Cluster 6','Cluster 7');

% FEAT VAL ON UMAP

for ii =  1 : n_feats
   figure;
   scatter(umap_data(:,1),umap_data(:,2),10,cell_set(:,ii),'filled');
   title(feat_names(ii))
end

% ALL REGONS SEVERITY

figure;
hold on
scatter(umap_data(:,1),umap_data(:,2),5,"filled"','MarkerFaceColor',[0.8 .8 .8])
scatter(umap_data(getSampsClassif(SampleInfo,1,"CTL",startSamp,""),1),umap_data(getSampsClassif(SampleInfo,1,"CTL",startSamp,""),2),50,"filled",'s','MarkerFaceColor',[255 159 28]./255)
scatter(umap_data(getSampsClassif(SampleInfo,2,"CTL",startSamp,""),1),umap_data(getSampsClassif(SampleInfo,2,"CTL",startSamp,""),2),25,"filled",'c','MarkerFaceColor',[46 196 182]./255)
scatter(umap_data(getSampsClassif(SampleInfo,3,"CTL",startSamp,""),1),umap_data(getSampsClassif(SampleInfo,3,"CTL",startSamp,""),2),25,"filled",'d','MarkerFaceColor',[231 29 54]./255)
hold off
legend("Background","Severity1","Severity2","Severity3");
title("Cell in CTL with different Severity")

figure;
hold on
scatter(umap_data(:,1),umap_data(:,2),5,"filled"','MarkerFaceColor',[0.8 .8 .8])
scatter(umap_data(getSampsClassif(SampleInfo,1,"AD",startSamp,""),1),umap_data(getSampsClassif(SampleInfo,1,"AD",startSamp,""),2),50,"filled",'s','MarkerFaceColor',[255 159 28]./255)
scatter(umap_data(getSampsClassif(SampleInfo,2,"AD",startSamp,""),1),umap_data(getSampsClassif(SampleInfo,2,"AD",startSamp,""),2),25,"filled",'c','MarkerFaceColor',[46 196 182]./255)
scatter(umap_data(getSampsClassif(SampleInfo,3,"AD",startSamp,""),1),umap_data(getSampsClassif(SampleInfo,3,"AD",startSamp,""),2),25,"filled",'d','MarkerFaceColor',[231 29 54]./255)
hold off
legend("Background","Severity1","Severity2","Severity3");
title("Cell in AD with different Severity")

figure;
hold on
scatter(umap_data(:,1),umap_data(:,2),5,"filled"','MarkerFaceColor',[0.8 .8 .8])
scatter(umap_data(getSampsClassif(SampleInfo,1,"DLB",startSamp,""),1),umap_data(getSampsClassif(SampleInfo,1,"DLB",startSamp,""),2),50,"filled",'s','MarkerFaceColor',[255 159 28]./255)
scatter(umap_data(getSampsClassif(SampleInfo,2,"DLB",startSamp,""),1),umap_data(getSampsClassif(SampleInfo,2,"DLB",startSamp,""),2),25,"filled",'c','MarkerFaceColor',[46 196 182]./255)
scatter(umap_data(getSampsClassif(SampleInfo,3,"DLB",startSamp,""),1),umap_data(getSampsClassif(SampleInfo,3,"DLB",startSamp,""),2),25,"filled",'d','MarkerFaceColor',[231 29 54]./255)
hold off
legend("Background","Severity1","Severity2","Severity3");
title("Cell in DLB with different Severity")

% CA1 SEVERITY

figure;
hold on
scatter(umap_data(:,1),umap_data(:,2),5,"filled"','MarkerFaceColor',[0.8 .8 .8])
scatter(umap_data(getSampsClassif(SampleInfo,1,"CTL",startSamp,"CA1"),1),umap_data(getSampsClassif(SampleInfo,1,"CTL",startSamp,"CA1"),2),50,"filled",'s','MarkerFaceColor',[255 159 28]./255)
scatter(umap_data(getSampsClassif(SampleInfo,2,"CTL",startSamp,"CA1"),1),umap_data(getSampsClassif(SampleInfo,2,"CTL",startSamp,"CA1"),2),25,"filled",'c','MarkerFaceColor',[46 196 182]./255)
scatter(umap_data(getSampsClassif(SampleInfo,3,"CTL",startSamp,"CA1"),1),umap_data(getSampsClassif(SampleInfo,3,"CTL",startSamp,"CA1"),2),25,"filled",'d','MarkerFaceColor',[231 29 54]./255)
hold off
legend("Background","Severity1","Severity2","Severity3");
title("Cell in CTL CA1 with different Severity")

figure;
hold on
scatter(umap_data(:,1),umap_data(:,2),5,"filled"','MarkerFaceColor',[0.8 .8 .8])
scatter(umap_data(getSampsClassif(SampleInfo,1,"AD",startSamp,"CA1"),1),umap_data(getSampsClassif(SampleInfo,1,"AD",startSamp,"CA1"),2),50,"filled",'s','MarkerFaceColor',[255 159 28]./255)
scatter(umap_data(getSampsClassif(SampleInfo,2,"AD",startSamp,"CA1"),1),umap_data(getSampsClassif(SampleInfo,2,"AD",startSamp,"CA1"),2),25,"filled",'c','MarkerFaceColor',[46 196 182]./255)
scatter(umap_data(getSampsClassif(SampleInfo,3,"AD",startSamp,"CA1"),1),umap_data(getSampsClassif(SampleInfo,3,"AD",startSamp,"CA1"),2),25,"filled",'d','MarkerFaceColor',[231 29 54]./255)
hold off
legend("Background","Severity1","Severity2","Severity3");
title("Cell in AD CA1 with different Severity")

figure;
hold on
scatter(umap_data(:,1),umap_data(:,2),5,"filled"','MarkerFaceColor',[0.8 .8 .8])
scatter(umap_data(getSampsClassif(SampleInfo,1,"DLB",startSamp,"CA1"),1),umap_data(getSampsClassif(SampleInfo,1,"DLB",startSamp,"CA1"),2),50,"filled",'s','MarkerFaceColor',[255 159 28]./255)
scatter(umap_data(getSampsClassif(SampleInfo,2,"DLB",startSamp,"CA1"),1),umap_data(getSampsClassif(SampleInfo,2,"DLB",startSamp,"CA1"),2),25,"filled",'c','MarkerFaceColor',[46 196 182]./255)
scatter(umap_data(getSampsClassif(SampleInfo,3,"DLB",startSamp,"CA1"),1),umap_data(getSampsClassif(SampleInfo,3,"DLB",startSamp,"CA1"),2),25,"filled",'d','MarkerFaceColor',[231 29 54]./255)
hold off
legend("Background","Severity1","Severity2","Severity3");
title("Cell in DLB CA1 with different Severity")

% CA3 SEVERITY

figure;
hold on
scatter(umap_data(:,1),umap_data(:,2),5,"filled"','MarkerFaceColor',[0.8 .8 .8])
scatter(umap_data(getSampsClassif(SampleInfo,1,"CTL",startSamp,"CA3"),1),umap_data(getSampsClassif(SampleInfo,1,"CTL",startSamp,"CA3"),2),50,"filled",'s','MarkerFaceColor',[255 159 28]./255)
scatter(umap_data(getSampsClassif(SampleInfo,2,"CTL",startSamp,"CA3"),1),umap_data(getSampsClassif(SampleInfo,2,"CTL",startSamp,"CA3"),2),25,"filled",'c','MarkerFaceColor',[46 196 182]./255)
scatter(umap_data(getSampsClassif(SampleInfo,3,"CTL",startSamp,"CA3"),1),umap_data(getSampsClassif(SampleInfo,3,"CTL",startSamp,"CA3"),2),25,"filled",'d','MarkerFaceColor',[231 29 54]./255)
hold off
legend("Background","Severity1","Severity2","Severity3");
title("Cell in CTL CA3 with different Severity")

figure;
hold on
scatter(umap_data(:,1),umap_data(:,2),5,"filled"','MarkerFaceColor',[0.8 .8 .8])
scatter(umap_data(getSampsClassif(SampleInfo,1,"AD",startSamp,"CA3"),1),umap_data(getSampsClassif(SampleInfo,1,"AD",startSamp,"CA3"),2),50,"filled",'s','MarkerFaceColor',[255 159 28]./255)
scatter(umap_data(getSampsClassif(SampleInfo,2,"AD",startSamp,"CA3"),1),umap_data(getSampsClassif(SampleInfo,2,"AD",startSamp,"CA3"),2),25,"filled",'c','MarkerFaceColor',[46 196 182]./255)
scatter(umap_data(getSampsClassif(SampleInfo,3,"AD",startSamp,"CA3"),1),umap_data(getSampsClassif(SampleInfo,3,"AD",startSamp,"CA3"),2),25,"filled",'d','MarkerFaceColor',[231 29 54]./255)
hold off
legend("Background","Severity1","Severity2","Severity3");
title("Cell in AD CA3 with different Severity")

figure;
hold on
scatter(umap_data(:,1),umap_data(:,2),5,"filled"','MarkerFaceColor',[0.8 .8 .8])
scatter(umap_data(getSampsClassif(SampleInfo,1,"DLB",startSamp,"CA3"),1),umap_data(getSampsClassif(SampleInfo,1,"DLB",startSamp,"CA3"),2),50,"filled",'s','MarkerFaceColor',[255 159 28]./255)
scatter(umap_data(getSampsClassif(SampleInfo,2,"DLB",startSamp,"CA3"),1),umap_data(getSampsClassif(SampleInfo,2,"DLB",startSamp,"CA3"),2),25,"filled",'c','MarkerFaceColor',[46 196 182]./255)
scatter(umap_data(getSampsClassif(SampleInfo,3,"DLB",startSamp,"CA3"),1),umap_data(getSampsClassif(SampleInfo,3,"DLB",startSamp,"CA3"),2),25,"filled",'d','MarkerFaceColor',[231 29 54]./255)
hold off
legend("Background","Severity1","Severity2","Severity3");
title("Cell in DLB CA3 with different Severity")

% DG SEVERITY

figure;
hold on
scatter(umap_data(:,1),umap_data(:,2),5,"filled"','MarkerFaceColor',[0.8 .8 .8])
scatter(umap_data(getSampsClassif(SampleInfo,1,"CTL",startSamp,"DG"),1),umap_data(getSampsClassif(SampleInfo,1,"CTL",startSamp,"DG"),2),50,"filled",'s','MarkerFaceColor',[255 159 28]./255)
scatter(umap_data(getSampsClassif(SampleInfo,2,"CTL",startSamp,"DG"),1),umap_data(getSampsClassif(SampleInfo,2,"CTL",startSamp,"DG"),2),25,"filled",'c','MarkerFaceColor',[46 196 182]./255)
scatter(umap_data(getSampsClassif(SampleInfo,3,"CTL",startSamp,"DG"),1),umap_data(getSampsClassif(SampleInfo,3,"CTL",startSamp,"DG"),2),25,"filled",'d','MarkerFaceColor',[231 29 54]./255)
hold off
legend("Background","Severity1","Severity2","Severity3");
title("Cell in CTL DG with different Severity")

figure;
hold on
scatter(umap_data(:,1),umap_data(:,2),5,"filled"','MarkerFaceColor',[0.8 .8 .8])
scatter(umap_data(getSampsClassif(SampleInfo,1,"AD",startSamp,"DG"),1),umap_data(getSampsClassif(SampleInfo,1,"AD",startSamp,"DG"),2),50,"filled",'s','MarkerFaceColor',[255 159 28]./255)
scatter(umap_data(getSampsClassif(SampleInfo,2,"AD",startSamp,"DG"),1),umap_data(getSampsClassif(SampleInfo,2,"AD",startSamp,"DG"),2),25,"filled",'c','MarkerFaceColor',[46 196 182]./255)
scatter(umap_data(getSampsClassif(SampleInfo,3,"AD",startSamp,"DG"),1),umap_data(getSampsClassif(SampleInfo,3,"AD",startSamp,"DG"),2),25,"filled",'d','MarkerFaceColor',[231 29 54]./255)
hold off
legend("Background","Severity1","Severity2","Severity3");
title("Cell in AD DG with different Severity")

figure;
hold on
scatter(umap_data(:,1),umap_data(:,2),5,"filled"','MarkerFaceColor',[0.8 .8 .8])
scatter(umap_data(getSampsClassif(SampleInfo,1,"DLB",startSamp,"DG"),1),umap_data(getSampsClassif(SampleInfo,1,"DLB",startSamp,"DG"),2),50,"filled",'s','MarkerFaceColor',[255 159 28]./255)
scatter(umap_data(getSampsClassif(SampleInfo,2,"DLB",startSamp,"DG"),1),umap_data(getSampsClassif(SampleInfo,2,"DLB",startSamp,"DG"),2),25,"filled",'c','MarkerFaceColor',[46 196 182]./255)
scatter(umap_data(getSampsClassif(SampleInfo,3,"DLB",startSamp,"DG"),1),umap_data(getSampsClassif(SampleInfo,3,"DLB",startSamp,"DG"),2),25,"filled",'d','MarkerFaceColor',[231 29 54]./255)
hold off
legend("Background","Severity1","Severity2","Severity3");
title("Cell in DLB DG with different Severity")

% PLOT SEVERITY INDEX

sev_metric = zeros(length(cell_set),1);

for ii = 1 : n_samps
   sev_metric(startSamp(ii):startSamp(ii+1)-1) = SampleInfo.Severity(ii); 
end

tau_metric = zeros(length(cell_set),1);

for ii = 1 : n_samps
   tau_metric(startSamp(ii):startSamp(ii+1)-1) = SampleInfo.Tau(ii); 
end

psyn_metric = zeros(length(cell_set),1);

for ii = 1 : n_samps
   psyn_metric(startSamp(ii):startSamp(ii+1)-1) = SampleInfo.Psyn(ii); 
end

aload_metric = zeros(length(cell_set),1);

for ii = 1 : n_samps
   aload_metric(startSamp(ii):startSamp(ii+1)-1) = SampleInfo.ALoad(ii); 
end

%SEVERITY
printSeverityOverUMAP(SampleInfo,0,"CTL",startSamp,"",umap_data,sev_metric,"")
printSeverityOverUMAP(SampleInfo,0,"AD",startSamp,"",umap_data,sev_metric,"")
printSeverityOverUMAP(SampleInfo,0,"DLB",startSamp,"",umap_data,sev_metric,"")

printSeverityOverUMAP(SampleInfo,0,"CTL",startSamp,"CA1",umap_data,sev_metric,"")
printSeverityOverUMAP(SampleInfo,0,"CTL",startSamp,"CA3",umap_data,sev_metric,"")
printSeverityOverUMAP(SampleInfo,0,"CTL",startSamp,"DG",umap_data,sev_metric,"")

printSeverityOverUMAP(SampleInfo,0,"AD",startSamp,"CA1",umap_data,sev_metric,"")
printSeverityOverUMAP(SampleInfo,0,"AD",startSamp,"CA3",umap_data,sev_metric,"")
printSeverityOverUMAP(SampleInfo,0,"AD",startSamp,"DG",umap_data,sev_metric,"")

printSeverityOverUMAP(SampleInfo,0,"DLB",startSamp,"CA1",umap_data,sev_metric,"")
printSeverityOverUMAP(SampleInfo,0,"DLB",startSamp,"CA3",umap_data,sev_metric,"")
printSeverityOverUMAP(SampleInfo,0,"DLB",startSamp,"DG",umap_data,sev_metric,"")

%TAU
printSeverityOverUMAP(SampleInfo,0,"CTL",startSamp,"",umap_data,tau_metric,"TAU")
printSeverityOverUMAP(SampleInfo,0,"AD",startSamp,"",umap_data,tau_metric,"TAU")
printSeverityOverUMAP(SampleInfo,0,"DLB",startSamp,"",umap_data,tau_metric,"TAU")

printSeverityOverUMAP(SampleInfo,0,"CTL",startSamp,"CA1",umap_data,tau_metric,"TAU")
printSeverityOverUMAP(SampleInfo,0,"CTL",startSamp,"CA3",umap_data,tau_metric,"TAU")
printSeverityOverUMAP(SampleInfo,0,"CTL",startSamp,"DG",umap_data,tau_metric,"TAU")

printSeverityOverUMAP(SampleInfo,0,"AD",startSamp,"CA1",umap_data,tau_metric,"TAU")
printSeverityOverUMAP(SampleInfo,0,"AD",startSamp,"CA3",umap_data,tau_metric,"TAU")
printSeverityOverUMAP(SampleInfo,0,"AD",startSamp,"DG",umap_data,tau_metric,"TAU")

printSeverityOverUMAP(SampleInfo,0,"DLB",startSamp,"CA1",umap_data,tau_metric,"TAU")
printSeverityOverUMAP(SampleInfo,0,"DLB",startSamp,"CA3",umap_data,tau_metric,"TAU")
printSeverityOverUMAP(SampleInfo,0,"DLB",startSamp,"DG",umap_data,tau_metric,"TAU")

%PSYN

printSeverityOverUMAP(SampleInfo,0,"CTL",startSamp,"",umap_data,psyn_metric,"psyn")
printSeverityOverUMAP(SampleInfo,0,"AD",startSamp,"",umap_data,psyn_metric,"psyn")
printSeverityOverUMAP(SampleInfo,0,"DLB",startSamp,"",umap_data,psyn_metric,"psyn")

printSeverityOverUMAP(SampleInfo,0,"CTL",startSamp,"CA1",umap_data,psyn_metric,"psyn")
printSeverityOverUMAP(SampleInfo,0,"CTL",startSamp,"CA3",umap_data,psyn_metric,"psyn")
printSeverityOverUMAP(SampleInfo,0,"CTL",startSamp,"DG",umap_data,psyn_metric,"psyn")

printSeverityOverUMAP(SampleInfo,0,"AD",startSamp,"CA1",umap_data,psyn_metric,"psyn")
printSeverityOverUMAP(SampleInfo,0,"AD",startSamp,"CA3",umap_data,psyn_metric,"psyn")
printSeverityOverUMAP(SampleInfo,0,"AD",startSamp,"DG",umap_data,psyn_metric,"psyn")

printSeverityOverUMAP(SampleInfo,0,"DLB",startSamp,"CA1",umap_data,psyn_metric,"psyn")
printSeverityOverUMAP(SampleInfo,0,"DLB",startSamp,"CA3",umap_data,psyn_metric,"psyn")
printSeverityOverUMAP(SampleInfo,0,"DLB",startSamp,"DG",umap_data,psyn_metric,"psyn")

%ALOAD

printSeverityOverUMAP(SampleInfo,0,"CTL",startSamp,"",umap_data,aload_metric,"aload")
printSeverityOverUMAP(SampleInfo,0,"AD",startSamp,"",umap_data,aload_metric,"aload")
printSeverityOverUMAP(SampleInfo,0,"DLB",startSamp,"",umap_data,aload_metric,"aload")

printSeverityOverUMAP(SampleInfo,0,"CTL",startSamp,"CA1",umap_data,aload_metric,"aload")
printSeverityOverUMAP(SampleInfo,0,"CTL",startSamp,"CA3",umap_data,aload_metric,"aload")
printSeverityOverUMAP(SampleInfo,0,"CTL",startSamp,"DG",umap_data,aload_metric,"aload")

printSeverityOverUMAP(SampleInfo,0,"AD",startSamp,"CA1",umap_data,aload_metric,"aload")
printSeverityOverUMAP(SampleInfo,0,"AD",startSamp,"CA3",umap_data,aload_metric,"aload")
printSeverityOverUMAP(SampleInfo,0,"AD",startSamp,"DG",umap_data,aload_metric,"aload")

printSeverityOverUMAP(SampleInfo,0,"DLB",startSamp,"CA1",umap_data,aload_metric,"aload")
printSeverityOverUMAP(SampleInfo,0,"DLB",startSamp,"CA3",umap_data,aload_metric,"aload")
printSeverityOverUMAP(SampleInfo,0,"DLB",startSamp,"DG",umap_data,aload_metric,"aload")

%% PLOTS FOR CLUSTERS

n_clusts = max(idx);

clust_count_samp = zeros(n_samps,n_clusts);
count_clust_cond = zeros(n_clusts,3);
count_clust_region = zeros(n_clusts,3);
count_clust_CR = zeros(n_clusts,3,3);

condition_idx = 51;
region_idx = 52;

for ii = 1 : length(data)
    count_clust_cond(idx(ii),data(ii,condition_idx)) = count_clust_cond(idx(ii),data(ii,condition_idx)) + 1;
    count_clust_region(idx(ii),data(ii,region_idx)) = count_clust_region(idx(ii),data(ii,region_idx)) + 1;
    count_clust_CR(idx(ii),data(ii,condition_idx),data(ii,region_idx)) = count_clust_CR(idx(ii),data(ii,condition_idx),data(ii,region_idx)) + 1;
end

count_clust_CR = horzcat(count_clust_CR(:,:,1),count_clust_CR(:,:,2),count_clust_CR(:,:,3));

for ii = 1 : n_samps
    for jj = 1 : n_clusts
       clust_count_samp(ii,jj) = sum(idx(startSamp(ii):startSamp(ii+1)-1)==jj); 
    end
end

clust_count_samp_norm = clust_count_samp;

for ii = 1 : 87
   clust_count_samp_norm(ii,:) =  clust_count_samp_norm(ii,:)./sum(clust_count_samp_norm(ii,:));
end

count_clust_cond_norm = count_clust_cond;
count_clust_cond_norm(:,1) = count_clust_cond_norm(:,1)./sum(data(:,condition_idx)==1);
count_clust_cond_norm(:,2) = count_clust_cond_norm(:,2)./sum(data(:,condition_idx)==2);
count_clust_cond_norm(:,3) = count_clust_cond_norm(:,3)./sum(data(:,condition_idx)==3);

count_clust_region_norm = count_clust_region;
count_clust_region_norm(:,1) = count_clust_region_norm(:,1)./sum(data(:,region_idx)==1);
count_clust_region_norm(:,2) = count_clust_region_norm(:,2)./sum(data(:,region_idx)==2);
count_clust_region_norm(:,3) = count_clust_region_norm(:,3)./sum(data(:,region_idx)==3);

count_clust_CR_norm = count_clust_CR;
count_clust_CR_norm(:,1) = count_clust_CR_norm(:,1)./sum(data(:,condition_idx)==1&data(:,region_idx)==1);
count_clust_CR_norm(:,2) = count_clust_CR_norm(:,2)./sum(data(:,condition_idx)==2&data(:,region_idx)==1);
count_clust_CR_norm(:,3) = count_clust_CR_norm(:,3)./sum(data(:,condition_idx)==3&data(:,region_idx)==1);
count_clust_CR_norm(:,4) = count_clust_CR_norm(:,4)./sum(data(:,condition_idx)==1&data(:,region_idx)==2);
count_clust_CR_norm(:,5) = count_clust_CR_norm(:,5)./sum(data(:,condition_idx)==2&data(:,region_idx)==2);
count_clust_CR_norm(:,6) = count_clust_CR_norm(:,6)./sum(data(:,condition_idx)==3&data(:,region_idx)==2);
count_clust_CR_norm(:,7) = count_clust_CR_norm(:,7)./sum(data(:,condition_idx)==1&data(:,region_idx)==3);
count_clust_CR_norm(:,8) = count_clust_CR_norm(:,8)./sum(data(:,condition_idx)==2&data(:,region_idx)==3);
count_clust_CR_norm(:,9) = count_clust_CR_norm(:,9)./sum(data(:,condition_idx)==3&data(:,region_idx)==3);

figure;
bar(count_clust_cond)
legend('CTL','AD','DLB')
title('Cell count per cluster by condition')

figure;
bar(count_clust_region)
legend('CA1','CA3','DG')
title('Cell count per cluster by region')


figure;
bar(count_clust_cond_norm)
legend('CTL','AD','DLB')
title('Normalized cell count per cluster by condition')

figure;
bar(count_clust_region_norm)
legend('CA1','CA3','DG')
title('Normalized cell count per cluster by region')

X = categorical({'CTL CA1','AD CA1','DLB CA1','CTL CA3','AD CA3','DLB CA3','CTL DG','AD DG','DLB DG'});
X = reordercats(X,{'CTL CA1','AD CA1','DLB CA1','CTL CA3','AD CA3','DLB CA3','CTL DG','AD DG','DLB DG'});


figure;
bar(X,count_clust_CR','stacked')
legend(num2str([1:n_clusts]'));
title('Cell count per cluster by region and condition')

figure;
bar(X,count_clust_CR_norm','stacked')
legend(num2str([1:n_clusts]'));

title('Normalized cell count per cluster by region and condition')


figure;
bar(X,count_clust_CR_norm')
legend(num2str([1:n_clusts]'));
grid on
title('Normalized cell count per cluster by region and condition')

% nomalized cell count with CTL as baseline

CR_CTL = count_clust_CR_norm;
CR_CTL(:,[1 2 3]) = CR_CTL(:,[1 2 3]) - CR_CTL(:,[1]);  
CR_CTL(:,[4 5 6]) = CR_CTL(:,[4 5 6]) - CR_CTL(:,[4]);  
CR_CTL(:,[7 8 9]) = CR_CTL(:,[7 8 9]) - CR_CTL(:,[7]);

figure;
bar(X,CR_CTL')
legend(num2str([1:n_clusts]'));

title('Normalized cell count per cluster by region and condition (CTL baseline)')


%Assign proteinopath values to cells and get distribution per cluster

tau_cell = zeros(length(cell_set),1);
aload_cell = zeros(length(cell_set),1);
psyn_cell = zeros(length(cell_set),1);
severity_cell = zeros(length(cell_set),1);
for ii = 1 : length(startSamp)-1
   tau_cell(startSamp(ii):startSamp(ii+1) - 1) = SampleInfo.Tau(ii);
   aload_cell(startSamp(ii):startSamp(ii+1) - 1) = SampleInfo.ALoad(ii);
   psyn_cell(startSamp(ii):startSamp(ii+1) - 1) = SampleInfo.Psyn(ii);
   severity_cell(startSamp(ii):startSamp(ii+1) - 1) = SampleInfo.Severity(ii);
end

tau_matrix = nan(length(tau_cell),n_clusts);
for ii = 1 : n_clusts
    temp = tau_cell(idx==ii);
    tau_matrix(1:length(temp),ii) = temp;
end
figure;
bar(mean(tau_matrix,'omitnan'))
title('Average Tau density distribution per cluster')

tau_matrix_reg = nan(length(tau_cell),n_clusts*3);
for rr = 1 : 3
    for ii = 1 : n_clusts
        temp = tau_cell(data(:,52)==rr & idx==ii);
        tau_matrix_reg(1:length(temp),ii+((rr-1)*7)) = temp;
    end
end
figure;
bar(reshape((mean(tau_matrix_reg,'omitnan')),[3,7]))
title('Average Tau density distribution per cluster by region')
legend(num2str([1:n_clusts]'));
xticklabels(["CA1","CA3","DG"])

aload_matrix = nan(length(aload_cell),n_clusts);
for ii = 1: n_clusts
    temp = aload_cell(idx==ii);
    aload_matrix(1:length(temp),ii) = temp;
end
figure;
bar(mean(aload_matrix,'omitnan'))
title('Average Aload density distribution per cluster')

aload_matrix_reg = nan(length(aload_cell),n_clusts*3);
for rr = 1 : 3
    for ii = 1 : n_clusts
        temp = aload_cell(data(:,52)==rr & idx==ii);
        aload_matrix_reg(1:length(temp),ii+((rr-1)*7)) = temp;
    end
end
figure;
bar(reshape((mean(aload_matrix_reg,'omitnan')),[3,7]))
title('Average ALoad density distribution per cluster by region')
legend(num2str([1:n_clusts]'));
xticklabels(["CA1","CA3","DG"])

psyn_matrix = nan(length(psyn_cell),n_clusts);
for ii = 1: n_clusts
    temp = psyn_cell(idx==ii);
    psyn_matrix(1:length(temp),ii) = temp;
end
figure;
bar(mean(psyn_matrix,'omitnan'))
title('Average Psyn density distribution per cluster')

psyn_matrix_reg = nan(length(psyn_cell),n_clusts*3);
for rr = 1 : 3
    for ii = 1 : n_clusts
        temp = psyn_cell(data(:,52)==rr & idx==ii);
        psyn_matrix_reg(1:length(temp),ii+((rr-1)*7)) = temp;
    end
end
figure;
bar(reshape((mean(psyn_matrix_reg,'omitnan')),[3,7]))
title('Average psyn density distribution per cluster by region')
legend(num2str([1:n_clusts]'));
xticklabels(["CA1","CA3","DG"])

severity_matrix = nan(length(severity_cell),n_clusts);
for ii = 1: n_clusts
    temp = severity_cell(idx==ii);
    severity_matrix(1:length(temp),ii) = temp;
end
figure;
bar(mean(severity_matrix,'omitnan'))
title('Average Severity distribution per cluster')

severity_matrix_reg = nan(length(severity_cell),n_clusts*3);
for rr = 1 : 3
    for ii = 1 : n_clusts
        temp = severity_cell(data(:,52)==rr & idx==ii);
        severity_matrix_reg(1:length(temp),ii+((rr-1)*7)) = temp;
    end
end
figure;
bar(reshape((mean(severity_matrix_reg,'omitnan')),[3,7]))
title('Average severity density distribution per cluster by region')
legend(num2str([1:n_clusts]'));
xticklabels(["CA1","CA3","DG"])

%Proteinopathies per region and condition

tau_condreg = getAvgComplete(data,tau_cell,idx,n_clusts);
aload_condreg = getAvgComplete(data,aload_cell,idx,n_clusts);
psyn_condreg = getAvgComplete(data,psyn_cell,idx,n_clusts);
severity_condreg = getAvgComplete(data,severity_cell,idx,n_clusts);


figure;
bar(X,tau_condreg');
title('Tau across different clusters')
legend(num2str([1:n_clusts]'));

figure;
bar(X,aload_condreg');
title('Aload across different clusters')
legend(num2str([1:n_clusts]'));

figure;
bar(X,psyn_condreg');
title('Psyn across different clusters')
legend(num2str([1:n_clusts]'));

figure;
bar(X,severity_condreg');
title('Severity across different clusters')
legend(num2str([1:n_clusts]'));

%pvals EXAMPLE DAVID

for zz = 1 : n_clusts
    
    group1 = tau_cell(idx == zz & data(:,51) == 1 & data(:,52) == 1);
    group2 = tau_cell(idx == zz & data(:,51) == 2 & data(:,52) == 1);
    
    compGroupPlot = nan(max(length(group1),length(group2)),2);
    compGroupPlot(1:length(group1),1) = group1;
    compGroupPlot(1:length(group2),2) = group2;
    
    figure; violinplot(compGroupPlot)
    title(strcat("TAU - CTL CA1 vs AD CA1 in Cluster ",num2str(zz)));
    p = ranksum(group1,group2);
    if p < 0.05
        disp(strcat("Tau - CTL CA1 vs AD CA1 in Cluster ", num2str(zz) ," : ", num2str(p)));
    end
                
end

% Features Violin plot per clust

for ii = 1 : 16
   figure;
   violinplot(cell_set(:,ii),idx);
   title(feat_names(ii))
end

% Gender wise

gender_cell = zeros(1,length(cell_set));

for ii = 1 : length(startSamp)-1
    if SampleInfo.Gender(ii) == 'M'
        gender_cell(startSamp(ii):startSamp(ii+1)-1) = 1;
    else
        gender_cell(startSamp(ii):startSamp(ii+1)-1) = 0;
    end
end

gender_matrix = zeros(2,n_clusts);

for ii = 1 : n_clusts
    gender_matrix(1,ii) = sum(gender_cell(idx==ii)==1);
    gender_matrix(2,ii) = sum(gender_cell(idx==ii)==0);
end

norm_gender_matrix = gender_matrix;
norm_gender_matrix(1,:) = norm_gender_matrix(1,:)./sum(norm_gender_matrix(1,:));
norm_gender_matrix(2,:) = norm_gender_matrix(2,:)./sum(norm_gender_matrix(2,:));

figure; bar(norm_gender_matrix');
title('Gender comparison - normalized count (1 is male)');
legend(num2str([1:n_clusts]'));

%% CORRELATION FOR CLUSTERS

% Correlation of severity with %

corr_clust_mat = horzcat(mean(tau_matrix,'omitnan')',mean(aload_matrix,'omitnan')',mean(psyn_matrix,'omitnan')',mean(severity_matrix,'omitnan')',count_clust_CR_norm);
var_names_clust = ["tau","aload","psyn","severity","ctlca1","adca1"," dlbca1","ctlca3","adca3","dlbca3","ctldg","addg","dlbdg"];
figure;
corrplot(corr_clust_mat,'type','Spearman','testR','on','varNames',var_names_clust);

% Correlation of pp and cluster composition by sample

SampleClustComp = zeros(n_samps,n_clusts);

for ii = 1 : n_samps
    SampleClustComp(ii,:) = hist(idx(startSamp(ii):startSamp(ii+1)-1),[1 2 3 4 5 6 7]); 
end

for ii = 1 : n_samps
    SampleClustCompNorm(ii,:) = SampleClustComp(ii,:) ./ sum(SampleClustComp(ii,:));
end

Corr_Samp_Clust_PP = horzcat(SampleInfo.Tau,SampleInfo.ALoad,SampleInfo.Psyn,SampleInfo.Severity,SampleClustCompNorm);
var_names_clust = ["tau","aload","psyn","severity","1","2","3","4","5","6","7"];

figure;
corrplot(Corr_Samp_Clust_PP,'type','Spearman','testR','on','varNames',var_names_clust);
title("Correlation Sample (all together)")

figure;
corrplot(Corr_Samp_Clust_PP(SampleInfo.Region == "CA1" & SampleInfo.Condition == "CTL",:),'type','Spearman','testR','on','varNames',var_names_clust)
title("Correlation Sample wise between cluster composition (%) and PP - CTL CA1")

figure;
corrplot(Corr_Samp_Clust_PP(SampleInfo.Region == "CA1" & SampleInfo.Condition == "AD",:),'type','Spearman','testR','on','varNames',var_names_clust)
title("Correlation Sample wise between cluster composition (%) and PP - AD CA1")

figure;
corrplot(Corr_Samp_Clust_PP(SampleInfo.Region == "CA1" & SampleInfo.Condition == "DLB",:),'type','Spearman','testR','on','varNames',var_names_clust)
title("Correlation Sample wise between cluster composition (%) and PP - DLB CA1")

figure;
corrplot(Corr_Samp_Clust_PP(SampleInfo.Region == "CA3" & SampleInfo.Condition == "CTL",:),'type','Spearman','testR','on','varNames',var_names_clust)
title("Correlation Sample wise between cluster composition (%) and PP - CTL CA3")

figure;
corrplot(Corr_Samp_Clust_PP(SampleInfo.Region == "CA3" & SampleInfo.Condition == "AD",:),'type','Spearman','testR','on','varNames',var_names_clust)
title("Correlation Sample wise between cluster composition (%) and PP - AD CA3")

figure;
corrplot(Corr_Samp_Clust_PP(SampleInfo.Region == "CA3" & SampleInfo.Condition == "DLB",:),'type','Spearman','testR','on','varNames',var_names_clust)
title("Correlation Sample wise between cluster composition (%) and PP - DLB CA3")

figure;
corrplot(Corr_Samp_Clust_PP(SampleInfo.Region == "DG" & SampleInfo.Condition == "CTL",:),'type','Spearman','testR','on','varNames',var_names_clust)
title("Correlation Sample wise between cluster composition (%) and PP - CTL DG")

figure;
corrplot(Corr_Samp_Clust_PP(SampleInfo.Region == "DG" & SampleInfo.Condition == "AD",:),'type','Spearman','testR','on','varNames',var_names_clust)
title("Correlation Sample wise between cluster composition (%) and PP - AD DG")

figure;
corrplot(Corr_Samp_Clust_PP(SampleInfo.Region == "DG" & SampleInfo.Condition == "DLB",:),'type','Spearman','testR','on','varNames',var_names_clust)
title("Correlation Sample wise between cluster composition (%) and PP - DLB DG")

%Region Wise

figure;
corrplot(Corr_Samp_Clust_PP(SampleInfo.Region == "CA1",:),'type','Spearman','testR','on','varNames',var_names_clust)
title("Correlation Sample wise between cluster composition (%) and PP - CA1")

figure;
corrplot(Corr_Samp_Clust_PP(SampleInfo.Region == "CA3",:),'type','Spearman','testR','on','varNames',var_names_clust)
title("Correlation Sample wise between cluster composition (%) and PP - CA3")

figure;
corrplot(Corr_Samp_Clust_PP(SampleInfo.Region == "DG",:),'type','Spearman','testR','on','varNames',var_names_clust)
title("Correlation Sample wise between cluster composition (%) and PP - DG")


%% PVALS FOR CLUSTERS

%pvals for gender differences in cluster composition (sample wise)

for ii = 5 : 11
    group1 = Corr_Samp_Clust_PP(SampleInfo.Gender == "M",ii);
    group2 = Corr_Samp_Clust_PP(SampleInfo.Gender == "F",ii);
    p = ranksum(group1,group2);
    if p <= 0.05
        disp(strcat("Cluster", num2str(ii-4), " - M vs F : " , num2str(p)));
        
        compGroupPlot = nan(max(length(group1),length(group2)),2);
        compGroupPlot(1:length(group1),1) = group1;
        compGroupPlot(1:length(group2),2) = group2;

        figure; violinplot(compGroupPlot)
        title(strcat("Cluster", num2str(ii-4), " - M vs F : " , num2str(p)));
    end
end

%pvals for gender differences in cluster composition SUB REG (sample wise)

for ii = 5 : 11
    for rr = ["CA1" "CA3" "DG"]
        for cc = ["CTL" "AD" "DLB"]
            
            group1 = Corr_Samp_Clust_PP(SampleInfo.Gender == "M" & SampleInfo.Region == rr & SampleInfo.Condition == cc,ii);
            group2 = Corr_Samp_Clust_PP(SampleInfo.Gender == "F" & SampleInfo.Region == rr & SampleInfo.Condition == cc,ii);
            p = ranksum(group1,group2);
            if p <= 0.05
                disp(strcat(cc," ",rr," - Cluster", num2str(ii-4), " - M vs F : " , num2str(p)));

                compGroupPlot = nan(max(length(group1),length(group2)),2);
                compGroupPlot(1:length(group1),1) = group1;
                compGroupPlot(1:length(group2),2) = group2;

                figure; [~] = violinplot(compGroupPlot);
                title(strcat(cc," ",rr," - Cluster", num2str(ii-4), " - M vs F : " , num2str(p)));
            end
        end
    end
end


% pvals for reg/cond differencs in cluster composition (sample wise)

for ii = 5 : 11
    for rr = ["CA1" "CA3" "DG"]
        for cc1 = ["CTL" "AD" "DLB"]
            for cc2 = ["CTL" "AD" "DLB"]
                if cc1 ~= cc2
                    group1 = Corr_Samp_Clust_PP(SampleInfo.Region == rr & SampleInfo.Condition == cc1,ii);
                    group2 = Corr_Samp_Clust_PP(SampleInfo.Region == rr & SampleInfo.Condition == cc2,ii);
                    p = ranksum(group1,group2);
                    if p <= 0.05
                        disp(strcat("Cluster", num2str(ii-4), " - Region ", rr ," ",cc1," vs ",cc2," : " , num2str(p)));
                        
                        compGroupPlot = nan(max(length(group1),length(group2)),2);
                        compGroupPlot(1:length(group1),1) = group1;
                        compGroupPlot(1:length(group2),2) = group2;

                        %figure; violinplot(compGroupPlot)
                        title(strcat("Cluster", num2str(ii-4), " - Region ", rr ," ",cc1," vs ",cc2," : " , num2str(p)));
                        
                    end
                end
            end
        end
    end
end

% pvals only condition wise cluster composition (sample wise)

for ii = 5 : 11
    for cc1 = ["CTL" "AD" "DLB"]
        for cc2 = ["CTL" "AD" "DLB"]
            if cc1 ~= cc2
                group1 = Corr_Samp_Clust_PP(SampleInfo.Condition == cc1,ii);
                group2 = Corr_Samp_Clust_PP(SampleInfo.Condition == cc2,ii);
                p = ranksum(group1,group2);
                if p <= 0.05
                    disp(strcat("Cluster", num2str(ii-4)," ",cc1," vs ",cc2," : " , num2str(p)));

                    compGroupPlot = nan(max(length(group1),length(group2)),2);
                    compGroupPlot(1:length(group1),1) = group1;
                    compGroupPlot(1:length(group2),2) = group2;
                    
                end
            end
        end
    end
end

% pvals only region wise cluster composition (sample wise)

for ii = 5 : 11
    for cc1 = ["CA1" "CA3" "DG"]
        for cc2 = ["CA1" "CA3" "DG"]
            if cc1 ~= cc2
                group1 = Corr_Samp_Clust_PP(SampleInfo.Region == cc1,ii);
                group2 = Corr_Samp_Clust_PP(SampleInfo.Region == cc2,ii);
                p = ranksum(group1,group2);
                if p <= 0.05
                    disp(strcat("Cluster", num2str(ii-4)," ",cc1," vs ",cc2," : " , num2str(p)));

                    compGroupPlot = nan(max(length(group1),length(group2)),2);
                    compGroupPlot(1:length(group1),1) = group1;
                    compGroupPlot(1:length(group2),2) = group2;
                    
                end
            end
        end
    end
end

%% SAVE ALL IMAGES PRODUCED

Folder = fullfile(pwd, './OutputImages/Graphs/');
AllFigH = allchild(groot);

for iFig = 1:numel(AllFigH)
  fig = AllFigH(iFig);
  ax  = fig.CurrentAxes;
  ax.FontSize = 17;
  fig.PaperUnits = 'centimeter';
  fig.PaperPosition = [0 0 29.7 21];
  FileName = strcat(int2str(iFig),'_',date," - ",int2str(randi(1000000)),'.png');
  saveas(fig, fullfile(Folder, FileName));
end

%% RENDER BACK CLUSTERS ON ORGINAL IMAGE CLUSTERS

%PREPARE FOLDER
out_fold_repr = "\\atlas\LCSB_Images\groups\skupin\_projects\MICMAC_2\OutputImages\Overlays\";

%SELECT WHICH SAMPLES TO RENDER
samples_to_render = [67];

label_number = ["one","two","three","four","five","six","seven"];

for ii = samples_to_render
    
    sub_folder_name = strcat(SampleInfo.Condition(ii),SampleInfo.ID(ii),'_',SampleInfo.Region(ii),"\");
    mkdir(strcat(out_fold_repr,sub_folder_name));

    for cc = 1 : n_clusts

        img = false(metadata.X(ii),metadata.Y(ii),metadata.Z(ii));
        
        for jj = 1 : structsPerSample(ii)
            idx_vol = startSamp(ii) + jj - 1;
            if idx(idx_vol) == cc
               bb = bounding_boxes(idx_vol,:);
               bb = bb + [0.5 0.5 0.5 -1 -1 -1];
               img(bb(2):(bb(2)+bb(5)),bb(1):(bb(1)+bb(4)),bb(3):(bb(3)+(bb(6)))) = img(bb(2):(bb(2)+bb(5)),bb(1):(bb(1)+bb(4)),bb(3):(bb(3)+(bb(6)))) + volumes{idx_vol};
            end
        end

        outputFileName = strcat(SampleInfo.Condition(ii),SampleInfo.ID(ii),'_',SampleInfo.Region(ii),"_",label_number(cc),'.tif');

        S_regcond = smooth3(img,'gaussian',[1 1 1]);

        for K=1:length(S_regcond(1, 1, :))
           imwrite(S_regcond(:, :, K), strcat(out_fold_repr,sub_folder_name,outputFileName), 'WriteMode',...
           'append',  'Compression','none');
        end
    
    end
end

%% CREATE FEATURE SPECTRUM

% params
feature_to_render = [1:16]; %selected_features;
number_of_structs = 10;

for kk = 1 : length(feature_to_render)
    
    % creating bins distributed normally
    mu = mean(data(:,feature_to_render(kk)));
    sigma = std(data(:,feature_to_render(kk)));
    boundaries = norminv(1/number_of_structs:1/number_of_structs:(number_of_structs-1)/number_of_structs,mu,sigma);
    boundaries(length(boundaries)+1) = inf;
    
    label = zeros(1,length(data));
    
    for ii = 1 : length(data)
        for jj = 1 :  length(boundaries)
            if data(ii,feature_to_render(kk)) <= boundaries(jj)
                label(ii) = jj;
                break;
            end
        end
    end
    idxs_to_render = nan(1,number_of_structs);
    for ii = 1 : number_of_structs
       if(~isempty(find(label==ii)))
        idx_temp = find(label==ii);
        idxs_to_render(ii) = idx_temp(randi([1 length(idx_temp)]));
       end
    end
    
    idxs_to_render(isnan(idxs_to_render)) = [];
    
    gridVolsSpectrum(idxs_to_render,volumes);
    title(feat_names(kk))
end

%% CREATE FEATURE MIN AND MAX RENDERING

% params
feature_to_render = [1:16]; %selected_features;
number_of_structs = 36;

for ii = 1 : length(feature_to_render)
    
    [~,idxs_to_render] = mink(cell_set(:,ii),number_of_structs);
    gridVols(idxs_to_render,volumes);
    title(strcat("Minimum for ",feat_names(ii)))
    
    [~,idxs_to_render] = maxk(cell_set(:,ii),number_of_structs);
    gridVols(idxs_to_render,volumes);
    title(strcat("Maximum for ",feat_names(ii)))
    
end

%% VIOLIN PLOTS

for ii = 1 : length(feat_names)
   figure; violinplot(mean_set(SampleInfo.Region == 'CA1',ii),SampleInfo.Condition(SampleInfo.Region == 'CA1')); 
   title(strcat(feat_names(ii)," in CA1"))
end

for ii = 1 : length(feat_names)
   figure; violinplot(mean_set(SampleInfo.Region == 'CA3',ii),SampleInfo.Condition(SampleInfo.Region == 'CA3')); 
   title(strcat(feat_names(ii)," in CA3"))
end

for ii = 1 : length(feat_names)
   figure; violinplot(mean_set(SampleInfo.Region == 'DG',ii),SampleInfo.Condition(SampleInfo.Region == 'DG')); 
   title(strcat(feat_names(ii)," in DG"))
end

for ii = 1 : n_samps
   samp_cat(ii) = strcat(string(SampleInfo.Condition(ii)),"-Severity_",num2str(SampleInfo.SeverityQ(ii)));
end

for ii = 1 : length(feat_names)
   figure; violinplot(mean_set(SampleInfo.Region == 'CA1',ii),samp_cat(SampleInfo.Region == 'CA1')); 
   title(feat_names(ii))
end

for ii = 1: length(feat_names)
   figure; violinplot(mean_set(SampleInfo.Condition == "CTL",ii),SampleInfo.Region(SampleInfo.Condition == "CTL")); 
   title(strcat(feat_names(ii)," in CTL"))
end

for ii = 1: length(feat_names)
   figure; violinplot(mean_set(SampleInfo.Condition == "AD",ii),SampleInfo.Region(SampleInfo.Condition == "AD")); 
   title(strcat(feat_names(ii)," in AD"))
end

for ii = 1: length(feat_names)
   figure; violinplot(mean_set(SampleInfo.Condition == "DLB",ii),SampleInfo.Region(SampleInfo.Condition == "DLB")); 
   title(strcat(feat_names(ii)," in DLB"))
end

for condition = ["CTL" "AD" "DLB"]
    figure; violinplot(SampleInfo.Tau(SampleInfo.Condition == condition),SampleInfo.Region(SampleInfo.Condition == condition));
    title(strcat("Tau in ",condition))
    figure; violinplot(SampleInfo.ALoad(SampleInfo.Condition == condition),SampleInfo.Region(SampleInfo.Condition == condition));
    title(strcat("Aload in ",condition))
    figure; violinplot(SampleInfo.Psyn(SampleInfo.Condition == condition),SampleInfo.Region(SampleInfo.Condition == condition));
    title(strcat("Psyn in ",condition))
end

figure; violinplot(SampleInfo_CA1.Tau,samp_cat_CA1,'GroupOrder',{'CTL-Severity_1','CTL-Severity_2','CTL-Severity_3','AD-Severity_1','AD-Severity_2','AD-Severity_3','DLB-Severity_1','DLB-Severity_2','DLB-Severity_3'})
title('Tau for Different Severity Classification')
figure; violinplot(SampleInfo_CA1.ALoad,samp_cat_CA1,'GroupOrder',{'CTL-Severity_1','CTL-Severity_2','CTL-Severity_3','AD-Severity_1','AD-Severity_2','AD-Severity_3','DLB-Severity_1','DLB-Severity_2','DLB-Severity_3'})
title('Amyloid Load for Different Severity Classification')
figure; violinplot(SampleInfo_CA1.Psyn,samp_cat_CA1,'GroupOrder',{'CTL-Severity_1','CTL-Severity_2','CTL-Severity_3','AD-Severity_1','AD-Severity_2','AD-Severity_3','DLB-Severity_1','DLB-Severity_2','DLB-Severity_3'})
title('Psyn for Different Severity Classification')

%% FOR BAR R

%COND

sz = [21 4];
varTypes = {'string','double','double','double'};
varNames = {'Condition','Cluster','Average','Std'};
S_cond = table('Size',sz,'VariableTypes',varTypes,'VariableNames',varNames);

S_cond.Cluster = repmat((1:7)',3,1);
S_cond.Condition(1:7) = "CTL";
S_cond.Condition(8:14) = "AD";
S_cond.Condition(15:21) = "DLB";

for ii = 1 : 21
    idxs = SampleInfo.Condition == S_cond.Condition(ii); 
    S_cond.Average(ii) = mean(clust_count_samp_norm(idxs, S_cond.Cluster(ii)));
    S_cond.Std(ii) = std(clust_count_samp_norm(idxs, S_cond.Cluster(ii)));
end

%REG
sz = [21 4];
varTypes = {'string','double','double','double'};
varNames = {'Region','Cluster','Average','Std'};
S_reg = table('Size',sz,'VariableTypes',varTypes,'VariableNames',varNames);

S_reg.Cluster = repmat((1:7)',3,1);
S_reg.Region(1:7) = "CA1";
S_reg.Region(8:14) = "CA3";
S_reg.Region(15:21) = "DG";

for ii = 1 : 21
    idxs = SampleInfo.Region == S_reg.Region(ii); 
    S_reg.Average(ii) = mean(clust_count_samp_norm(idxs, S_reg.Cluster(ii)));
    S_reg.Std(ii) = std(clust_count_samp_norm(idxs, S_reg.Cluster(ii)));
end

%REGCOND
sz = [63 5];
varTypes = {'string','string','double','double','double'};
varNames = {'Condition','Region','Cluster','Average','Std'};
S_regcond = table('Size',sz,'VariableTypes',varTypes,'VariableNames',varNames);

S_regcond.Cluster = repmat((1:7)',9,1);
S_regcond.Region(1:21) = "CA1";
S_regcond.Region(22:42) = "CA3";
S_regcond.Region(43:63) = "DG";
S_regcond.Condition = repmat(vertcat(repmat(("CTL"),7,1),repmat(("AD"),7,1),repmat(("DLB"),7,1)),3,1);

for ii = 1 : 63
    idxs = SampleInfo.Condition == S_regcond.Condition(ii) & SampleInfo.Region == S_regcond.Region(ii); 
    S_regcond.Average(ii) = mean(clust_count_samp_norm(idxs, S_regcond.Cluster(ii)));
    S_regcond.Std(ii) = std(clust_count_samp_norm(idxs, S_regcond.Cluster(ii)));
end

%pval condreg
for ii = 1 : 7
    for rr = ["CA1" "CA3" "DG"]
        for cc1 = ["CTL" "AD" "DLB"]
            for cc2 = ["CTL" "AD" "DLB"]
                if cc1 ~= cc2
                    group1 = clust_count_samp_norm(SampleInfo.Region == rr & SampleInfo.Condition == cc1,ii);
                    group2 = clust_count_samp_norm(SampleInfo.Region == rr & SampleInfo.Condition == cc2,ii);
                    p = ranksum(group1,group2);
                    if p <= 0.05
                        disp(strcat("Cluster", num2str(ii), " - Region ", rr ," ",cc1," vs ",cc2," : " , num2str(p)));
                        
                        compGroupPlot = nan(max(length(group1),length(group2)),2);
                        compGroupPlot(1:length(group1),1) = group1;
                        compGroupPlot(1:length(group2),2) = group2;

                        %figure; violinplot(compGroupPlot)
                        title(strcat("Cluster", num2str(ii), " - Region ", rr ," ",cc1," vs ",cc2," : " , num2str(p)));
                        
                    end
                end
            end
        end
    end
end

%% Export files for R plotting

writematrix(cell_set);
writematrix(cell_set_norm);
writematrix(count_clust_CR_norm);
writematrix(feat_names);
writematrix(idx);
writematrix(mean_set);
writetable(SampleInfo);
writetable(metadata);