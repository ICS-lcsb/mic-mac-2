%% Parameters

%TESTING IMAGE PROCESSING MODE PARAMETER
test = false; %true when testing image processing only on one image, false when process whole stack
export_superclusters = false; %if true will export a separate image containing only superclusters (check clust_struct parameter to dedide the threshold)
export_cutoff = false; %if true will export a separate image containing only microglia removed because cut by the borders of the acqusition
export_touching = false; % if true will export a separate image containing only microglia removed because touching one another

%FOLDER

fold_name = 'C:\Users\corrado.ameli\Desktop\New folder';

%SAMPLE NAME
condition = 'Hip';
id = '1';
format = '.czi'; %supported '.tif' or '.czi' 

%CHANNEL PARAMETERS
channel_iba = 2; 
begin_z = 1;
end_z = 40;
  
%IMAGE PROCESSING PARAMETERS
blurness = 25; %LESS: more local background uniform - MORE: wider background uniform - DEF: 25 (10-100) H:25
threshold = 0.35; %MORE: less details and less noise - LESS: more details and more noise - DEF 0.35 (0-1) H:0.35

%STRUCTURE SEPARATION PARAMETERS
iterative_sep = false;
min_struct = 10000; %minimum number of voxels for considering a structure a cell DEF: 7000 H:10000
max_struct = 45000; %maximum number of voxels for considering a structure a cell DEF: 20000 H:45000
clust_struct = 135000; %minimum number of voxel for considering a structure a supercluster
precision = 4; %LESS: slower but more accurate - MORE: faster but less accurate DEF: 3

%VERY RAMIFIED RETRIVAL
extent = 0.04; %LESS: less biased but retreives less cells - MORE: more biased but retreives more cells DEF: 0.04
max_vol_big = 30000; % avoids superclusters to be included

%BORDER CUTOFF PARAMETER
remove_cutoff = false; %TRUE if you want to remove cutoff structures, false otherwise
bound_percentage = 0.01; %LESS: less structure but more complete - MORE: more structure less complete DEF: 0.1 H:0.05

PSF = fspecial('gaussian',3,5);

%% Load

addpath(genpath('./Library'));

dispstat('           -- MIC-MAC 2.0 -- ','keepthis');
if strcmp(format,'.tif')
    I_M = tifOpenSingle();
elseif strcmp(format,'.czi')
    I_M = cziOpenSingle(channel_iba);
else
    error('Unknown format');
end

if ~remove_cutoff
   export_cutoff = false;
   warning('Cannot export cutoff image if remove_cutoff option is disabled.');
end
    
if(test)
    I_M = I_M(:,:,floor((end_z-begin_z)/2)); 
    ii = 1;
else
    I_M = I_M(:,:,begin_z:end_z);
end

[xx, yy, zz] = size(I_M);

I_M = I_M./max(max(max(I_M)));

if(export_superclusters || export_cutoff || export_touching)
    mkdir(fold_name, 'Others')
end

%% Image Processing Iba Channel

dispstat('Start Processing IBA1.','timestamp','keepthis');

I_Mp = zeros( [xx yy zz],'logical' );

for ii = 1 : zz
    dispstat(['         ' num2str(ceil(((ii-1)/zz)*100)) '%']);
    
    B = BM3D(1,I_M(:,:,ii),25,'lc',0);
    
    C = imgaussfilt(B,blurness);
    D = imsubtract(B,C);
    E = imadjust(D);
    
    F = deconvlucy(E,PSF);
    
    H = F > threshold;
    
    %J = imclose(H,SE);
    
    I_Mp(:,:,ii) = H;
end

TOT_IBA1_density = sum(I_Mp,'all')/(xx*yy*zz);

dispstat('End Processing IBA1.','timestamp','keepthis');

%% Separate Structures

dispstat('Start Separating IBA1.','timestamp','keepthis');

if iterative_sep
   [A_BW_DECONN,A_NOT_SUB,A_BIG] = iterativeSeparation(I_Mp, max_struct, min_struct, 2, precision);
else
   CC2 = bwlabeln(I_Mp);
   ST = regionprops3(CC2,'Volume');
   
   idx_between = find([ST.Volume] <= max_struct & [ST.Volume] > min_struct);
   A_BW_DECONN_OK = ismember(CC2,idx_between);
   
   idx_clust = find([ST.Volume] > clust_struct);
   A_BW_CLUST = ismember(CC2,idx_clust);
   
   idx_touch = find([ST.Volume] < clust_struct & [ST.Volume] > max_struct);
   A_BW_TOUCH = ismember(CC2,idx_touch);
end
dispstat('End Separation IBA1.','timestamp','keepthis');

%% Retreive Very Big Ramified Cells


% dispstat('Start Retreiving Very Ramified Cells.','timestamp','keepthis');
% 
% CC3 = bwlabeln(A_BIG);
% st3 = regionprops3(CC3,'Extent','Volume');
% 
% for ii = 1 : height(st3)
%     dispstat(['         ' num2str(ceil(((ii-1)/height(st3))*100)) '%']);
%     if st3.Extent(ii) > extent || st3.Volume(ii) > max_vol_big
%        CC3(CC3==ii) = 0; 
%     end
% end
% 
% A_BIG_OK = logical(CC3);
% 
% A_BW_DECONN_OK = or(A_BW_DECONN,A_BIG_OK); 
% 
% dispstat('End Retreiving Very Ramified Cells.','timestamp','keepthis');


%% Removing Structures Cut Out from Planes

if remove_cutoff

dispstat('Start Removing Cut Out IBA1.','timestamp','keepthis');

bounds = ones(size(A_BW_DECONN_OK))*1;
%bounds(2:end-1,2:end-1,1) = 1;
%bounds(2:end-1,2:end-1,end) = 1;
bounds(10:end-10,10:end-10,2:end-1) = 0;

CC2 = bwlabeln(A_BW_DECONN_OK);
CC_off = zeros(size(CC2));

st = regionprops3(CC2,'Image','BoundingBox');

bound_scores = zeros(1,height(st));

for ii = 1 : length(bound_scores)
    
    dispstat(['         ' num2str(ceil(((ii-1)/length(bound_scores))*100)) '%']);
    
    str = double(bwmorph3(st.Image{ii,1},'remove'));
    %str = st.Image{ii,1};
    [idx,idy,idz] = getIdxBounding(st,ii);
    n_pix_struct = sum(sum(sum(str)));
    n_pix_bound = sum(sum(sum(str .* bounds(idx,idy,idz))));
    
    bound_scores(ii) = n_pix_bound/n_pix_struct;
        
end

for ii = 1 : length(bound_scores)
    dispstat(['         ' num2str(ceil(((ii-1)/length(bound_scores))*100)) '%']);
    if bound_scores(ii) > bound_percentage
       CC_off(CC2==ii) = 1;
       CC2(CC2==ii) = 0;
    end
end

A_BW_CUTBORD = logical(CC_off);
F = logical(CC2);

dispstat('End Removing Cut Out IBA1.','timestamp','keepthis');

else
    F = A_BW_DECONN_OK;
end

%% Export metadata

save([fold_name condition '_' id '_metadata'], 'begin_z', 'end_z', 'blurness', ... 
    'bound_percentage', 'channel_iba', 'extent', 'max_struct', 'clust_struct', ...
    'max_vol_big', 'min_struct', 'precision', 'threshold', 'remove_cutoff', 'TOT_IBA1_density','xx','yy','zz');

%% Export tif

dispstat('Starting Export.','timestamp','keepthis')
dispstat(['         Start Exporting Main Image...']);

outputFileName = strcat(condition,id,'.tif');

S = smooth3(F,'gaussian',[1 1 1]);

for K=1:length(S(1, 1, :))
   imwrite(S(:, :, K), [ fold_name outputFileName ], 'WriteMode',...
   'append',  'Compression','none');
end

if export_cutoff
    dispstat(['         Start Exporting Cutoff Image...']);
    outputFileName = strcat(condition,id,'_CUTOFF.tif');

    S = smooth3(A_BW_CUTBORD,'gaussian',[1 1 1]);

    for K=1:length(S(1, 1, :))
       imwrite(S(:, :, K), [ fold_name '/Others/iba1' outputFileName ], 'WriteMode',...
       'append',  'Compression','none');
    end
end

if export_superclusters
    dispstat(['         Start Exporting Superclusters Image...']);
    outputFileName = strcat(condition,id,'_CLUSTERS.tif');

    S = smooth3(A_BW_CLUST,'gaussian',[1 1 1]);

    for K=1:length(S(1, 1, :))
       imwrite(S(:, :, K), [ fold_name '/Others/iba1' outputFileName ], 'WriteMode',...
       'append',  'Compression','none');
    end
end

if export_touching
    dispstat(['         Start Exporting Touching Image...']);
    outputFileName = strcat(condition,id,'_TOUCHING.tif');

    S = smooth3(A_BW_TOUCH,'gaussian',[1 1 1]);

    for K=1:length(S(1, 1, :))
       imwrite(S(:, :, K), [ fold_name '/Others/iba1' outputFileName ], 'WriteMode',...
       'append',  'Compression','none');
    end
end


dispstat('         Export Done.','keepthis');
dispstat('End Of Process.','timestamp','keepthis');
