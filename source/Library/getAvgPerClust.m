function [clust_avg] = getAvgPerClust(data,array_values,idx,n_clusts,condition,region)

    clust_avg = zeros(1,n_clusts);

    for ii = 1 : n_clusts
       temp = array_values(data(:,51)==condition & data(:,52)==region & idx == ii);
       clust_avg(ii) = sum(temp)/length(temp);  
    end

end