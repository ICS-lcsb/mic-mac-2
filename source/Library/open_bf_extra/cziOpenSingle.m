function [I] = cziOpenSingle(channel)
    

    addpath('./bfmatlab');
    data = bfopen();
    metadata = data{1, 2};
    n_stacks = str2num(metadata.get('Global Information|Image|SizeZ #1'));
    try
    n_channels = str2num(metadata.get('Global Information|Image|SizeC #1')); 
    catch
    n_channels = 1;    
    end
    IMGS = data{1,1};
    dim_2 = size(IMGS{1,1});
    dim_3 = horzcat(dim_2, n_stacks);
    
    I = zeros(dim_3);
    fprintf(['Reading Channel ' int2str(channel) '... \n']);
    for jj = 1 : n_stacks
        index = (jj-1)*n_channels + channel;
        I(:,:,jj) = IMGS{index,1};
    end

    fprintf([ ' Channel ' int2str(channel)  ' Done! \n']);

end


