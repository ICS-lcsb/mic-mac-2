function [I] = tifOpenSingle()
    

    addpath('./bfmatlab');
    data = bfopen();
   
  
    IMGS = data{1,1};
    [n_stacks, ~] = size(IMGS);
    dim_2 = size(IMGS{1,1});
    dim_3 = horzcat(dim_2, n_stacks);
    
    I = zeros(dim_3);
    fprintf(['Reading Channel 1... \n']);
    for jj = 1 : n_stacks
        I(:,:,jj) = IMGS{jj,1};
    end

    fprintf([ ' Channel 1 Done! \n']);

end
