function [idx,idy,idz] = getIdxBounding(st,ii)

    idx = st.BoundingBox(ii,2)+0.5 : st.BoundingBox(ii,2)-0.5+st.BoundingBox(ii,5);
    idy = st.BoundingBox(ii,1)+0.5 : st.BoundingBox(ii,1)-0.5+st.BoundingBox(ii,4);
    idz = st.BoundingBox(ii,3)+0.5 : st.BoundingBox(ii,3)-0.5+st.BoundingBox(ii,6);

end

