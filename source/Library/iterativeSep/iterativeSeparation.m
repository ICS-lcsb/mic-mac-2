function [A_BW_DECONN,A_NOT_SUB,A_BIG] = iterativeSeparation(A_BW_F, max_vol_th, min_vol_th, max_iter, precision)

% ITERATIVESEPARATION  Separates connected components in A_BW_F.
%
%   -INPUT:
%           A_BW_F: the logical 3D matrix containing the objects
%
%           max_vol_th is the threshold for which a connected component,
%           based on its volume, will be processed for splitting.
%
%           min_vol_th is the threshold for which a connected component,
%           based in its volume, will be considered as a single unit.
%
%           max_iter identifies the maximum number of morphological opening
%           that the program will apply in order to find at least two
%           suitable sub-connected components to split.
%
%           precision identifies how rough the border that will separate
%           the two touching sub-connected components will be evaluated: 0
%           is the slowest but the most precise. See also getBorder.m
%
%   -OUTPUT:
%           A_BW_DECONN: logical 3D matrix containing the splitted objects
%
%           A_NOT_SUB: logical 3D matrix containing the objects that could
%           not be splitted
%
%   -PROCEDURE:
%           Firstly, the function removes all the structures that are
%           already separated according to min_vol_th. Then, it iterates
%           until there is no objects that needs to be splitted. For each
%           structure whose volume is bigger than max_vol_th, by applying
%           increasing morphological opening, the algorithm tries to find
%           two subregions that are at least min_vol_th big. If there are
%           none, the structure cannot be separated and it is saved in
%           A_NOT_SUB. If there are at least two subregions, getBorder()
%           finds the border that will split the two subregions. Then, the
%           border is subtracted from A_BW_F, making the structure be
%           splitted. At each iteration, structures that can be considered
%           non splittable, are removed from A_BW_F and put into
%           A_BW_DECONN.
%
%           Note: imadd is substituted with A OR B (A|B) operator.
%                 imsubtract is substituted with A AND ~B (A&~B) operator.
%

dispstat(['         '  'Removing Small Artifacts...']);

[CC,n_structs] = bwlabeln(A_BW_F);
stats = regionprops3(CC);

sum_vol = sum(stats.Volume);

idx_artif = find([stats.Volume] <= min_vol_th);
if ~isempty(idx_artif)
    A_NOT_SUB = ismember(CC, idx_artif);
end

idx_correct = find([stats.Volume] <= max_vol_th & [stats.Volume] > min_vol_th);
if ~isempty(idx_correct)
    A_BW_DECONN = ismember(CC, idx_correct);
end

if length(idx_correct) == (height(stats) - idx_artif)
    return
end

A_BW_F = A_BW_F & ~(A_NOT_SUB | A_BW_DECONN);
A_BIG = zeros(size(A_BW_F));

count_fail = 0;
count_split = 0;
count_separated = length(idx_correct);


while n_structs > 0
    
    continue_erode = 2;
    
    [~,ind_max] = max(stats.Volume);
    
    bb = ceil(stats.BoundingBox(ind_max,:));
    
    bx = bb(2) : (bb(2)+ bb(5) -1);
    by = bb(1) : (bb(1)+ bb(4) -1);
    bz = bb(3) : (bb(3)+ bb(6) -1);
    
    vol_init =  ismember(CC(bx,by,bz), ind_max);
  
    while continue_erode
        
        vol_er = imopen(vol_init, strel('cube', continue_erode));
        [CC_er, n_structs_er] = bwlabeln(vol_er);
        
        stats_er = regionprops3(CC_er);
        
        if n_structs_er > 1 && (sum(stats_er.Volume > (min_vol_th - (min_vol_th * continue_erode/10))) > 1)
            
            [~,ind_maxes] = maxk(stats_er.Volume,2);
            subV1 = ismember(CC_er, ind_maxes(1)) & vol_init;
            subV2 = ismember(CC_er, ind_maxes(2)) & vol_init;
            
            border = getBorder(subV1,subV2,vol_init,continue_erode+precision);
            
            A_BW_F(bx,by,bz) = A_BW_F(bx,by,bz) & (~border);
            
            continue_erode = 0;
            count_split = count_split + 1;
            
        elseif (continue_erode == max_iter+1) || (sum(stats_er.Volume > min_vol_th) == 0)
            
            continue_erode = 0;
            count_fail = count_fail+1;
            A_BIG(bx,by,bz) = A_BIG(bx,by,bz) | vol_init;
            A_BW_F(bx,by,bz) = A_BW_F(bx,by,bz) & (~vol_init);
            
        else
            
            continue_erode = continue_erode + 1;
            
        end
        
    end
    
    [CC,n_structs] = bwlabeln(A_BW_F);
    stats = regionprops3(CC);
    
    idx_artif = find([stats.Volume] <= min_vol_th);
    if ~isempty(idx_artif)
        A_NOT_SUB = A_NOT_SUB | ismember(CC, idx_artif);
    end
    
    idx_correct = find([stats.Volume] <= max_vol_th & [stats.Volume] > min_vol_th);
    if ~isempty(idx_correct)
        A_BW_DECONN = A_BW_DECONN | ismember(CC, idx_correct);
        count_separated = count_separated + 1;
    end
    
    A_BW_F = A_BW_F & ~(A_NOT_SUB | A_BW_DECONN);
    
    sum_vol_processed = sum(stats.Volume);
    dispstat(['         ' num2str(100 - (ceil((sum_vol_processed/sum_vol)*100))) '%   ' 'SPLITS: ' num2str(count_split) '  SEPARATED: ' num2str(count_separated) '  REJECTED: ' num2str(count_fail)]);
    
end


end

