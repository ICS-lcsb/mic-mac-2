function showInBBone(vol)

sz = size(vol);

CC = bwconncomp(vol);
stats = regionprops3(CC);
bb = ceil(stats.BoundingBox(1,:));

xx = bb(2) : min((bb(2)+ bb(5)),sz(2))-1;
yy = bb(1) : min((bb(1)+ bb(4)),sz(1))-1;
zz = bb(3) : min((bb(3)+ bb(6)),sz(3));

volumeViewer(vol(xx,yy,zz));


end

