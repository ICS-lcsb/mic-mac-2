function [border] = getBorder(subV1,subV2,vol_init,struct_dim)

first_touch = 0;
dont_touch = 1;
SE = strel('cube',struct_dim);
border = zeros(size(subV1));

while dont_touch

    if(~first_touch)
        subV1 = imdilate(subV1,SE) & vol_init;
    end
    
    subV2 = imdilate(subV2,SE) & vol_init;

    if  first_touch || sum(sum(sum(subV1 & subV2))) 
        
        first_touch = 1;
        border = subV1 & subV2;
        [~,n]= bwlabeln( vol_init & (~border) );
        
        if n > 1
           dont_touch = 0; 
        end
        
    end

end

end