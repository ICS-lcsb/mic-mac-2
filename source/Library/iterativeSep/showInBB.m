function  showInBB(stats,CC2,num)


bb = ceil(stats.BoundingBox(num,:));

vol = ismember(CC2, num);

v = vol(bb(2):(bb(2)+bb(5)-1),bb(1):(bb(1)+bb(4)-1),bb(3):(bb(3)+(bb(6)-1)));

volumeViewer(v);


end

