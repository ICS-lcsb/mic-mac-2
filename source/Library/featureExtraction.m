%% Parameters

folder_path = './OutputImages/HumanAnalysis/CA1/';
reprocess_samples = true; % if false it does not recompute features of samples already extracted.

fill_width = 5; %Default 5 with 20x
se = strel('diamond', fill_width);

%% Opening Director

addpath(genpath('./Library'));

dispstat('           -- MIC-MAC 2.0 -- ','keepthis');
dispstat('          -Feature Extraction- ','keepthis');
dispstat('          ','keepthis');

fold = dir(folder_path);
[n_objs, ~] = size(fold);
samples = [""];
n_samps = 0;

for ii = 1 : n_objs
    if contains(fold(ii).name,'.tif') && ~contains(fold(ii).name,'.mat')
        if reprocess_samples
            n_samps = n_samps+1;
            samples(n_samps) = string(fold(ii).name);
        else
            n_occurrences = 0;
            for jj = 1:n_objs
                if(contains(fold(jj).name,fold(ii).name))
                    n_occurrences = n_occurrences + 1;
                end
            end
            if n_occurrences < 2
                n_samps = n_samps+1;
                samples(n_samps) = string(fold(ii).name);
            end
        end
    end
end

dispstat(['Found ' num2str(n_samps) ' samples.'],'timestamp','keepthis');

%% Extracting Sample by Sample

dispstat('Start Process. ','timestamp','keepthis');

for ii = 1: n_samps
    
    % Read Image
    FileTif = strcat(folder_path,samples(ii));
    
    InfoImage=imfinfo(char(FileTif));
    x=InfoImage(1).Width;
    y=InfoImage(1).Height;
    nStacks=length(InfoImage);
    I=zeros(y,x,nStacks,'logical');
    
    TifLink = Tiff(FileTif, 'r');
    for i=1:nStacks
        TifLink.setDirectory(i);
        I(:,:,i)=TifLink.read();
    end
    TifLink.close();
    
    % Retreiving Single Structures
    CC2 = bwlabeln(I);
    structs = regionprops3(CC2,'BoundingBox','Image');
    n_structs = height(structs);
    
    attributes = zeros(n_structs,8);
    
    for jj = 1 : n_structs
        
        dispstat(['         Computing Sample ' num2str(ii) ' out of ' num2str(n_samps) ': ' num2str(ceil(((jj-1)/n_structs)*100)) '%']);
        
        % Isolating Structure
        bb = ceil(structs.BoundingBox(jj,:));
        vol = ismember(CC2, jj);
        vol = vol(bb(2):(bb(2)+bb(5)-1),bb(1):(bb(1)+bb(4)-1),bb(3):(bb(3)+(bb(6)-1)));
        
        [~,~,z] = size(vol);
        for kk = 1 : z
           vol(:,:,kk) = imclose(vol(:,:,kk),se); 
        end
        
        % Computing Features
        skel = Skeleton3D(vol);
        
        attributes(jj,1:5) = table2array(regionprops3(vol,'Volume','Extent','Solidity','SurfaceArea','EquivDiameter'));
        attributes(jj,6) = sum(sum(sum(bwmorph3(skel,'branchpoints'))));
        attributes(jj,7) = sum(sum(sum(bwmorph3(skel,'endpoints'))));
        attributes(jj,8) = attributes(jj,7)/((attributes(jj,7)+attributes(jj,6)));
    end
    
    dispstat(['Sample ' samples(ii) ' Done.'],'timestamp','keepthis');
    
    % Export Data
    save(strcat(folder_path,samples(ii),'_attributes.mat'),'attributes')
    save(strcat(folder_path,samples(ii),'_volumes.mat'),'structs')
end

dispstat('End of Process. ','timestamp','keepthis');
