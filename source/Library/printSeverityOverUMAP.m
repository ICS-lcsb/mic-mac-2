function [] = printSeverityOverUMAP(SampleInfo,index,condition,startSamp,region,umap_data,sev_metric,extra_text)

temp_idx = getSampsClassif(SampleInfo,index,condition,startSamp,region);
figure;
hold on
scatter(umap_data(:,1),umap_data(:,2),5,"filled",'MarkerFaceColor',[0.8 .8 .8])
scatter(umap_data(temp_idx,1),umap_data(temp_idx,2),20,sev_metric(temp_idx),"filled")
colormap(flip(autumn))
colorbar
caxis([0 1])
hold off
legend("Background","Cells of interest");
title(strcat("Cell in ",condition," ",region," with different Severity ",extra_text))

end

