function [v] = getSampsClassif(SampleInfo,index,condition,startSamp,region)

v = [];

if region == ""

    if(index == 0)

        for ii = 1 : height(SampleInfo)
           if SampleInfo.Condition(ii) == condition
              v = horzcat(v,startSamp(ii):startSamp(ii+1)-1); 
           end
        end

    else

        for ii = 1 : height(SampleInfo)
           if SampleInfo.SeverityQ(ii) == index && SampleInfo.Condition(ii) == condition
              v = horzcat(v,startSamp(ii):startSamp(ii+1)-1); 
           end
        end

    end

else
    
    if(index == 0)

        for ii = 1 : height(SampleInfo)
           if SampleInfo.Condition(ii) == condition && SampleInfo.Region(ii) == region
              v = horzcat(v,startSamp(ii):startSamp(ii+1)-1); 
           end
        end

    else

        for ii = 1 : height(SampleInfo)
           if SampleInfo.SeverityQ(ii) == index && SampleInfo.Condition(ii) == condition && SampleInfo.Region(ii) == region
              v = horzcat(v,startSamp(ii):startSamp(ii+1)-1); 
           end
        end

    end

end

