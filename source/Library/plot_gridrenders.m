
clust = idx;

flag_structs=1; % Plot the 3D renders
flag_plotgraph=0; % Graph and overlaid the structure

flag_info=1;
flag_subinfo=0;
size_bar_um=20; % Size of scale bar

upscale_im=[1,1,2];

range_clust = [1:max(clust)];
max_clust = max(clust);

% numb_struct=49;
% subplot_grid=[7,7];

numb_struct=1;
subplot_grid=[1,1];



color_edge{1} = [121 125 98]./256;
color_edge{2} = [217 174 148]./256;
color_edge{3} = [155 155 122]./256;
color_edge{4} = [255 203 105]./256;
color_edge{5} = [208 140 96]./256;
color_edge{6} = [153 123 102]./256;

%% Assigning all parameters from input structure: overwriting if needed

%%%%%%
n_pix_bar=round(size_bar_um/0.208);

x_max = 400;
y_max = 400;
z_max = 35;

% Start iterating through all specified clusters 
for i_clust=3%range_clust
    
    tot_str=zeros(subplot_grid(1)*x_max,subplot_grid(2)*y_max,z_max);
    offset_x=0;
    offset_y=0;
    for i=1:numb_struct
        
        idx_rand = find(clust==i_clust,randi([1,sum(clust==i_clust)]));
        %[~,idx_rand] = mink(distClust(clust==i_clust,i_clust),i);
        
        single_str= volumes_Cl{idx_rand(end)};
        
        tot_str(offset_x+(1:size(single_str,1)),offset_y+(1:size(single_str,2)),...
            (1:size(single_str,3)))=single_str;
        if i==1
            tot_str((1:n_pix_bar),(1:4),...
                (end-3:end))=1;
        end        
        
        
        if mod(i,subplot_grid(2))==0
            offset_x=offset_x+x_max;
            offset_y=0;
        else
            offset_y=offset_y+y_max;
        end;
        
    end;
    
    % Print the big array and save it
    if flag_structs
        fig_renders=figure();
        image3Dvis(tot_str,[],[],color_edge{i_clust},upscale_im);
        
        axis equal;axis off;
        set(gcf,'Color','white');
        view([60,40])
        
        %print(fig_renders,['Renders_Cluster' num2str(i_clust) '_' aux_str  '_NumbRenders' num2str(numb_struct) '.png'], '-dpng', '-r600');
        view([60,40])
        title(strcat("Cluster ",num2str(i_clust)))
    end;
    
    
    
end

