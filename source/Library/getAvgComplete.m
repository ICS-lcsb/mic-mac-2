function [condreg] = getAvgComplete(data,array_values,idx,n_clusts)

condreg = zeros(9,n_clusts);


condreg(1,:) = getAvgPerClust(data,array_values,idx,n_clusts,1,1);
condreg(2,:) = getAvgPerClust(data,array_values,idx,n_clusts,2,1);
condreg(3,:) = getAvgPerClust(data,array_values,idx,n_clusts,3,1);
condreg(4,:) = getAvgPerClust(data,array_values,idx,n_clusts,1,2);
condreg(5,:) = getAvgPerClust(data,array_values,idx,n_clusts,2,2);
condreg(6,:) = getAvgPerClust(data,array_values,idx,n_clusts,3,2);
condreg(7,:) = getAvgPerClust(data,array_values,idx,n_clusts,1,3);
condreg(8,:) = getAvgPerClust(data,array_values,idx,n_clusts,2,3);
condreg(9,:) = getAvgPerClust(data,array_values,idx,n_clusts,3,3);


end

