
 function [nodes_meas_table,graph_meas,connect_mat,weight_mat]=...
     extract_graph_meas(nodes_str,edges_str)

%
% In this function, I will take the structure of the graph and extract
% different graphical measures, like the average depth length, average
% degree, etc. Additionally, another function will compute some
% other measures more related with geometrical features.
% IMPORTANT: this function depends on the graph theory functions that are
% in the toolbox called "GraphFunctionsMIT"
%
% Input:
% - nodes and edges structures
%
% Output:
% - set of graphical measures
% - matrices
%
% Author: Luis Salamanca 
% Version: 0.1
% Date: 01.07.2018 
 
flag_use_Euc=1;

% Obtain the binary adjacency matrix, and the weighted adjacency matrix
edges_simp=[vertcat(edges_str(:).id),vertcat(edges_str(:).Length),...
    vertcat(edges_str(:).InNode),vertcat(edges_str(:).EndNode)];
nodes_conn=vertcat(nodes_str(:).id);
nodes_deg=vertcat(nodes_str(:).Degree);

connect_mat=sparse(zeros(max(reshape(edges_simp(:,3:4),1,[]))));
weight_mat=sparse(zeros(max(reshape(edges_simp(:,3:4),1,[]))));
weightEuc_mat=sparse(zeros(max(reshape(edges_simp(:,3:4),1,[]))));

index1=(edges_simp(:,3)-1)*size(connect_mat,1)+edges_simp(:,4);
index2=(edges_simp(:,4)-1)*size(connect_mat,1)+edges_simp(:,3);
edges_simp=[edges_simp,index1];

% Also the weights obtained considering the size of the voxels
edges_simp=[edges_simp,vertcat(edges_str(:).EucAllSteps)];

% We have to take into account possible multiedges
rep_elements=find(diff(index1)==0);
rep_elements_aux=index1(diff(index1)==0);

connect_mat(index1)=1;
connect_mat(index2)=1;

for i_m=rep_elements'
    connect_mat(index1(i_m))=connect_mat(index1(i_m))+1;
    connect_mat(index2(i_m))=connect_mat(index2(i_m))+1;
end;
connect_mat=connect_mat(nodes_conn,nodes_conn);

% And now the matrix of weights. Here we cannot consider multiedges, so we
% just take the smaller weight
[index_weight1,ind_w]=unique(index1);
index_weight2=index2(ind_w);
weights_simp=edges_simp(ind_w,2);
weightsEuc_simp=edges_simp(ind_w,6);
for i_m=unique(rep_elements_aux)'
    weight_sub=edges_simp((edges_simp(:,5)==i_m),2);
    weight_sub=min(weight_sub);
    ind_sub=find(index_weight1==i_m);
    weights_simp(ind_sub)=weight_sub;
    % And for Euclidean now
    weight_sub=edges_simp((edges_simp(:,5)==i_m),6);
    weight_sub=min(weight_sub);
    ind_sub=find(index_weight1==i_m);
    weightsEuc_simp(ind_sub)=weight_sub;
end;

weight_mat(index_weight1)=weights_simp;
weight_mat(index_weight2)=weights_simp;
weight_mat=weight_mat(nodes_conn,nodes_conn);

% And for Euclidean
weightEuc_mat(index_weight1)=weightsEuc_simp;
weightEuc_mat(index_weight2)=weightsEuc_simp;
weightEuc_mat=weightEuc_mat(nodes_conn,nodes_conn);

%%%% Now, we select which weighted matrix we are using
if flag_use_Euc
    weight_mat=weightEuc_mat;
end;

shortest_paths=graphallshortestpaths((weight_mat),'directed','false');

% Start with the measures. All these computed using the toolbox GraphFunctionsMIT
graph_meas=struct;
nodes_meas=[];

graph_meas.LinkDensity=linkDensity(connect_mat);
graph_meas.AverageDegree=averageDegree(connect_mat);

if flag_use_Euc
    graph_meas.LengthMean=mean(edges_simp(:,6));
    graph_meas.LengthMax=max(edges_simp(:,6));
else
    graph_meas.LengthMean=mean(edges_simp(:,2));
    graph_meas.LengthMax=max(edges_simp(:,2));
end;

nodes_meas=[nodes_meas;closeness(connect_mat)'];
nodes_meas=[nodes_meas;closeness(weight_mat)'];
graph_meas.ClosenessAmean=mean(nodes_meas(1,:));
graph_meas.ClosenessWmean=mean(nodes_meas(2,:));
graph_meas.ClosenessAmax=max(nodes_meas(1,:));
graph_meas.ClosenessWmax=max(nodes_meas(2,:));
graph_meas.ClosenessAmin=min(nodes_meas(1,:));
graph_meas.ClosenessWmin=min(nodes_meas(2,:));

if size(connect_mat,1)<2
    nodes_meas=[nodes_meas;0];
else
    nodes_meas=[nodes_meas;nodeBetweennessFaster(connect_mat)];
end;
graph_meas.NodeBetweennessMean=mean(nodes_meas(3,:));
graph_meas.NodeBetweennessMax=max(nodes_meas(3,:));
graph_meas.NodeBetweennessMin=min(nodes_meas(3,:));

nodes_meas=[nodes_meas;eigenCentrality(connect_mat)']; 
nodes_meas=[nodes_meas;eigenCentrality(weight_mat)']; 
graph_meas.EigenCentralityAmean=mean(nodes_meas(4,:));
graph_meas.EigenCentralityWmean=mean(nodes_meas(5,:)); 
graph_meas.EigenCentralityAmax=max(nodes_meas(4,:));
graph_meas.EigenCentralityWmax=max(nodes_meas(5,:));
graph_meas.EigenCentralityAmin=min(nodes_meas(4,:));
graph_meas.EigenCentralityWmin=min(nodes_meas(5,:));

graph_meas.sMetric=sMetric(connect_mat);
graph_meas.diameterA=diameter(connect_mat);
graph_meas.diameterW=diameter(weight_mat);

graph_meas.RadiusA=graphRadius(connect_mat);
graph_meas.RadiusW=graphRadius(weight_mat);

graph_meas.AvepathLengthA=avePathLength(connect_mat);
graph_meas.AvepathLengthW=avePathLength(weight_mat);

nodes_meas=[nodes_meas;vertexEccentricity(connect_mat)];
nodes_meas=[nodes_meas;vertexEccentricity(weight_mat)];
graph_meas.VertexEccentricityAmean=mean(nodes_meas(end-1,:));
graph_meas.VertexEccentricityWmean=mean(nodes_meas(end,:));
graph_meas.VertexEccentricityAmax=max(nodes_meas(end-1,:));
graph_meas.VertexEccentricityWmax=max(nodes_meas(end,:));
graph_meas.VertexEccentricityAmin=min(nodes_meas(end-1,:));
graph_meas.VertexEccentricityWmin=min(nodes_meas(end,:));

graph_meas.RatioEnding=sum(nodes_deg==1)/length(nodes_deg);

% Names of the measures
measures={'ClosenessA';'ClosenessW';'NodeBetweenness';'EigenCentralityA';'EigenCentralityW';...
    'VertexEccentricityA';'VertexEccentricityW'};
nodes_meas_table=array2table(nodes_meas,'RowNames',measures);





