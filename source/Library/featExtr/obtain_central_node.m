
function [id_node_center,ind_center]=obtain_central_node(nodes_str,nodes_meas)

%
% Just a function to compute the node that correspond to the cell body.
% Obviously, this is just an approximation, as several features of the
% nodes are used to try to locate the node that would correspond to the
% central one. The features consider, in order of importance, are:
% - node degree / betweenness: number of times the node acts as a bridge /
% closeness: inverse of the sum of the distances to all other nodes / 
% vertex Eccentricity: maximum distance to any other vertex
% 
% With this feature, and using a weigthed sum of them, we obtain scores and
% from them we select the "central node"
%
% Input:
% - nodes and their properties
%
% Output:
% - estimation of the central node, and its location in the array
%
% Author: Luis Salamanca 
% Version: 0.1
% Date: 01.07.2018 


weights_crit=[1,1.5,1.5,1]; % Weights for the features

aux_red=vertcat(nodes_str(:).id);
nodes_str_red=vertcat(nodes_str(aux_red));
degree_nodes=vertcat(nodes_str_red(:).Degree);

score_select=zeros(1,size(nodes_str_red,1));

% The first criteria is choosing one of the nodes highest degree
score_select(degree_nodes==max(degree_nodes))=weights_crit(1);

% Then, we will consider other criteria based on some graph measures
% Betweenness: number of times the variable acts as a bridge
ind_aux_bet=find(nodes_meas(3,:)==max(nodes_meas(3,:)));
score_select(ind_aux_bet)=score_select(ind_aux_bet)+weights_crit(2);

% Closeness: inverse of the sum of the distances to all other nodes
ind_aux_close=find(nodes_meas(2,:)==max(nodes_meas(2,:)));
score_select(ind_aux_close)=score_select(ind_aux_close)+weights_crit(3);

% Vertex Eccentricity: maximum distance to any other vertex
ind_aux=find(nodes_meas(7,:)==min(nodes_meas(7,:)));
score_select(ind_aux)=score_select(ind_aux)+weights_crit(4);

% Selection:
[val_max,ind_center]=max(score_select);
if length(ind_center)>1
    if length(ind_aux_bet)>1
        ind_center=intersect(ind_aux_bet,ind_aux_close);
        if length(ind_center)>1
            ind_center=intersect(ind_center,find(degree_nodes==max(degree_nodes)));
            if isempty(ind_center) || length(ind_center)>1
                ind_center=ind_aux_close;
            end;
        elseif isempty(ind_center)
            ind_center=ind_aux_close;
        end;
    else
        ind_center=ind_aux_bet;
        id_node_center=nodes_str_red(ind_center,1);
    end;
else
    id_node_center=nodes_str_red(ind_center,1).id;
end;