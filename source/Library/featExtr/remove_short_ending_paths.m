
function [nodes,edges,flag_change]=remove_short_ending_paths(nodes,edges,length_p)

%
% In this case, we need to have a edge shorter than the length indicated, and
% also connected to a node of degree 1. We remove the second and update the
% first.
%
% Input:
% - nodes and edges structures
% - maximum length of paths to remove
%
% Output:
% - nodes and edges structures corrected
% - flag to indicate if something has changed
%
% Author: Luis Salamanca 
% Version: 0.1
% Date: 01.07.2018 

flag_change=0;
for i_ed=1:size(edges)
    
    if ~isempty(edges{i_ed,2}) && (edges{i_ed,3} <= length_p)
        
        deg_nodes_con=[nodes{edges{i_ed,4},6},nodes{edges{i_ed,5},6}];
        ind_deg_zero=find(deg_nodes_con==1);
        
        if ~isempty(ind_deg_zero)
            
            flag_change=1;
            
            if isempty(3+setdiff(1:2,ind_deg_zero))
               continue; 
            end
            
            node_to_remove=edges{i_ed,3+ind_deg_zero};
            node_to_modify=edges{i_ed,3+setdiff(1:2,ind_deg_zero)};
            
            
            
            % Modify the node of degree larger than 1
            edg_conn=nodes{node_to_modify,5};
            ind_edge=find(edg_conn==i_ed);
            edg_conn=edg_conn(setdiff(1:length(edg_conn),ind_edge));
            nodes{node_to_modify,5}=edg_conn;
            
            vox_edg_conn=nodes{node_to_modify,3};
            vox_edg_conn=vox_edg_conn(setdiff(1:size(vox_edg_conn,1),ind_edge),:);
            nodes{node_to_modify,3}=vox_edg_conn;
            
            nodes{node_to_modify,6}=nodes{node_to_modify,6}-1;
            
            % Remove the node of degree 1
            nodes(node_to_remove,:)={[]};
            
            % Remove the edge
            edges(i_ed,:)={[]};
            
        end;
        
    end;
    
end;