

function plot_graph_layer(nodes,edges,separation_nodes,plot_edges_nodes,...
    flag_legend,color_back)

%
% Plots a tree scheme (with loops if they exist), where the top node is the
% one with the highest degree, and then the rest pending from it.
%
% Input:
% - nodes and edges structures
% - separation_nodes: separation in the horizontal direction
% - plot_edges_nodes: extra information of the edgens and nodes
% - other parameters for plotting
%
%
% Author: Luis Salamanca 
% Version: 0.1
% Date: 01.07.2018 
 

all_id_nodes=vertcat(nodes.id);
all_deg_nodes=vertcat(nodes.Degree);

max_deg=max(all_deg_nodes);
ind_max_deg=find(all_deg_nodes==max_deg);
id_max_deg=all_id_nodes(ind_max_deg);
nodes_next_it=id_max_deg;
nodes_next_come_from=zeros(1,length(id_max_deg));

not_end=1;

nodes_proc=[]; % Layer, id, Edge, and Node it connect to, and Parent node
edges_proc=[];
number_layer=1;

% First, go thorugh all the structures, storing the nodes that go in each layer
nodes_per_layer_conn=[];

while not_end
    
    number_nodes_per_layer(number_layer)=length(nodes_next_it);
    aux_nodes_per_layer_conn=[];
    el_i_id=1;
    for i_id=reshape(nodes_next_it,1,[])
        
        nodes_proc=[nodes_proc,i_id];
        edges_conn_id=nodes(i_id).ConnEdges;
        edges_conn_id=setdiff(edges_conn_id,[0,edges_proc]);
        
        edges_proc=[edges_proc,edges_conn_id];
        
        if ~isempty(edges_conn_id)
            nodes_conn_id=[];
            
            for i_ed=edges_conn_id
                
                aux_nodes_conn=setdiff([edges(i_ed).InNode,edges(i_ed).EndNode],i_id);
                nodes_conn_id=[nodes_conn_id,aux_nodes_conn];
                
                aux_el=[number_layer,i_id,i_ed,aux_nodes_conn,nodes_next_come_from(el_i_id)];
                aux_nodes_per_layer_conn=[aux_nodes_per_layer_conn;aux_el];
                
            end;
            
        else
            
            aux_el=[number_layer,i_id,0,0,nodes_next_come_from(el_i_id)]; % This indicates is a terminating node
            aux_nodes_per_layer_conn=[aux_nodes_per_layer_conn;aux_el];
            
        end;
        el_i_id=el_i_id+1;
        
    end;
    nodes_per_layer_conn=[nodes_per_layer_conn;aux_nodes_per_layer_conn];
    
    number_layer=number_layer+1;
    nodes_next_it=aux_nodes_per_layer_conn(:,4)';
    [nodes_next_it,ind_diff]=setdiff(nodes_next_it,[0,nodes_proc]);
    nodes_next_come_from=aux_nodes_per_layer_conn(:,2)';
    nodes_next_come_from=nodes_next_come_from(ind_diff);
    
    if length(unique(nodes_proc))==length(all_id_nodes)
        not_end=0;
    end;
    
end;

% Later, we will decide which node is the parent node. In case many of them have
% similar depths, and perhaps they are separated quite a lot, we can establish
% that all of them become parents nodes
not_end=1;

layer_max_n_nodes=max(number_nodes_per_layer);
layers=struct;

separation_y=20;
offset_y=0;

next_offsets=zeros(1,number_nodes_per_layer(end));
next_offsets_node=zeros(1,number_nodes_per_layer(end));

for n_l=max(nodes_per_layer_conn):-1:1
    
    ind_val=find(nodes_per_layer_conn(:,1)==n_l);
    diff_groups=unique(nodes_per_layer_conn(ind_val,5));
    diff_groups_nodes_parent=(nodes_per_layer_conn(ind_val,2));
    [in_offset,ind_sort]=sort(next_offsets);
    in_offset_node=next_offsets_node(ind_sort);
    diff_groups_aux=[];
    if ~isempty(in_offset_node) && sum(in_offset_node>0)
        for i_n_gr=1:length(in_offset_node)
            ind_coin=find(diff_groups_nodes_parent==in_offset_node(i_n_gr));
            if ~isempty(ind_coin);
                diff_groups_aux=[diff_groups_aux,...
                    reshape(nodes_per_layer_conn(ind_val(ind_coin),5),1,[])];
            end;            
        end;
        diff_groups_aux=[diff_groups_aux,...
            reshape(setdiff(diff_groups,diff_groups_aux),1,[])];
        diff_groups_aux=unique(diff_groups_aux,'stable');
        diff_groups=diff_groups_aux;
    end;
    next_offsets=[];
    next_offsets_node=[];
    list_nodes=[];
    locations_nodes=[];
    calc_offset=0;
    
    for i_diff=1:length(diff_groups)
        
        nodes_groups=nodes_per_layer_conn(ind_val,:);
        nodes_groups=nodes_groups((nodes_groups(:,5)==diff_groups(i_diff)),2);
        nodes_groups=unique(nodes_groups);
        
        offset_group=0;
        new_order_gr=[];
        aux_loc_new_gr=[];
        for i_n_gr=1:length(in_offset_node)
            ind_pos=find(nodes_groups==in_offset_node(i_n_gr));
            new_order_gr=[new_order_gr,nodes_groups(ind_pos)];
            if ~isempty(ind_pos)
                aux_loc_new_gr=[aux_loc_new_gr,in_offset(i_n_gr)];
                offset_group=max(max(aux_loc_new_gr))+separation_nodes;
            end;
            
        end;
        
        rem_nodes=length(nodes_groups)-length(new_order_gr);
        location=linspace(0,(rem_nodes-1)*separation_nodes,...
            rem_nodes)+offset_group;
        
        location=[aux_loc_new_gr,location];
        new_order_gr=[new_order_gr,reshape(setdiff(nodes_groups,new_order_gr),1,[])];
        
        if calc_offset>min(location)
            location=location+(calc_offset-min(location));
        end;
        calc_offset=max(location)+separation_nodes;
        
        locations_nodes=[locations_nodes,location];
        % calc_offset=max(location)+separation_nodes;
        
        next_offsets=[next_offsets,mean(location)];
        next_offsets_node=[next_offsets_node,diff_groups(i_diff)];
        
        list_nodes=[list_nodes,reshape(new_order_gr,1,[])];
        
    end;
    
    layers(n_l).list_nodes=list_nodes;
    layers(n_l).locations_nodes=[locations_nodes;...
        ones(1,length(locations_nodes))*offset_y];
    offset_y=offset_y+separation_y;
    
end;

stop=1;

% Order the nodes in a more convenient way, before plotting them
nodes_location=horzcat(layers.list_nodes);
nodes_location=[nodes_location;horzcat(layers.locations_nodes)];
nodes_location=nodes_location';
[dum,ind_sort]=sort(nodes_location(:,1));
nodes_location=nodes_location(ind_sort,:);

if strcmp(color_back,'black')
    colors={'c','y','b','g','w','m','c--','y--','b--','g--','w--','m--'};
else
    colors={'c','y','b','g','k','m','c--','y--','b--','g--','k--','m--'};
end;

% And now the edges. If an edge connects two nodes in the same layer, we give
% the edge some curvature
edges_red=[vertcat(edges.id),vertcat(edges.Length),vertcat(edges.InNode),...
    vertcat(edges.EndNode)];

already_conn_nodes=[];
for i_e=1:size(edges_red,1)
    
    id_edge=edges_red(i_e,1);
    nodes_conn=[edges_red(i_e,3),edges_red(i_e,4)];
    
    path_conn=[nodes_location((nodes_location(:,1)==nodes_conn(1)),2),...
        nodes_location((nodes_location(:,1)==nodes_conn(1)),3)];
    path_conn=[path_conn;nodes_location((nodes_location(:,1)==nodes_conn(2)),2),...
        nodes_location((nodes_location(:,1)==nodes_conn(2)),3)];
    if unique(nodes_per_layer_conn((nodes_per_layer_conn(:,2)==nodes_conn(1)),1))==...
            unique(nodes_per_layer_conn((nodes_per_layer_conn(:,2)==nodes_conn(2)),1))
        path_conn=[path_conn(1,:);mean(path_conn,1);path_conn(2,:)];
        path_conn(2,2)=path_conn(2,2)-8;
    end;
    
    % In case the nodes are not in the same layer
    if ~isempty(already_conn_nodes)
        flag_already=sum(sort(already_conn_nodes,2)==sort(kron(nodes_conn,...
            ones(size(already_conn_nodes,1),1)),2),2);
        flag_already=sum(flag_already==2);
        
        if flag_already>0
            
            path_conn=[path_conn(1,:);mean(path_conn,1);path_conn(2,:)];
            ang_mod=atand(abs(path_conn(1,2)-path_conn(3,2))/abs(path_conn(1,1)-path_conn(3,1)));
            mod_path=sign(mod(flag_already,2)-0.5);
            mod_path=mod_path*5;
            new_x=mod_path*cosd(ang_mod+45);
            new_y=mod_path*sind(ang_mod+45);
            
            path_conn(2,:)=path_conn(2,:)+[new_x,new_y];
            
        end;
    end;
    already_conn_nodes=[already_conn_nodes;nodes_conn];
    
    % Interpolate with a spline
    if size(path_conn,1)==3
        
        if flag_already>0
            
            aux_y=path_conn(1,2):(0.1*sign(path_conn(3,2)-path_conn(1,2)))...
                :path_conn(3,2);
            aux_x=spline(path_conn(:,2),path_conn(:,1),aux_y');
            path_conn=[reshape(aux_x,1,[])',reshape(aux_y,1,[])'];
            
        else
            aux_x=path_conn(1,1):(0.1*sign(path_conn(3,1)-path_conn(1,1)))...
                :path_conn(3,1);
            aux_y=spline(path_conn(:,1),path_conn(:,2),aux_x');
            path_conn=[aux_x',reshape(aux_y,1,[])'];
        end;
        
    end;
    
    color_ind=ceil(edges_red(i_e,2)/5);
    if color_ind>length(colors)
        color_ind=color_ind-length(colors);
    end;
    plot(path_conn(:,1),path_conn(:,2),colors{1,color_ind},...
        'LineWidth',2);
    if plot_edges_nodes(2)
        pos_marker=mean(path_conn,1);
        text(pos_marker(1),pos_marker(2),...
            ['Edge' num2str(id_edge)]);
        text(pos_marker(1),pos_marker(2)+2,...
            ['Len' num2str(edges_red(i_e,2))]);        
    end;
    hold on;
    
end;

% And now, plot the graphall_nodes=vertcat(nodes(:).id);
if strcmp(color_back,'black')
    colors={'c','y','b','g','w','m','cs','ys','bs','gs','ws','ms'};
else
    colors={'c','y','b','g','k','m','cs','ys','bs','gs','ks','ms'};
end;

for i_n=1:size(nodes_location,1)
    
        aux_pos=nodes_location(i_n,2:3);
        
        degree=all_deg_nodes(i_n);
        size_markers=(degree*50)+75;
        
        scatter(aux_pos(1),aux_pos(2),size_markers,...
            'MarkerFaceColor',colors{1,degree},'MarkerEdgeColor','k');
        if plot_edges_nodes(1)
            text(aux_pos(1)+1,aux_pos(2),['Node' num2str(nodes_location(i_n,1))]);
        end;
        hold on;
    
end;

h_tot=[];
h_tag={};
if strcmp(color_back,'black')
    colors={'c','y','b','g','w','m','c--','y--','b--','g--','w--','m--'};
else
    colors={'c','y','b','g','k','m','c--','y--','b--','g--','k--','m--'};
end;

if flag_legend
    
    dum_loc=[2*min(nodes_location(:,2)),2*max(nodes_location(:,3))];
    count=1;
    for i_deg=1:max(all_deg_nodes)        
        
        color_ind=i_deg;
        if color_ind>length(colors)
            color_ind=color_ind-length(colors);
        end;
        size_markers=(i_deg*50)+75;
        h=scatter(dum_loc(1),dum_loc(2),size_markers,...
            'MarkerFaceColor',colors{1,color_ind},'MarkerEdgeColor','k');
        aux_tag=['Degree ' num2str(i_deg)];
        h_tag{1,count}=aux_tag;
        h_tot=[h_tot,h];

        count=count+1;
        
    end;
    
    
    all_edges=unique(floor((edges_red(:,2)-1)/5))+1;
    for i_deg=all_edges'
        
        color_ind=i_deg;
        if color_ind>length(colors)
            color_ind=color_ind-length(colors);
        end;
        h=plot([dum_loc(1);dum_loc(1)],[dum_loc(2);dum_loc(2)],colors{1,color_ind},...
            'LineWidth',2);
        
        aux_tag=['Length ' num2str((i_deg-1)*5+1) ' to ' num2str((i_deg)*5)];
        h_tag{1,count}=aux_tag;
        h_tot=[h_tot,h];
        count=count+1;
        
    end;
    legend(h_tot,h_tag,'Location','southeast','FontSize',12);
    
    axis([min(nodes_location(:,2))-10,max(nodes_location(:,2))+10,...
        min(nodes_location(:,3))-10,max(nodes_location(:,3))+10]);
    axis off;%axis equal;
    set(gcf,'Color',color_back);
    
end;
