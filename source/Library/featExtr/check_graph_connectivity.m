
function [error_node_degree_info,wrong_node_degree_info,...
    error_conn_node_edge,wrong_conn_node_edge]=...
    check_graph_connectivity(edges,nodes)

% Go through the whole graph and checks if all the connectivity information is
% correctly stored, to do so, it first checks:
% 1 - In the node information, the degree matches the number of outputs voxels.
% 2 - Then, that the edge of each of those output voxels also is connected to
% that node
% 3 - And that the output voxel is also included in the list of the voxels in
% the path
%
% Input:
% - edges and nodes structures
%
% Output:
% - information about possible error when doing some of the processings of
% the graph
%
% Author: Luis Salamanca 
% Version: 0.1
% Date: 01.07.2018 

error_node_degree_info=0;
wrong_node_degree_info=[];
for i_n=1:size(nodes,1)
    
    if ~isempty(nodes{i_n,1})
        
        vox_path_conn=nodes{i_n,3};
        corr_vox_path=sum(vox_path_conn,2);        
        vox_path_conn=vox_path_conn(corr_vox_path>0,:);
        path_conn=nodes{i_n,5};
        path_conn=path_conn(corr_vox_path>0);
        deg_est=nodes{i_n,6};
        aux_err=zeros(1,3);
        
        if size(vox_path_conn,1)~=length(path_conn)
            aux_err(1)=1;
        end;
        if size(vox_path_conn,1)~=deg_est
            aux_err(2)=1;
        end;
        if deg_est~=length(path_conn)
            aux_err(3)=1;
        end;
        if sum(aux_err)>0
            aux_err(4)=i_n;
            wrong_node_degree_info=[wrong_node_degree_info;aux_err];
            error_node_degree_info=1;
        end;
        
    end;
    
end;

% Now, check if the connections are correct, the nodes with the edges, and
error_conn_node_edge=0;
wrong_conn_node_edge=[];
for i_n=1:size(nodes,1)
    
    if ~isempty(nodes{i_n,1})
        
        vox_path_conn=nodes{i_n,3};
        path_conn=nodes{i_n,5};
        
        flag_first=0;
        if i_n==1
            flag_first=1;
        end;
        
        for i_ed=(1+flag_first):size(vox_path_conn,1)
            
            aux_err=zeros(1,2);
            edge_an=path_conn(i_ed);
            edge_vox_path_conn=vox_path_conn(i_ed,:);
            
            vox_path=edges{edge_an,2};
            if size(vox_path,1)>1
                vox_path=[vox_path(1,:);vox_path(end,:)];
                ind_vox_path=sum(vox_path==kron(edge_vox_path_conn,ones(size(vox_path,1),1)),2);
                ind_vox_path=find(ind_vox_path==3);
            else
                ind_vox_path=sum(vox_path==edge_vox_path_conn);
            end;
            
            if isempty(ind_vox_path)
                
                aux_err(1)=1;
                
            else
                
                if size(vox_path,1)>1
                    
                    node_conn_to_path=edges{edge_an,3+ind_vox_path};
                    if i_n~=node_conn_to_path
                        aux_err(2)=1;
                    end;
                    
                elseif size(vox_path,1)==1
                    
                    node_conn_to_path=[edges{edge_an,4},...
                        edges{edge_an,5}];
                    if sum(node_conn_to_path==[i_n,i_n])==0
                        aux_err(2)=1;
                    end;
                    
                end;
                
            end;
            
            if sum(aux_err)>0
                aux_err(3:4)=[i_n,edge_an,];
                wrong_conn_node_edge=[wrong_conn_node_edge;aux_err];
                error_conn_node_edge=1;
            end;
                        
        end;
        
    end;
    
end;





