
function [nodes,edges,single_str_filt_m,flag_run]=constructGraph(skel,sizes_voxels,...
    flag_plot,flag_movieflood)

% This function takes the skeleton and construct the graph. For doing so,
% it runs a flooding, by using a convolutional filter, and whenever it finds 
% a junction, i.e. 3 or more pixels connected to the pixel in the center of
% the convolution matrix, it creates a node. It continues like this, giving
% back as output the list of nodes, edges, 
%
% Input:
% - skel: skeleton computed before, a 3D array with 1s and 0s
% - sizes_voxels: in um, for coordinates x, y and z
% - flag_plot: for showing the process of flooding
% - flag_movieflood: for recording a video
%
% Output:
% - nodes and edges: structures containing all the information
% - single_str_filt_m: 3D array
%
% Author: Luis Salamanca 
% Version: 0.1
% Date: 01.07.2018 

% To obtain a matrix with the neighborhood relations
% Values in this matrix: 1 are ending nodes, 2 are elements in edges, >3
% are junctions where several edges collide

filter_neig=ones(3,3,3);
single_str_filt=imfilter(uint8(skel),filter_neig,'conv');
single_str_filt=single_str_filt-1;
single_str_filt_m=single_str_filt.*uint8(skel);

not_chosen_first=1;
ind_term= find(single_str_filt_m == 1);

if sum(sum(sum(single_str_filt)))==0
    nodes = 0; edges = 0; single_str_filt_m = 0; flag_run = 0;
    return;
end

if isempty(ind_term)
    ind_term2= find(single_str_filt_m == 2);
    first_pos = ind2sub(size(single_str_filt),ind_term2(1));
    single_str_filt_m(first_pos) = 1;
    ind_term= find(single_str_filt_m == 1);
end
    
    
count_first=1;
if flag_movieflood
    flag_plot=1;
    movie_f=VideoWriter('VideoFlooding.avi');
    open(movie_f)
end
iteration=1;
flag_run=1;

% Just to select the node where the flooding starts
while not_chosen_first
    
    [x_it,y_it,z_it]=ind2sub(size(single_str_filt),ind_term(count_first));
    
    range_filt=[max(1,x_it-1),min(x_it+1,size(skel,1));...
        max(1,y_it-1),min(y_it+1,size(skel,2));...
        max(1,z_it-1),min(z_it+1,size(skel,3))]';
    mat_all_zeros_neig=zeros(size(skel));
    mat_all_zeros_neig(range_filt(1):range_filt(2),range_filt(3):range_filt(4),...
        range_filt(5):range_filt(6))=1;
    ind_neig=single_str_filt_m.*uint8(mat_all_zeros_neig);
    
    ind_edge_first=find(ind_neig==2);
    if ~isempty(ind_edge_first)

        curr_vox=[x_it,y_it,z_it,0,0,0,0];
        not_chosen_first=0;
        
    else
        
        count_first=count_first+1;
        
        if count_first>length(ind_term)
            flag_run=0;
            not_chosen_first=0;
        end
    end
    
end

% Once we have the initial node selected, this algorithm works as follows:
% 1. It start from a set of nodes 'start_nodes_to_proc', and explores all the
% edges emerging from it that have not been visited yet. The end of a edge
% is marked when we find a element of the skeleton with degree higher than
% two, i.e, a junction or an ending node. If it is an ending node, it is
% simply added to the set of nodes, and if is a junction, we store this
% information in the variable 'start_edges_to_proc'
% 2. All the information stored in 'start_edges_to_proc' is used to add new
% edges to the 'edges' structures. Besides, we define new nodes that are
% included in 'start_nodes_to_proc', eligible to be connected to not
% explored edges.
% 3. And then, we continue iterating like this.
% 4. We store information of the elements visited on a separate matrix. Then,
% at some point we will have visited everything and then the process is
% finished

if flag_run
    
    edges={};
    flag_end_edges=[];
    count_edges=1;
    
    nodes={};
    flag_end_nodes=[];
    count_nodes=1;
    
    tot_vox=sum(reshape((single_str_filt_m>0),1,[]));
    
    not_end=1;
    
    start_nodes_to_proc=curr_vox;
    
    mat_proc_voxels=uint8(skel); % To 1 those not processed.
    mat_proc_voxels_to_ones=zeros(size(skel)); % To 1 those not processed.
    mat_all_zeros=zeros(size(skel));
    
    while not_end
        
        %% Start processing nodes
        start_edges_to_proc=[];
        
        all_id_added=[];
        aux_prev_mat_proc_voxels=mat_proc_voxels;
        aux_prev_mat_proc_voxels_to_ones=mat_proc_voxels_to_ones;
        
        for i_no=1:size(start_nodes_to_proc,1)
            
            prev_mat_proc_voxels=mat_proc_voxels;
            prev_mat_proc_voxels_to_ones=mat_proc_voxels_to_ones;
            % First, check the voxels that belong to the node. To do so, we have to
            % check if there are other voxels nearby with degree 3 or larger. In
            % case of nodes with degree 1, we don't check this.
            nodes{count_nodes,1}=count_nodes; %id
            all_id_added=[all_id_added,count_nodes];
            curr_vox=start_nodes_to_proc(i_no,1:3);
            in_aux_nodes=start_nodes_to_proc(i_no,4:6);
            id_path_arrive=start_nodes_to_proc(i_no,7);
            if count_nodes>1
                edges{id_path_arrive,5}=count_nodes; % ending node
            end
            aux_start_nodes=[];
            
            flag_proc_vox=1;
            prev_mat_proc_voxels(curr_vox(1),curr_vox(2),curr_vox(3))=0;
            prev_mat_proc_voxels_to_ones(curr_vox(1),curr_vox(2),curr_vox(3))=1;
            
            flag_more_neig=1;
            aux_nodes=curr_vox;
            %aux_nodes=[];
            flag_first_round=1;
            
            while flag_more_neig
                
                range_filt=[max(1,curr_vox(1)-1),min(curr_vox(1)+1,size(skel,1));...
                    max(1,curr_vox(2)-1),min(curr_vox(2)+1,size(skel,2));...
                    max(1,curr_vox(3)-1),min(curr_vox(3)+1,size(skel,3))]';
                mat_all_zeros_neig=mat_all_zeros;
                mat_all_zeros_neig(range_filt(1):range_filt(2),range_filt(3):range_filt(4),...
                    range_filt(5):range_filt(6))=1;
                ind_neig=single_str_filt_m.*prev_mat_proc_voxels.*uint8(mat_all_zeros_neig);
                
                % Starting nodes of edges
                ind_neig_aux=find(ind_neig==2);
                [x_ex,y_ex,z_ex]=ind2sub(size(single_str_filt_m),ind_neig_aux);
                aux=[x_ex,y_ex,z_ex];
                aux_start_nodes=[aux_start_nodes;aux];
                
                % Ending nodes with degree 1
                ind_neig_aux=find(ind_neig==1);
                prev_mat_proc_voxels(ind_neig_aux)=0;
                prev_mat_proc_voxels_to_ones(ind_neig_aux)=1;
                
                [x_ex,y_ex,z_ex]=ind2sub(size(single_str_filt_m),ind_neig_aux);
                aux=[x_ex,y_ex,z_ex];
                aux_nodes=[aux_nodes;aux];
                flag_proc_vox=[flag_proc_vox,ones(1,size(aux,1))]; % Already to 1 as we
                % don't have to process them
                
                % Nodes that may span larger
                ind_neig=find(ind_neig>2);
                if ~isempty(ind_neig)
                    
                    prev_mat_proc_voxels(ind_neig)=0;
                    prev_mat_proc_voxels_to_ones(ind_neig)=1;
                    [x_ex,y_ex,z_ex]=ind2sub(size(single_str_filt_m),ind_neig);
                    aux=[x_ex,y_ex,z_ex];
                    aux_nodes=[aux_nodes;aux];
                    flag_proc_vox=[flag_proc_vox,zeros(1,size(aux,1))];
                    
                end
                
                if sum(flag_proc_vox==0)==0
                    flag_more_neig=0;
                else
                    curr_vox=min(find(flag_proc_vox==0));
                    flag_proc_vox(curr_vox)=1;
                    curr_vox=aux_nodes(curr_vox,:);
                end
                
            end
            
            nodes{count_nodes,2}=unique(aux_nodes,'rows','stable'); %voxels belonging
            
            aux_start_nodes=[in_aux_nodes;aux_start_nodes];
            nodes{count_nodes,3}=unique(aux_start_nodes,'rows','stable'); %voxels belonging
            if ~isempty(nodes{count_nodes,3}) && size(nodes{count_nodes,3},1)>1
                nodes{count_nodes,4}=0; % Indicates it has not been processed yet
            else
                nodes{count_nodes,4}=1; % Already processed
            end
            if sum(sum(nodes{count_nodes,3},2)==0)
                nodes{count_nodes,5}=0;
            else
                nodes{count_nodes,5}=id_path_arrive;
            end
            
            nodes{count_nodes,6}=sum(sum(nodes{count_nodes,3},2)>0);
            nodes{count_nodes,7}=0;
            
            % Check if the node has been constructed already
            
            tab_vox_node=nodes{count_nodes,2};
            tab_vox_node=sortrows(tab_vox_node);
            
            flag_repeat_node=0;
            if length(all_id_added)>1
                for i_id_c=1:(length(all_id_added)-1)
                    
                    if all_id_added(i_id_c)~=count_nodes
                        aux_tab_vox_node=nodes{all_id_added(i_id_c),2};
                        if size(aux_tab_vox_node,1)==size(tab_vox_node,1)
                            aux_tab_vox_node=sortrows(aux_tab_vox_node);
                            tot_sum=reshape(abs(tab_vox_node-aux_tab_vox_node),1,[]);
                            if sum(tot_sum)==0
                                flag_repeat_node=1;
                                id_repeated=all_id_added(i_id_c);
                            end
                        end
                    end
                    
                end
            end
            
            if flag_repeat_node
                
                aux_voxel_start=nodes{count_nodes,3};
                aux_voxel_start=aux_voxel_start(1,:);
                aux_aux_voxel_start=nodes{id_repeated,3};
                aux_aux_voxel_start=[aux_voxel_start;aux_aux_voxel_start];
                nodes{id_repeated,3}=aux_aux_voxel_start;
                aux_id_path_arrive=nodes{id_repeated,5};
                aux_id_path_arrive=[id_path_arrive,aux_id_path_arrive];
                edges{id_path_arrive,5}=id_repeated;
                nodes{id_repeated,5}=aux_id_path_arrive;
                nodes{id_repeated,6}=size(aux_aux_voxel_start,1);
                
                nodes(count_nodes,:)={[]};
                
            else
                % Here we exclude the first node
                aux_edges_proc=[unique(aux_start_nodes(2:end,:),'rows','stable'),...
                    kron(count_nodes,ones(size(nodes{count_nodes,3},1)-1,1))];
                start_edges_to_proc=[start_edges_to_proc;aux_edges_proc];
                
                count_nodes=count_nodes+1;
            end
            
            aux_prev_mat_proc_voxels(prev_mat_proc_voxels_to_ones==1)=0;
            aux_prev_mat_proc_voxels_to_ones(prev_mat_proc_voxels_to_ones==1)=1;
            
            %%%%%%%%%% Draw figure
            if flag_plot==1
                color_surf={'go','bs','r*','c+','mv','yo'};
                ind_marker=1;
                for i_val=(min(reshape(single_str_filt_m(single_str_filt_m>0),1,[]))):max(reshape(single_str_filt_m,1,[]))
                    aux_single_str_filt_m=single_str_filt_m.*aux_prev_mat_proc_voxels;
                    ind_val=find(aux_single_str_filt_m==i_val);
                    [x_aux,y_aux,z_aux]=ind2sub(size(single_str_filt_m),ind_val);
                    plot3(y_aux,x_aux,z_aux,color_surf{1,ind_marker});hold on;
                    ind_marker=ind_marker+1;
                end
                ind_val=find(aux_prev_mat_proc_voxels_to_ones==1);
                [x_aux,y_aux,z_aux]=ind2sub(size(single_str_filt_m),ind_val);
                plot3(y_aux,x_aux,z_aux,'ks');
                axis equal;axis off;
                set(gcf,'Color','white');
                view(140,80)
                camzoom(2)
                if flag_movieflood
                    frame=getframe;
                    writeVideo(movie_f,frame.cdata);
                end
                hold off;
                pause(0.0125)
            end
            %%%%%%%%%%%
            
        end
        
        mat_proc_voxels(aux_prev_mat_proc_voxels_to_ones==1)=0;
        mat_proc_voxels_to_ones(aux_prev_mat_proc_voxels_to_ones==1)=1;
        
        %%%%%%%%% End processing nodes
        
        %% Now, we process the edges
        % That starts in the voxels contained in start_edges_to_proc
                
%         if mod(iteration,10)==0
%             fprintf('Processing edges, it %d\n',iteration)
%         end
        start_nodes_to_proc=[];
        
        aux_prev_mat_proc_voxels=mat_proc_voxels;
        aux_prev_mat_proc_voxels_to_ones=mat_proc_voxels_to_ones;
        
        all_id_added=[];
        
        for i_ed=1:size(start_edges_to_proc,1)
            
            if sum(start_edges_to_proc(i_ed,:))>0
                
                prev_mat_proc_voxels=mat_proc_voxels;
                prev_mat_proc_voxels_to_ones=mat_proc_voxels_to_ones;
                
                edges{count_edges,1}=count_edges;
                all_id_added=[all_id_added,count_edges];
                
                curr_vox_start=start_edges_to_proc(i_ed,1:3);
                curr_node_id=start_edges_to_proc(i_ed,4);
                aux_id_paths=nodes{curr_node_id,5};
                ind_connec_edges_vox=sum(nodes{curr_node_id,3}==kron(curr_vox_start,...
                    ones(size(nodes{curr_node_id,3},1),1)),2);
                ind_connec_edges_vox=(find(ind_connec_edges_vox==3));
                aux_id_paths(ind_connec_edges_vox)=count_edges;
                nodes{curr_node_id,5}=aux_id_paths; % id path connected
                
                prev_mat_proc_voxels(curr_vox_start(1),curr_vox_start(2),curr_vox_start(3))=0;
                prev_mat_proc_voxels_to_ones(curr_vox_start(1),...
                    curr_vox_start(2),curr_vox_start(3))=1;
                path_edge=curr_vox_start;
                %path_edge=[];
                
                not_end_edge=1;
                while not_end_edge
                    
                    range_filt=[max(1,curr_vox_start(1)-1),min(curr_vox_start(1)+1,size(skel,1));...
                        max(1,curr_vox_start(2)-1),min(curr_vox_start(2)+1,size(skel,2));...
                        max(1,curr_vox_start(3)-1),min(curr_vox_start(3)+1,size(skel,3))]';
                    mat_all_zeros_neig=mat_all_zeros;
                    mat_all_zeros_neig(range_filt(1):range_filt(2),range_filt(3):range_filt(4),...
                        range_filt(5):range_filt(6))=1;
                    ind_neig=single_str_filt_m.*prev_mat_proc_voxels.*uint8(mat_all_zeros_neig);
                    
                    
                    ind_neig_node=find(ind_neig==1);
                    ind_neig_edge=find(ind_neig==2);
                    ind_neig_node_big=find(ind_neig>2);
                    if ~isempty(ind_neig_node_big)
                        
                        
                        % A voxel that belongs to a node
                        ind_neig_node_big=min(ind_neig_node_big);
                        %prev_mat_proc_voxels(ind_neig_node_big)=0;
                        %prev_mat_proc_voxels_to_ones(ind_neig_node_big)=1;
                        [x_ex,y_ex,z_ex]=ind2sub(size(single_str_filt_m),ind_neig_node_big);
                        aux=[x_ex,y_ex,z_ex,curr_vox_start,count_edges];
                        
                        start_nodes_to_proc=[start_nodes_to_proc;aux];
                    end
                    
                    if ~isempty(ind_neig_node)
                        % Ending nodes with degree 1. Add directly to the nodes list
                        prev_mat_proc_voxels(ind_neig_node)=0;
                        prev_mat_proc_voxels_to_ones(ind_neig_node)=1;
                        [x_ex,y_ex,z_ex]=ind2sub(size(single_str_filt_m),ind_neig_node);
                        aux=[x_ex,y_ex,z_ex];
                        nodes{count_nodes,1}=count_nodes; %id
                        nodes{count_nodes,2}=aux; % voxels node
                        nodes{count_nodes,3}=curr_vox_start; % last voxel path
                        nodes{count_nodes,4}=1; % path processed
                        nodes{count_nodes,5}=count_edges; % id path connected
                        nodes{count_nodes,6}=1; % degree node
                        count_nodes=count_nodes+1;
                    end
                    
                    if ~isempty(ind_neig_edge)
                        
                        % Voxels of edges. It can only be the next one in the path
                        prev_mat_proc_voxels(ind_neig_edge)=0;
                        prev_mat_proc_voxels_to_ones(ind_neig_edge)=1;
                        [x_ex,y_ex,z_ex]=ind2sub(size(single_str_filt_m),ind_neig_edge);
                        aux=[x_ex,y_ex,z_ex];
                        path_edge=[path_edge;aux];
                    end
                    
                    if isempty(ind_neig_edge) && isempty(ind_neig_node) &&...
                            isempty(ind_neig_node_big)
                        % Two scenarios here
                        % 1. In case of degree 0, and a path formed by only one voxel that
                        % is integrated in the nearest node.
                        % 2. Or in case we have a cycle, situation in which both ends
                        % will have the nodes already visited and therefore it can not
                        % find anything else.
                        
                        end_path_edge=path_edge(end,:);
                        all_other_paths=start_edges_to_proc(1:end,1:3);
                        comp_paths=sum(all_other_paths==kron(end_path_edge,...
                            ones(size(all_other_paths,1),1)),2);
                        ind_comp_paths=(find(comp_paths==3));
                        if size(path_edge,1)==1
                            ind_comp_paths=ind_comp_paths(2:end);
                        end
                        
                        if ~isempty(ind_comp_paths)
                                                        
                            edges{count_edges,2}=path_edge; % path voxels
                            edges{count_edges,3}=size(path_edge,1); % length path
                            edges{count_edges,4}=curr_node_id; % start node
                            
                            ind_node_comp=start_edges_to_proc(ind_comp_paths(1),4);
                            edges{count_edges,5}=ind_node_comp; % ending node
                            
                            % Two new distances
                            path_edge_scale=path_edge.*kron(sizes_voxels,ones(size(path_edge,1),1));
                            % Euclidean from starting to ending node
                            edges{count_edges,6}=sqrt(sum((path_edge_scale(end,:)-path_edge_scale(1,:)).^2));
                            % All path length, step by step
                            aux_length_scaled=(path_edge_scale(2:end,:)-path_edge_scale(1:(end-1),:)).^2;
                            aux_length_scaled=sum(aux_length_scaled,2);
                            aux_length_scaled=sqrt(aux_length_scaled);
                            edges{count_edges,7}=sum(aux_length_scaled); 
                            
                            connec_edges_vox=nodes{ind_node_comp,3};
                            connec_edges=nodes{ind_node_comp,5};
                            
                            ind_connec_edges_vox=sum(connec_edges_vox==kron(end_path_edge,...
                                ones(size(connec_edges_vox,1),1)),2);
                            ind_connec_edges_vox=(find(ind_connec_edges_vox==3));
                            
                            connec_edges(ind_connec_edges_vox)=count_edges;
                            nodes{ind_node_comp,5}=connec_edges;
                            
                            start_edges_to_proc(ind_comp_paths(1),:)=0;
                            
                            if length(ind_comp_paths)>1
                                
                                for i_comp=2:length(ind_comp_paths)
                                    
                                    count_edges=count_edges+1;
                                    edges{count_edges,1}=count_edges; % path voxels
                                    edges{count_edges,2}=path_edge; % path voxels
                                    edges{count_edges,3}=size(path_edge,1); % length path
                                    edges{count_edges,4}=curr_node_id; % start node
                                    
                                    edges{count_edges,5}=start_edges_to_proc(ind_comp_paths(i_comp),4); % ending node
                                    
                                    % Two new distances
                                    path_edge_scale=path_edge.*kron(sizes_voxels,ones(size(path_edge,1),1));
                                    % Euclidean from starting to ending node
                                    edges{count_edges,6}=sqrt(sum((path_edge_scale(end,:)-path_edge_scale(1,:)).^2));
                                    % All path length, step by step
                                    aux_length_scaled=(path_edge_scale(2:end,:)-path_edge_scale(1:(end-1),:)).^2;
                                    aux_length_scaled=sum(aux_length_scaled,2);
                                    aux_length_scaled=sqrt(aux_length_scaled);
                                    edges{count_edges,7}=sum(aux_length_scaled);
                                    
                                    start_edges_to_proc(ind_comp_paths(i_comp),:)=0;
                                    
                                end
                                
                            end
                            not_end_edge=0;
                            count_edges=count_edges+1;
                            
                        else
                            stop=1;
                            edges{count_edges,1}=[];
                            
                            ind_row_start=curr_vox_start;
                            aux_mat=nodes{curr_node_id,3};
                            ind_row_start=(aux_mat==kron(ones(size(aux_mat,1),1),ind_row_start));
                            ind_row_start=find(sum(ind_row_start,2)==3);
                            
                            mod_node_vox=nodes{curr_node_id,2};
                            mod_node_vox=[mod_node_vox;curr_vox_start];
                            nodes{curr_node_id,2}=mod_node_vox;
                            
                            mod_node_vox=nodes{curr_node_id,3};
                            mod_node_vox=mod_node_vox(setdiff(1:size(mod_node_vox,1),...
                                ind_row_start),:);
                            nodes{curr_node_id,3}=mod_node_vox;
                            
                            mod_node_vox=nodes{curr_node_id,5};
                            mod_node_vox=mod_node_vox(setdiff(1:length(mod_node_vox),...
                                ind_row_start));
                            nodes{curr_node_id,5}=mod_node_vox;
                            
                            nodes{curr_node_id,6}=nodes{curr_node_id,6}-1;
                            
                            % Removed voxels. They will be added at the end of the list
                            % of the voxels that form the node, and won't be considering
                            % when merging the path in case the final degree is 2
                            nodes{curr_node_id,7}=nodes{curr_node_id,7}+1;
                        end
                        
                        not_end_edge=0;
                        
                    end
                    
                    
                    if ~isempty(ind_neig_node_big) || ~isempty(ind_neig_node)
                        
                        edges{count_edges,2}=path_edge; % path voxels
                        edges{count_edges,3}=size(path_edge,1); % length path
                        edges{count_edges,4}=curr_node_id; % start node
                        if ~isempty(ind_neig_node)
                            edges{count_edges,5}=count_nodes-1; % ending node
                        end
                        
                        % Two new distances
                        path_edge_scale=path_edge.*kron(sizes_voxels,ones(size(path_edge,1),1));
                        % Euclidean from starting to ending node
                        edges{count_edges,6}=sqrt(sum((path_edge_scale(end,:)-path_edge_scale(1,:)).^2));
                        % All path length, step by step
                        aux_length_scaled=(path_edge_scale(2:end,:)-path_edge_scale(1:(end-1),:)).^2;
                        aux_length_scaled=sum(aux_length_scaled,2);
                        aux_length_scaled=sqrt(aux_length_scaled);
                        edges{count_edges,7}=sum(aux_length_scaled);
                        
                        not_end_edge=0;
                        count_edges=count_edges+1;
                        
                    elseif ~isempty(ind_neig_edge)
                        prev_curr_vox_start=curr_vox_start;
                        curr_vox_start=path_edge(end,:);
                        %curr_vox_start
%                         if sum(curr_vox_start==[70,6,43])==3
%                             stop=1;
%                         end
%                         if prev_curr_vox_start==curr_vox_start
%                             stop=1;
%                         end
                    end
                    
                end
                                
                aux_prev_mat_proc_voxels(prev_mat_proc_voxels_to_ones==1)=0;
                aux_prev_mat_proc_voxels_to_ones(prev_mat_proc_voxels_to_ones==1)=1;
                
                %%%%%%%%%% Draw figure
                if flag_plot==1
                    color_surf={'go','bs','r*','c+','mv','yo'};
                    ind_marker=1;
                    for i_val=(min(reshape(single_str_filt_m(single_str_filt_m>0),1,[]))):max(reshape(single_str_filt_m,1,[]))
                        aux_single_str_filt_m=single_str_filt_m.*aux_prev_mat_proc_voxels;
                        ind_val=find(aux_single_str_filt_m==i_val);
                        [x_aux,y_aux,z_aux]=ind2sub(size(single_str_filt_m),ind_val);
                        plot3(y_aux,x_aux,z_aux,color_surf{1,ind_marker});hold on;
                        ind_marker=ind_marker+1;
                    end
                    ind_val=find(aux_prev_mat_proc_voxels_to_ones==1);
                    [x_aux,y_aux,z_aux]=ind2sub(size(single_str_filt_m),ind_val);
                    plot3(y_aux,x_aux,z_aux,'ks');
                    axis equal;axis off;
                    set(gcf,'Color','white');
                    view(140,80)
                    camzoom(2)
                    if flag_movieflood
                        frame=getframe;
                        writeVideo(movie_f,frame.cdata);
                    end
                    hold off;
                    pause(0.0125)
                end
                %%%%%%%%%%%
            end
            
        end
        
        if isempty(start_nodes_to_proc)
            stop=1;
        end
        
        mat_proc_voxels(aux_prev_mat_proc_voxels_to_ones==1)=0;
        mat_proc_voxels_to_ones(aux_prev_mat_proc_voxels_to_ones==1)=1;
        
        %%%%%%%%% End processing nodes
        if sum(reshape(mat_proc_voxels,1,[]))==0
            not_end=0;
        end
        
        iteration=iteration+1;
        
    end
    
else
    
    nodes=[];
    edges=[];
    single_str_filt_m=[];
    
end

if flag_movieflood
    close(movie_f)
end