


function video_rec(skel,single_str,nodes,edges,new_nodes_merge,...
    new_edges_merge,flag_rec,zoom_cams)

%
% Records a video of a structure. First, as opaque element, then, becoming
% transparent and strat showing the skeleton, and then finally showing the
% graph in 3D and how it has been curated.
%
% Input:
% - skeleton in 3D array
% - single_str with 3D volume
% - nodes and edges structures, at different steps
% - flag_rec: to record or not 
%
% Author: Luis Salamanca 
% Version: 0.1
% Date: 01.07.2018 

if flag_rec==1
    az=1:2.5:360;
    az1=1:1.5:360;
    az2=1:1:360;
elseif flag_rec==0
    az=1:0.5:360;
    az1=1:0.5:360;
    az2=1:1:360;
elseif flag_rec==2
    az=180;
    az1=180;
    az2=180;
end;
color_back='black';
figure('units','normalized','position',[0 0 1 1])
col_fig='magenta';
alpha_val=1;
zoom_cam=1.5;
col_skel='cyan';
plot_edges_nodes=[0,0];

if flag_rec==1
    movie_f=VideoWriter('VideoProc.mp4','MPEG-4');
    open(movie_f)
end;

alpha_range=linspace(1,0.1,ceil(length(az)/8)+1);
count_alpha=1;
alpha_range1=linspace(0.2,0,ceil(length(az)/8));
count_alpha1=1;

spins=2;
for i_s=1:spins
    for i_az=1:length(az)
        
        if i_az==1 && i_s==1
            plot_skeleton(skel,col_skel,color_back,[az(i_az),0]);
            hold on;
            plot_overlay(single_str,col_fig,1,color_back,[az(i_az),0]);
            count_alpha=count_alpha+1;
            h=gca;
            camzoom(zoom_cams(1));
            if flag_rec==1
                currFrame = getframe;
                writeVideo(movie_f,currFrame);
            elseif flag_rec==2
                currFrame = getframe;
            end;
        elseif i_az==1 && i_s==3
            close all;
            figure('units','normalized','position',[0 0 1 1])
            hold on;

            plot_graph3D(new_nodes_merge,new_edges_merge,1,2,plot_edges_nodes,color_back,[az(i_az),20]);
            plot_overlay(single_str,col_fig,0.1,color_back,[az(i_az),20]);

            h=gca;
            camzoom(zoom_cams(1));
            if flag_rec==1
                currFrame = getframe;
                writeVideo(movie_f,currFrame);
            elseif flag_rec==2
                currFrame = getframe;
            end;
        else
            h.View=[az(i_az),20];
        end;
        
        
        if i_s==2
            if mod(i_az,8)==0
                alpha(alpha_range(count_alpha));
                count_alpha=count_alpha+1;
                
                if flag_rec==1
                    currFrame = getframe;
                    writeVideo(movie_f,currFrame);
                elseif flag_rec==2
                    currFrame = getframe;
                else
                    pause(0.002);
                end;
            else
                %pause(0.004);
                if flag_rec==1
                    currFrame = getframe;
                    writeVideo(movie_f,currFrame);
                elseif flag_rec==2
                    currFrame = getframe;
                else
                    pause(0.004);
                end;
            end;
        else
            if flag_rec==1
                currFrame = getframe;
                writeVideo(movie_f,currFrame);
            elseif flag_rec==2
                currFrame = getframe;
            else
                pause(0.004);
            end;
        end;
        
        
    end;
end;

stop=1;

if 1
close all;
figure('units','normalized','position',[0 0 1 1])
spins=1;
for i_s=1:spins
    for i_az=1:length(az1)

        if i_az==1 && i_s==1
            plot_overlay(single_str,col_fig,0.1,color_back,[az1(i_az),20]);
            hold on;
            plot_graph3D(nodes,edges,1,2,plot_edges_nodes,color_back,[az1(i_az),20]);
            h=gca;
            camzoom(zoom_cams(2));
        else
            h.View=[az1(i_az),20];
        end;
        
        if flag_rec==1
            currFrame = getframe;
            writeVideo(movie_f,currFrame);
        elseif flag_rec==2
            currFrame = getframe;
        else
            pause(0.004);
        end;
        
    end;
end;
end;

if 1
close all;
figure('units','normalized','position',[0 0 1 1])
spins=2;
for i_s=1:spins
    for i_az=1:length(az2)

        if i_az==1 && i_s==1
            plot_overlay(single_str,col_fig,0.1,color_back,[az2(i_az),20]);
            hold on;
            plot_graph3D(new_nodes_merge,new_edges_merge,1,2,plot_edges_nodes,color_back,[az2(i_az),20]);
            h=gca;
            camzoom(zoom_cams(2));
        else
            h.View=[az2(i_az),20];
        end;
        
        if flag_rec==1
            currFrame = getframe;
            writeVideo(movie_f,currFrame);
        elseif flag_rec==2
            currFrame = getframe;
        else
            pause(0.004);
        end;

    end;
end;
end;

if flag_rec==1
    close(movie_f)
end;