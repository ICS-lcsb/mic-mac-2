
function [geom_meas,geom_node_meas]=extract_geom_meas(nodes_str,edges_str,single_str,id_node_center)

%
% In this function, we will extract more geometrical measures, using also
% the volumes and the positions of the different voxels.
%
% Input:
% - nodes and edges structures
% - sizes_voxels: in um, for coordinates x, y and z
% - single_str: 3D array of the structure
% - id_node_center: estimated center
%
% Output:
% - set of geometrical measures
%
% Author: Luis Salamanca 
% Version: 0.1
% Date: 01.07.2018 

geom_meas=struct;
geom_node_meas=[];
sizes_voxels = [0.208,0.208,1];
edges_simp=[vertcat(edges_str(:).id),vertcat(edges_str(:).Length)];

geom_meas.Volume=sum(reshape(single_str,1,[])>0)*prod(sizes_voxels);
geom_meas.RatioVolNumbEdges=geom_meas.Volume/size(edges_simp,1);

edges_tort=zeros(1,size(edges_simp,1));
for i_e=1:size(edges_simp,1)

    length_p=edges_str(edges_simp(i_e,1)).EucAllSteps;
    dist=edges_str(edges_simp(i_e,1)).EuclideanStartEnd;
    edges_tort(i_e)=length_p/dist;
    
end;
geom_node_meas=[geom_node_meas;edges_tort];

geom_meas.TortuisityMean=mean(edges_tort);
geom_meas.TortuisityMax=max(edges_tort);
geom_meas.TortuisityMix=min(edges_tort);

% Compactness, by using the convex hull around the structure. Later is
% inverted to obtain the spreadness
ind_str=find(single_str);
[x_st,y_st,z_st]=ind2sub(size(single_str),ind_str);

dt=delaunayTriangulation(x_st,y_st,z_st);
[faces_convex,volumen_convex]=convexHull(dt);
str_convex.vertices=[y_st,x_st,z_st];
str_convex.faces=faces_convex;

geom_meas.Compactness=geom_meas.Volume/(volumen_convex*prod(sizes_voxels));

% Polarity
id_nodes=vertcat(nodes_str(:).id);
nodes_str_red=vertcat(nodes_str(id_nodes));
deg_nodes=vertcat(nodes_str_red(:).Degree);
ind_deg1_nodes=find(deg_nodes==1);
centroid_end_nodes=zeros(length(ind_deg1_nodes),3);

for i_end=1:length(ind_deg1_nodes)
    centroid_end_nodes(i_end,:)=mean(nodes_str_red(ind_deg1_nodes(i_end)).Centroid,1);
end;

% Normalize the centroids by the size of voxels
centroid_end_nodes=centroid_end_nodes.*kron(sizes_voxels,ones(size(centroid_end_nodes,1),1));

centroid_end_nodes_centered=centroid_end_nodes-kron(mean(nodes_str_red...
    (find(id_nodes==id_node_center)).Centroid,1),ones(length(ind_deg1_nodes),1));
length_all_centroids=sqrt(sum(centroid_end_nodes_centered.^2,2));

sum_centroids=mean(centroid_end_nodes_centered,1);
sum_centroids=sqrt(sum(sum_centroids.^2));

geom_meas.Polarity=sum_centroids;
geom_meas.PolarityNorm=sum_centroids/max(length_all_centroids);




