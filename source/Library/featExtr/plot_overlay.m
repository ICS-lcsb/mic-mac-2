

function plot_overlay(single_str,col,alpha_val,color_back,angles_view)

%
% Plots the structure with some transparency, normally on top of the
% skeleton or the graph, to see how these others capture the structure
% itself.
%
% Input:
% - single_str: 3D array of the volumen
% - properties for plotting, like color, transparency, angle of view
%
% Author: Luis Salamanca 
% Version: 0.1
% Date: 01.07.2018 
 

if nargin<2
    col=[.7 .7 .8];
    alpha_val=0.5;
elseif nargin==2
    alpha_val=0.5;
    if isempty(col)
        col=[.7 .7 .8];
    end;
elseif nargin==3
    if isempty(alpha_val)
         alpha_val=0.5;
    end;
    if isempty(col)
        col=[.7 .7 .8];
    end;
end;

if isempty(col)
    col=[.7 .7 .8];
end;

hiso = patch(isosurface(single_str,0),'FaceColor',col,'EdgeColor','none');
hiso2 = patch(isocaps(single_str,0),'FaceColor',col,'EdgeColor','none');
axis equal;axis off;
lighting phong;
isonormals(single_str,hiso);
alpha(alpha_val);
set(gca,'DataAspectRatio',[1 1 1])
camlight;
set(gcf,'Color',color_back);
view(angles_view)