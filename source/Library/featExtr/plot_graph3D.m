
function plot_graph3D(nodes,edges,flag_int,num_interp,plot_edges_nodes,...
    color_back,angles_view,offset_xyz)

%
% Plots the graph from the list of nodes and edges, using nice
% looking 3D nodes
%
% Input:
% - nodes and edges structures
% - flag for interpolating paths, and number of interpolations
% - flag plot_edges_nodes for plotting properties of edges and nodes
% - properties of the plot for visualization
%
% Author: Luis Salamanca 
% Version: 0.1
% Date: 01.07.2018 
 
flag_nocolor=1; % Fixed color for edges and nodes
width_line=1; % For the size of the edges
offset_size=10; % For the size of the nodes. It is also scale by its length
flag_fixsize=1; % In case we want a fixed size for the nodes
size_markers_fix=4; % Size 2 for small struct, and 4 for big

if nargin<3
    num_interp=1;
elseif nargin<8
    offset_xyz=[0,0,0];
end;

if isstruct(nodes)
    nodes=struct2cell(nodes)';
end;
if isstruct(edges)
    edges=struct2cell(edges)';
end;

[x,y,z]=sphere(40);

pos_nodes=[];
colors_markers=[];
size_markers=[];

if flag_nocolor
    colors=repmat({'g'},1,100);
else
    if strcmp(color_back,'black')
        colors={'c','y','b','g','w','m','c','y','b','g','w','m'};
    else
        colors={'c','y','r','b','g','k','m','c','y','r','b','g','k','m'};
    end;
end;

for i_n=1:size(nodes,1)
    
    if ~isempty(nodes{i_n,1})
        aux_pos=nodes{i_n,2};
        aux_pos=mean(aux_pos,1);
        pos_nodes=[pos_nodes;aux_pos];
        
        degree=nodes{i_n,6};
        colors_markers=[colors_markers;degree];
    else
        aux_pos=[0,0,0];
        pos_nodes=[pos_nodes;aux_pos];
        colors_markers=[colors_markers;0];
    end;
    
end;

for i_e=1:size(edges,1)
    %i_e
    if i_e==32
        stop=1;
    end;
    if ~isempty(edges{i_e,1})
        path=edges{i_e,2};
        id_edge=edges{i_e,1};
        path=[pos_nodes(edges{i_e,4},:);path;...
            pos_nodes(edges{i_e,5},:)];
        
        if flag_int==1
            
            new_path=path;
            for i_int=1:num_interp
                path_int=new_path(1,:);
                for i_p=1:(size(new_path,1)-1)
                    newval=mean(new_path(i_p:(i_p+1),:),1);
                    path_int=[path_int;newval;new_path(i_p+1,:)];
                end;
                new_path=path_int;
            end;
            
        else
            new_path=path;
        end;
        color_ind=ceil(edges{i_e,3}/5);
        if color_ind>length(colors)
            color_ind=color_ind-length(colors);
        end;
        try
            plot3(new_path(:,2)+offset_xyz(2),new_path(:,1)+offset_xyz(1),...
                new_path(:,3)+offset_xyz(3),colors{1,color_ind},...
                'LineWidth',width_line);
        catch
           fprintf('Missed Edge \n') 
        end;
        if plot_edges_nodes(1)
            pos_marker=new_path(round(size(new_path,1)/2),:);
            text(pos_marker(2),pos_marker(1),pos_marker(3),...
                ['Edge' num2str(id_edge)]);
        end;
        hold on;
    end;
    
end;

if flag_nocolor
    colors=repmat({'b'},1,100);
end;

for i_n=1:size(nodes,1)
    
    if ~isempty(nodes{i_n,1})
        aux_pos=nodes{i_n,2};
        aux_pos=mean(aux_pos,1);
        id_node=nodes{i_n,1};
        deg_node=nodes{i_n,6};
        
        degree=nodes{i_n,6};
        if flag_fixsize
            size_markers=size_markers_fix;
        else
            size_markers=log(degree+offset_size);
        end;
        
        x_aux=x*size_markers+aux_pos(1);
        y_aux=y*size_markers+aux_pos(2);
        z_aux=z*size_markers+aux_pos(3);
        
        if degree>length(colors)
            degree=degree-length(colors);
        end;
        
        hold on;
        mesh(y_aux+offset_xyz(2),x_aux+offset_xyz(1),z_aux+offset_xyz(3),'EdgeColor','none','LineStyle','none','FaceLighting','phong',...
            'FaceColor',colors{1,degree});
        hold off;
        if plot_edges_nodes(2)
            text(aux_pos(2),aux_pos(1),aux_pos(3),['Node' num2str(id_node) ...
                ' Deg' num2str(deg_node)]);
        end;
    end;
    
end;

camlight;
axis equal;axis off;
set(gcf,'Color',color_back);

view(angles_view)
   