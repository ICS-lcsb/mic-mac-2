
function [new_nodes_merge_str,new_edges_merge_str,all_errors,vec_err]=...
    graph_formation(single_str)

% Function to completely obtain the graph, from the skeletonization, to the
% calculation of the graph, and then its simplication. All these, together with
% intermediate checks of the graph, to see if it is being computed and stored
% correctly
%
% Input:
% - single_str: the 3D array of the volume
% - params_plot: different parameters for building the graph
%
% Output:
% - final structures containing all the information of edges and nodes
%
% Author: Luis Salamanca 
% Version: 0.1
% Date: 01.07.2018 

%% Default required variables

% Parameters for the plots
flag_plot=0; % If we want to observe the flooding process
flag_movieflood=0; % If we want to record a video of the flooding
all_flag_plots=zeros(1:8); % flag_plot_skel - flag_plot_graph1 - flag_plot_graph2 - flag_plot_graph3
% flag_plot_overlay - flag_plot_onlyskel - flag_plot_graphlayer
% flag_videorec
zoom_cams=[1.2,0.96]; % For the function video_rec. It has to be manually tuned for each structure

plot_edges_nodes=[0,0]; % Represent names: 1 edges, 2 nodes
plot_edges_nodes_l=[0,0];
separation_nodes=10;
flag_legend=1;
color_back='white';
angles_view=[140,20];
color_back_layer='black';
sizes_voxels = [0.208,0.208,1];
length_p = 4;
distance_nodes = 4;

%% Assigning all parameters from input structure: overwriting if needed
% all_inputs=fieldnames(params_plot);
% for i_in=1:length(all_inputs)
%     eval([all_inputs{i_in} '=params_plot.' all_inputs{i_in} ';']);
% end;


%% Start with the graph formation

% 3D Skelotinization
skel=Skeleton3D(logical(single_str));

% It takes the skeleton and computed the existing nodes and edges. Since
% this first graph will have errors, we have to clean it in subsequent
% steps. The output of this function are the structures nodes and edges
% that contain all the information of these elements in the graph formed.
% Each column contain the following information:
%
% Nodes: 1st: Id of the node;
% 2nd: the coordinate of the voxels belonging to the centroid;
% 3rd: the voxels that connect the centroid to the respective edges,
% order as we have Id of the edges in column 5;
% 4th: node processed? Just a dummy value, ignore it
% 5th: Id of connected edges, respecting the order in 3
% 6th: degree of the node, i.e., just the number of edges connected.
% 7th:another dummy variable
%
% Edges: 1st: Id of the edge;
% 2nd: the coordinate of all the voxels that form the path
% 3rd: the length of the path
% 4th: the Id of the node from which the path is emerging
% 5th: the Id of the node where the path is ending

% Remove possible disconnected components
str_all=regionprops(skel,'Area','PixelIdxList');
if length(str_all)>1
    all_areas=vertcat(str_all(:).Area);
    [~,ind_max]=max(all_areas);
    skel=zeros(size(skel));
    skel(str_all(ind_max).PixelIdxList)=1;
    skel=logical(skel);
end

[nodes,edges,~,flag_run]=constructGraph(skel,sizes_voxels,flag_plot,flag_movieflood);

if flag_run
    
    more_change=1;
    new_nodes=nodes;
    new_edges=edges;
    all_errors=[];
    vec_err=[];
    iter_number=1;
    
    while more_change && iter_number < 1000

        
        % Remove nodes of degree 2 and merge them in the corresponding paths
        [new_nodes,new_edges,flag_change]=remove_nodes_deg2(new_nodes,new_edges,sizes_voxels);
        
        % Remove those edges that terminate on a degree 1 node and whose length
        % are smaller or equal than length_p
        [new_nodes,new_edges,flag_change_def]=remove_short_ending_paths(new_nodes,new_edges,length_p);
        
        % We just check if the current state of the graph is correct, i.e., if the
        % indexes in the nodes and edges structures are correct
        [error_1,tab_err1,error_2,tab_err2]=check_graph_connectivity(new_edges,new_nodes);
        all_errors=[all_errors;ones(1,4)*iter_number;tab_err1;tab_err2];
        vec_err=[vec_err;iter_number;error_1;error_2];
        iter_number=iter_number+1;
        
        if flag_change_def==0 && flag_change==0
            more_change=0;
        end
        
    end
    
    more_change=1;
    new_nodes_in=new_nodes;
    new_edges_in=new_edges;
    
    while more_change && iter_number < 1000
        
        % I merge nodes that are close to each other, also changing the assignments of
        % the edges and nodes involved
 
        [new_nodes_merge,new_edges_merge,~,flag_change]=...
            merge_nodes(new_nodes_in,new_edges_in,sizes_voxels,distance_nodes);
        
        if flag_change
            [new_nodes_merge,new_edges_merge,flag_change]=...
                remove_nodes_deg2(new_nodes_merge,new_edges_merge,sizes_voxels);
            if ~flag_change
                more_change=0;
            end
        else
            more_change=0;
        end
        
        new_nodes_in=new_nodes_merge;
        new_edges_in=new_edges_merge;
        
        [error_1,tab_err1,error_2,tab_err2]=check_graph_connectivity(new_edges_in,new_nodes_in);
        all_errors=[all_errors;ones(1,4)*iter_number;tab_err1;tab_err2];
        vec_err=[vec_err;iter_number;error_1;error_2];
        iter_number=iter_number+1;
        
    end
    clear('new_nodes_in','new_edges_in')
    
    name_cols_nodes={'id','Centroid','VoxConnEdges','Proc','ConnEdges','Degree','RemEdges'};
    new_nodes_merge_str=cell2struct(new_nodes_merge,name_cols_nodes,2);
    
    name_cols_edges={'id','Path','Length','InNode','EndNode','EuclideanStartEnd','EucAllSteps'};
    new_edges_merge_str=cell2struct(new_edges_merge,name_cols_edges,2);
    
else
    
    new_nodes_merge_str=[];
    new_edges_merge_str=[];
    all_errors=[];
    vec_err=[];
    
end

% Here we just have some function to represent the graph, the skeleton, and
% the original figure
if sum(all_flag_plots)>0
    
    % flag_plot_skel - flag_plot_graph1 - flag_plot_graph2 - flag_plot_graph3
    % flag_plot_overlay - flag_plot_onlyskel - flag_plot_graphlayer
    % flag_videorec
    flag_plot_skel=all_flag_plots(1);
    flag_plot_graph1=all_flag_plots(2);
    flag_plot_graph2=all_flag_plots(3);
    flag_plot_graph3=all_flag_plots(4);
    flag_plot_overlay=all_flag_plots(5);
    flag_plot_onlyskel=all_flag_plots(6);
    flag_plot_graphlayer=all_flag_plots(7);
    flag_videorec=all_flag_plots(8);
    
    if flag_plot_skel
        figure;
        plot_skeleton(skel,[],color_back,angles_view);
        hold on;
        plot_overlay(single_str,[],0.2,color_back,angles_view);
        
        figure;
        plot_graph3D(new_nodes_merge,new_edges_merge,1,2,plot_edges_nodes,color_back,angles_view);
        hold on;
        plot_overlay(single_str,'magenta',0.2,color_back,angles_view);
    end
    
    % Plot the graph with nice 3D nodes. Here we have different
    % representations for different stages
    if flag_plot_graph1
        figure
        plot_graph3D(nodes,edges,1,2,plot_edges_nodes,color_back,angles_view);
    end
    if flag_plot_graph2
        figure;
        plot_graph3D(new_nodes,new_edges,1,2,plot_edges_nodes,color_back,angles_view);
    end
    if flag_plot_graph3
        figure;
        plot_graph3D(new_nodes_merge,new_edges_merge,1,2,plot_edges_nodes,color_back,angles_view);
    end
    hold on;
    
    % Overlay the 3D structure on top of the graph with some transparancy,
    % to see how it looks
    if flag_plot_overlay
        plot_overlay(single_str,[],0.2,color_back,angles_view);
        hold on;
    end
    
    % Plot the skeleton obtained with Skeleton3D function
    if flag_plot_onlyskel
        plot_skeleton(skel,[],color_back,angles_view)
    end
    
    % I cannot remember what it does XD
    %figure;
    %plot_graph_filt(single_str_filt_m)
    
    % Plot the nodes neatly, putting the node with the highest degree on
    % top, and the rest pending below
    if flag_plot_graphlayer
        figure;
        plot_graph_layer(new_nodes_merge_str,new_edges_merge_str,separation_nodes,...
            plot_edges_nodes_l,flag_legend,color_back_layer)
    end
    
    % Record a video from the structure
%     if flag_videorec
%         video_rec(skel,single_str,nodes,edges,new_nodes_merge,new_edges_merge,1,zoom_cams)
%     end
%     
%     pause
    
end
