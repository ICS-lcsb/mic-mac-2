
function [nodes,edges,flag_change]=remove_nodes_deg2(nodes,edges,sizes_voxels)

%
% Remove wrong nodes of degree 2, converting them back into edges.
%
% Input:
% - nodes and edges structures, and sizes_voxels in um
%
% Output:
% - nodes and edges structures corrected
% - flag to indicate if something has changed
%
% Author: Luis Salamanca 
% Version: 0.1
% Date: 01.07.2018 
 

flag_change=0;
for i_n=1:size(nodes,1)
    %i_n
    if nodes{i_n,6}==2
        
        deg_conn=nodes{i_n,5};
        
        if(~isempty(find(deg_conn == 0)))
           continue; 
        end
        
        mat_conn=[edges{deg_conn,4};edges{deg_conn,5}];
        mat_conn=mat_conn(1,:)-mat_conn(2,:);
        
        if sum(mat_conn==0)==0
            
            flag_change=1;
            % We will simply check the axis with the greatest distance, and then add
            % that number of voxels inbetween to fill the gap
            edges_con=nodes{i_n,5};
            new_path=edges{edges_con(1),2};
            end_path=edges{edges_con(2),2};
            
            vox_edges_con=nodes{i_n,3};
            
            
            if sum(new_path(end,:)==vox_edges_con(1,:))<3
                new_path=new_path(end:-1:1,:);
            end;
            if sum(end_path(1,:)==vox_edges_con(2,:))<3
                end_path=end_path(end:-1:1,:);
            end;
            
            if size(end_path,1)==0 || edges_con(2)==51
                stop=1;
            end;
            
            distances_aux=(new_path(end,:)-end_path(1,:));
            distances=max(0,abs(new_path(end,:)-end_path(1,:))-1);
            x_int=round(linspace(new_path(end,1)-sign(distances_aux(1)),...
                end_path(1,1)+sign(distances_aux(1)),max(distances)));
            if end_path(1,1)<new_path(end,1)
                x_int=sort(x_int,'descend');
            else
                x_int=sort(x_int,'ascend');
            end;
            y_int=round(linspace(new_path(end,2)-sign(distances_aux(2)),...
                end_path(1,2)+sign(distances_aux(2)),max(distances)));
            if end_path(1,2)<new_path(end,2)
                y_int=sort(y_int,'descend');
            else
                y_int=sort(y_int,'ascend');
            end;
            z_int=round(linspace(new_path(end,3)-sign(distances_aux(3)),...
                end_path(1,3)+sign(distances_aux(3)),max(distances)));
            if end_path(1,3)<new_path(end,3)
                z_int=sort(z_int,'descend');
            else
                z_int=sort(z_int,'ascend');
            end;
            
            aux_path=[x_int',y_int',z_int'];
            if ~isempty(aux_path)
                if sum((aux_path(end,:)-end_path(1,:)).^2)>sum((aux_path(1,:)-end_path(1,:)).^2)
                    aux_path=aux_path(end:-1:1,:);
                end;
            end;
            
            new_path=[new_path;aux_path;end_path];
            
            % Update first edge with all the information
            edges{edges_con(1),2}=new_path;
            edges{edges_con(1),3}=size(new_path,1);
                        
            aux_nodes_conn=[edges{edges_con(1),4},edges{edges_con(1),5}];
            aux_nodes_conn=setdiff(aux_nodes_conn,i_n);
            edges{edges_con(1),4}=aux_nodes_conn;
            
            aux_nodes_conn=[edges{edges_con(2),4},edges{edges_con(2),5}];
            aux_nodes_conn=setdiff(aux_nodes_conn,i_n);
            edges{edges_con(1),5}=aux_nodes_conn;
            
            % Two new distances
            path_edge_scale=new_path.*kron(sizes_voxels,ones(size(new_path,1),1));
            % Euclidean from starting to ending node
            edges{edges_con(1),6}=sqrt(sum((path_edge_scale(end,:)-path_edge_scale(1,:)).^2));
            % All path length, step by step
            aux_length_scaled=(path_edge_scale(2:end,:)-path_edge_scale(1:(end-1),:)).^2;
            aux_length_scaled=sum(aux_length_scaled,2);
            aux_length_scaled=sqrt(aux_length_scaled);
            edges{edges_con(1),7}=sum(aux_length_scaled);
            
            % Remove second edge
            edges(edges_con(2),:)={[]};
            
            % Remove node
            nodes(i_n,:)={[]};
            
            % All other nodes that also connect to the removed edge, have to update
            % the id
            cols_edges=nodes(:,5);
            for i_nodes=1:size(cols_edges,1)
                aux_cols_edges=nodes{i_nodes,5};
                if sum(aux_cols_edges==edges_con(2))>0
                    ind_subs=find(aux_cols_edges==edges_con(2));
                    aux_cols_edges(ind_subs)=edges_con(1);
                    nodes{i_nodes,5}=aux_cols_edges;
                end;
            end;
            
        end;
        
    end;
    
end;