

function [nodes,edges,all_conn_established,flag_changes]=merge_nodes(nodes,edges,...
    sizes_voxels,distance_nodes)

%
% Simply a function that takes the list of nodes and edges, and merge those
% nodes that are quite close to each other, then changing the list to only
% have one encompassing both of them
%
% Author: Luis Salamanca 
% Version: 0.1
% Date: 01.07.2018 

flag_changes=0;

% First, calculate centroids
all_id_centrois=[];
for i_n=1:size(nodes,1)
    
    if ~isempty(nodes{i_n,2})
        
        aux_vox=nodes{i_n,2};
        aux_vox=mean(aux_vox,1);
        aux_id_cent=[nodes{i_n,1},aux_vox];
        
        all_id_centrois=[all_id_centrois;aux_id_cent];
        
    end
    
end

% Now, distances between centroids
aux_cent1=all_id_centrois(:,2:4);
%
aux_cent1=aux_cent1.*kron(sizes_voxels,ones(size(all_id_centrois,1),1));
%
aux_cent2=reshape(aux_cent1',1,[]);
aux_cent1=kron(ones(1,length(aux_cent2)/3),aux_cent1);
aux_cent2=kron(aux_cent2,ones(size(aux_cent1,1),1));

distance_cent=(aux_cent1-aux_cent2).^2;
distance_cent=reshape(distance_cent',3,[])';
distance_cent=sqrt(sum(distance_cent,2));
distance_cent=reshape(distance_cent,size(aux_cent1,1),[]);
distance_cent=triu(distance_cent);
distance_cent(distance_cent==0)=1000;

[rows_v,cols_v]=find(distance_cent<=distance_nodes);
rows_val_id=all_id_centrois(rows_v,1);
cols_val_id=all_id_centrois(cols_v,1);
cat_nodes_conn=[rows_val_id,cols_val_id];
cat_nodes_conn=sort(cat_nodes_conn,2);

% And now, keep only those that are connected through an edge, forming also the
% structure with all the information we will use later
id_edges=vertcat(edges{:,1});
edges_conn=[vertcat(edges{:,4}),vertcat(edges{:,5})];
edges_conn=sort(edges_conn,2);
already_conn_nodes=[0,0]; % The id of the node, and the row in the structure
all_conn_established={}; % First column, the nodes, and second, the edges that
% connect them. The order of the edges doesn't matter,
% as we are going to remove them
count_groups=1;

% Here we just check which nodes are we going to merge together into a
% single one. All this is stored into all_conn_established, to later
% process the edges and nodes structures
for i_n=1:size(cat_nodes_conn,1)
    
    curr_nodes=cat_nodes_conn(i_n,:);
    ind_edge_conn=sum(edges_conn==kron(curr_nodes,ones(size(edges_conn,1),1)),2);
    ind_edge_conn=find(ind_edge_conn==2);
    
    if ~isempty(ind_edge_conn)
        
        deg_nodes_chosen=[nodes{cat_nodes_conn(i_n,1),6},nodes{cat_nodes_conn(i_n,2),6}];
        
        if sum(deg_nodes_chosen==1)==0
            
            if sum(curr_nodes(1)==already_conn_nodes(:,2)) || ...
                    sum(curr_nodes(2)==already_conn_nodes(:,2))
                
                ind_already=find(curr_nodes(1)==already_conn_nodes(:,2));
                node_added=curr_nodes(2);
                if isempty(ind_already)
                    ind_already=find(curr_nodes(2)==already_conn_nodes(:,2));
                    node_added=curr_nodes(1);
                end
                ind_already=already_conn_nodes(ind_already,1);
                
                
                aux_nodes_already=all_conn_established{ind_already,1};
                aux_edges_already=all_conn_established{ind_already,2};
                
                aux_nodes_already=unique([reshape(aux_nodes_already,1,[]),...
                    reshape(curr_nodes,1,[])]);
                aux_edges_already=unique([reshape(aux_edges_already,1,[]),...
                    reshape(id_edges(ind_edge_conn),1,[])]);
                
                all_conn_established{ind_already,1}=aux_nodes_already;
                all_conn_established{ind_already,2}=aux_edges_already;
                
                aux_already=[ind_already,node_added];
                already_conn_nodes=[already_conn_nodes;aux_already];
                
                ind_repeat=find(already_conn_nodes(1:(end-1),2)==node_added);
                if ~isempty(ind_repeat)
                    if sum(already_conn_nodes(ind_repeat,1)~=(ind_already*ones(1,length(ind_repeat))))>0
                        
                        group_to_merge=unique(already_conn_nodes(ind_repeat,1));
                        ind_to_keep=min([group_to_merge,ind_already]);
                        ind_to_remove=setdiff([group_to_merge,ind_already],ind_to_keep);
                        
                        aux_nodes_already=all_conn_established{ind_to_keep,1};
                        aux_edges_already=all_conn_established{ind_to_keep,2};
                        aux_nodes_already=unique([reshape(aux_nodes_already,1,[]),...
                            reshape(all_conn_established{ind_to_remove,1},1,[])]);
                        aux_edges_already=unique([reshape(aux_edges_already,1,[]),...
                            reshape(all_conn_established{ind_to_remove,2},1,[])]);
                        all_conn_established{ind_to_keep,1}=aux_nodes_already;
                        all_conn_established{ind_to_keep,2}=aux_edges_already;
                        already_conn_nodes((already_conn_nodes(:,1)==ind_to_remove),1)=ind_to_keep;
                        all_conn_established(ind_to_remove,1:2)={[]};
                        
                    end
                end
                [val_dum,ind_non]=unique(already_conn_nodes(:,2),'stable');
                already_conn_nodes=already_conn_nodes(ind_non,:);
                                
            else
                
                all_conn_established{count_groups,1}=curr_nodes;
                all_conn_established{count_groups,2}=id_edges(ind_edge_conn);
                aux_already=[kron(count_groups,[1;1]),curr_nodes'];
                if already_conn_nodes(1,1)==0
                    already_conn_nodes=aux_already;
                else
                    already_conn_nodes=[already_conn_nodes;aux_already];
                end
                
                count_groups=count_groups+1;
                
            end
            
        end
        
    end
    
end

% Now, I need to merge the nodes, taking the centroid of all of the voxels that
% formed them, and then remove the edges interconnecting, and reconnect the rest
% of the edges, interpolating accordingly

for i_conn=1:size(all_conn_established)
    
    if ~isempty(all_conn_established{i_conn,1})
        
        nodes_rem=all_conn_established{i_conn,1};
        edges_rem=all_conn_established{i_conn,2};
        
        cent_nodes=[];
        edges_conn=[];
        start_edges_conn=[];
        for i_n=1:length(nodes_rem)
            
            cent_nodes=[cent_nodes;mean(nodes{nodes_rem(i_n),2},1)];
            if nodes_rem(i_n)==1
                aux_d=nodes{nodes_rem(i_n),5};
                edges_conn=[edges_conn,aux_d(2:end)];
                aux_d=nodes{nodes_rem(i_n),3};
                start_edges_conn=[start_edges_conn;aux_d(2:end,:)];
            else
                edges_conn=[edges_conn,nodes{nodes_rem(i_n),5}];
                start_edges_conn=[start_edges_conn;nodes{nodes_rem(i_n),3}];
            end
            
        end
        
        [edges_keep,indexes_val]=setdiff(edges_conn,edges_rem);
        start_edges_conn=start_edges_conn(indexes_val,:);
        node_first=nodes_rem(1);
        nodes_rem=nodes_rem(2:end);
        new_cent=round(mean(cent_nodes,1));
        
        % Go through the edges that are kept, and redo the path between them and the
        % new center
        new_conn_paths=[];
        
        for i_edg=1:length(edges_keep)
            
            % Reconnect the edges, also adding some extra voxels, from the last
            % start/end of the path, to the new centroid of the node
            vox_edges_con=edges{edges_keep(i_edg),2};
            connecting_vox=start_edges_conn(i_edg,:);
            
            distances_aux=connecting_vox-new_cent;
            distances=max(0,abs(new_cent-connecting_vox)-1);
            x_int=round(linspace(connecting_vox(1)-sign(distances_aux(1)),...
                new_cent(1)+sign(distances_aux(1)),max(distances)));
            if new_cent(1)<connecting_vox(1)
                x_int=sort(x_int,'descend');
            else
                x_int=sort(x_int,'ascend');
            end
            y_int=round(linspace(connecting_vox(2)-sign(distances_aux(2)),...
                new_cent(2)+sign(distances_aux(2)),max(distances)));
            if new_cent(2)<connecting_vox(2)
                y_int=sort(y_int,'descend');
            else
                y_int=sort(y_int,'ascend');
            end
            z_int=round(linspace(connecting_vox(3)-sign(distances_aux(3)),...
                new_cent(3)+sign(distances_aux(3)),max(distances)));
            if new_cent(3)<connecting_vox(3)
                z_int=sort(z_int,'descend');
            else
                z_int=sort(z_int,'ascend');
            end
            
            aux_path=[x_int',y_int',z_int']; % The voxel close to the centroid is the last one
            if ~isempty(aux_path)
                if sum((aux_path(end,:)-new_cent).^2)>sum((aux_path(1,:)-new_cent).^2)
                    aux_path=aux_path(end:-1:1,:);
                end
            end
            
            new_path=[];
            
            if sum(connecting_vox==vox_edges_con(1,:))==3
                new_path=[aux_path(end:-1:1,:);vox_edges_con];
                new_conn_paths=[new_conn_paths;new_path(1,:)];
            else
                if sum(connecting_vox==vox_edges_con(end,:))==3
                    new_path=[vox_edges_con;aux_path];
                    new_conn_paths=[new_conn_paths;new_path(end,:)];
                end
            end
            
            edges{edges_keep(i_edg),2}=new_path;
            edges{edges_keep(i_edg),3}=size(new_path,1);
            flag_changes=1;
            
            if sum(nodes_rem==edges{edges_keep(i_edg),4})==1
                edges{edges_keep(i_edg),4}=node_first;
            end
            if sum(nodes_rem==edges{edges_keep(i_edg),5})==1
                edges{edges_keep(i_edg),5}=node_first;
            end
            
            % Two new distances
            path_edge_scale=new_path.*kron(sizes_voxels,ones(size(new_path,1),1));
            % Euclidean from starting to ending node
            edges{edges_keep(i_edg),6}=sqrt(sum((path_edge_scale(end,:)-path_edge_scale(1,:)).^2));
            % All path length, step by step
            aux_length_scaled=(path_edge_scale(2:end,:)-path_edge_scale(1:(end-1),:)).^2;
            aux_length_scaled=sum(aux_length_scaled,2);
            aux_length_scaled=sqrt(aux_length_scaled);
            edges{edges_keep(i_edg),7}=sum(aux_length_scaled);
            
        end
        
        % Store all new values in the first node, the one that will
        % constitute the super node
        
        if node_first==1
            nodes{node_first,2}=new_cent;
            nodes{node_first,3}=[0,0,0;new_conn_paths];
            nodes{node_first,5}=[0,edges_keep];
            nodes{node_first,6}=length(edges_keep);
        else
            nodes{node_first,2}=new_cent;
            nodes{node_first,3}=new_conn_paths;
            nodes{node_first,5}=edges_keep;
            nodes{node_first,6}=length(edges_keep);
        end
        
        % And remove the edges and nodes merges and dissapeared
        nodes(nodes_rem,:)={[]};
        edges(edges_rem,:)={[]};
        
    end
end


