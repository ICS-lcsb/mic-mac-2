

function plot_skeleton(skel,col,color_back,angles_view)

%
% Plots the skeleton
%
% Input:
% - single_str: 3D array of the volumen
% - properties for plotting, like color, transparency, angle of view
%
% Author: Luis Salamanca 
% Version: 0.1
% Date: 01.07.2018 

if nargin<2 
    col='k';
else
    if isempty(col)
        col='k';
    end;
end;

w=size(skel,1);
l=size(skel,2);
h=size(skel,3);
[x,y,z]=ind2sub([w,l,h],find(skel(:)));

plot3(y,x,z,'square','Markersize',4,'MarkerFaceColor',col,'Color',col); 
axis equal;axis off;
set(gcf,'Color',color_back);
view(angles_view)