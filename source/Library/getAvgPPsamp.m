function [means] = getAvgPPsamp(SampleInfo,PP)

means = zeros(1,9);

means(1,1) = mean(PP(SampleInfo.Region == 'CA1' & SampleInfo.Condition == 'CTL'));
means(1,2) = mean(PP(SampleInfo.Region == 'CA1' & SampleInfo.Condition == 'AD'));
means(1,3) = mean(PP(SampleInfo.Region == 'CA1' & SampleInfo.Condition == 'DLB'));
means(1,4) = mean(PP(SampleInfo.Region == 'CA3' & SampleInfo.Condition == 'CTL'));
means(1,5) = mean(PP(SampleInfo.Region == 'CA3' & SampleInfo.Condition == 'AD'));
means(1,6) = mean(PP(SampleInfo.Region == 'CA3' & SampleInfo.Condition == 'DLB'));
means(1,7) = mean(PP(SampleInfo.Region == 'DG' & SampleInfo.Condition == 'CTL'));
means(1,8) = mean(PP(SampleInfo.Region == 'DG' & SampleInfo.Condition == 'AD'));
means(1,9) = mean(PP(SampleInfo.Region == 'DGs' & SampleInfo.Condition == 'DLB'));


end

