
function image3Dvis(image3D,thres,str_isosurf,color_edge,upscale_image)

% Function for 3D rendering of the structures
%
% Input:
% - image3D: 3D array with the volume
% - other parameters for the rendering
%
% Author: Luis Salamanca 
% Version: 0.1
% Date: 01.07.2018 

% In case we use sparse matrices
image3D=full(image3D);

% Since the size of the voxels might not be square
if exist('upscale_image','var') && prod(upscale_image)>1
    ups_mat=ones(upscale_image(1:2));
    image3D_up=[];
    for i_z=1:size(image3D,3)
        image3D_aux=kron(image3D(:,:,i_z),ups_mat);
        image3D_aux=reshape(kron(ones(1,upscale_image(3)),image3D_aux),...
            size(image3D,1),size(image3D,2),[]);
        image3D_up=cat(3,image3D_up,image3D_aux);
    end;
    image3D=(imgaussfilt3(image3D_up,upscale_image)>0.1);
end;

% In case of not declare parameters
if nargin<2
    thres=0.9;
    hiso = patch(isosurface(full(image3D),thres)); 
elseif nargin<3    
    if isempty(thres)
        thres=0.9;
    end;
    hiso = patch(isosurface(full(image3D),thres)); 
else
    if isempty(thres)
        thres=0.9;
    end;
    if isempty(str_isosurf)
        hiso = patch(isosurface(full(image3D),thres)); 
    else
        hiso = patch(str_isosurf);
    end;    
end;

hiso.EdgeColor = 'none';

if exist('color_edge','var')
    hiso.FaceColor = color_edge;
else
    hiso.FaceColor = 'magenta';
end;

axis equal
set(gca,'Box','on')
camproj perspective
camzoom(0.7)
view(105,55) %$view(105,55)
camlight(-45,45)
hcap.AmbientStrength = 0.9;
lighting gouraud

