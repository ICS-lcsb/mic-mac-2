function [idx,idy,idz] = limitBoundingBox(boundingBox, maxZ)

    idx = boundingBox(1,2)+0.5 : boundingBox(1,2)-0.5+boundingBox(1,5);
    idy = boundingBox(1,1)+0.5 : boundingBox(1,1)-0.5+boundingBox(1,4);
    idz = boundingBox(1,3)+0.5 : min(maxZ,boundingBox(1,3)-0.5+boundingBox(1,6));
    
    if boundingBox(1,3)-0.5+boundingBox(1,6) > maxZ +1
        error('X')
    end
end

