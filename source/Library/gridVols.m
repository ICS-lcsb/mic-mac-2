function gridVols(idx,vols)

flag_structs=1; % Plot the 3D renders
flag_plotgraph=0; % Graph and overlaid the structure

flag_info=1;
flag_subinfo=0;
size_bar_um=20; % Size of scale bar

upscale_im=[1,1,5];
subplot_grid=[ceil(sqrt(length(idx))),ceil(sqrt(length(idx)))];

n_pix_bar=round(size_bar_um/0.208);

x_max = 600;
y_max = 600;
z_max = 35;

 
tot_str=zeros(subplot_grid(1)*x_max,subplot_grid(2)*y_max,z_max);
offset_x=0;
offset_y=0;
for i=1:length(idx)

    single_str= vols{idx(i)};

    tot_str(offset_x+(1:size(single_str,1)),offset_y+(1:size(single_str,2)),...
        (1:size(single_str,3)))=single_str;
    if i==1
        tot_str((1:n_pix_bar),(1:4),...
            (end-3:end))=1;
    end        

    if mod(i,subplot_grid(2))==0
        offset_x=offset_x+x_max;
        offset_y=0;
    else
        offset_y=offset_y+y_max;
    end

end

% Print the big array and save it
if flag_structs
    fig_renders=figure();
    image3Dvis(tot_str,[],[],[167 88 217]./256,upscale_im);

    axis equal;axis off;
    set(gcf,'Color','white');
    view([60,60])

    %print(fig_renders,['Renders_Cluster' num2str(i_clust) '_' aux_str  '_NumbRenders' num2str(numb_struct) '.png'], '-dpng', '-r600');
    %view([60,40])
end;

    
    


end

